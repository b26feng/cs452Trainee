# CS 452 - TC2

Date submitted: July 29, 2018

Members: 

- An Nguyen (SID 20648054)
- Tom (Bingzheng) Feng (SID 20640728)

## I. Operating Details

- Souce Code can be found at https://git.uwaterloo.ca/b26feng/cs452Trainee/tree/withtime

- Full pathname of the executable file:
    ```
    load -b 0x218000 -h 10.15.167.5 "ARM/b26feng/main.elf"
    go
    ```

- To compile the code, 
    ```
    cd lib
    make clean; make; make install
    cd ../src
    make clean; make
    // download the executable main.elf onto the ARM box and run as usual
    ```

## II. Kernel

### Notifiers

The notifiers all `AwaitEvent` on their corresponding interrupt similar to the TIMER3_Interrupt in kernel3, except for the TrainOut Notifier also having to deal with the CTS interrupt. To mitigate the effects of the CTS interrupt having not been disabled and re-asserted before the next byte is written which overwrites the previous byte, we decided to go with the Marklin's suggestion to use a delay of 10 ticks before writing the next byte. In order to minimize the number of times that the Notifiers have to go back to the Servers to fetch a byte on each interrupt thus improving performance, we also employ a 3-byte buffer for Transmit COM1 and a 64-byte buffer for Transmit COM2. 

In addition to Putc(), we also implement PutStr() by making them both into system calls to reduce message passing by just simply calling Send() otherwise.
Printf() is achieved by making a copy of `bwio.c` and replacing the bw functions with the corresponding interrupt-driven functions. We name the file `interrupt-io.c`. This is needed to allow for transmitting bytes atomically thus rendering the escape sequences properply.
Only Getc() is supported for receiving data for K1-K4.


### Sensor Server, Sensor Notifier, Sensor Print Task (modified)

The Sensor "Notifier" mimics the other Notifiers in that it sits in a loop polling for sensor data every 10 ticks, then Sends the raw data in form of a string to the Sensor Server.
The Server can then store the data in its body and Replies to any task that queries for it. For kernel 4, there is only one task querying for it that is the SensorPrintTask. It sits in a loop Sending for the data from the Server, process it and display onto the screen the 5 most recently triggered sensors. For TC1, the TCSensorCourier also queries for the Sensor Update to update the current position of the train in the Train Controller. 

For performance, a new syscall GetStr(COM1, 10) is implemented for exclusive use in the Sensor Notifier in place of calling `Getc(COM1)` 10 times to reduce message passing. Besides, before sending to the Sensor Server, the Sensor Notifier also performs a small check on the polled data and will only send when the data is not all 0's (new).

With some of these changes, we are able to bring the delay time to 5 ticks in the TrainOutNotifier.

### Couriers (modified)

We have added 5 Couriers `TCTS`, `TSTC`, `TCSensor`, and `TCDog`.

- `TSTCCourier` waits on the Track Server for any turn-out being switched, then it Sends the TrackNode corresponding to that switch to the Train Controller to notify the train if that newly switched turn-out is going to affect its next sensor's prediction. 
- `TSTCCourier` 
- `TCSensorCourier` simply sits in a loop, fetches a new poll of sensors and timestamp from the Sensor Server and Sends to the Train Controller to update the current position of the Train.
- `TCDogCourier` is spawned by the Train Controller and kept in a queue in the TC when not in use. Its goal is to first Send to the TC for the amount of the time that we expect the next sensor to be hit, registers its ID onto the Train structure, Delays, then Sends back to the TC after that amount of time to check if that Sensor was indeed triggered i.e. the DogTaskID is not that of itself. If the DogTaskID has not changed, that means the Courier gets back before the Sensor is expected to trigger, so the Train's`bTimedOut` is set to 1. The WatchDog (Detective) task is always reused, i.e. added back to the buffer.
- `TCPRCourier` is used to take PathRouting orders from the TC to the PathRouter Server.

### Display Task 

After initializing the startup layout, and the switches, the display task sits in a loop calling Getc(COM2) and parses the input into commands and call the appropriate functions to set speed for the train, reverse the train and set state for the switches. Sadly, `q` is now not a clean exit and only breaks out of the main loop back to RedBoot.

For TC1, we have added parsing to allow for some new functionalities below.

-`trk A` or `trk B` initializes the track and its switches to A or B accordingly
-`reg TrainID Sensor Offset` registers the train's initial location in the Train Controller, Offset amount from Sensor
-`stop-after TrainID Sensor` stops the Train after Sensor is hit (for measuring the Stopping Distance) by executing a one-time-use worker called `Worker` spawned by the TC
-`velocity Sensor1 Sensor2` which prints out the Velocity between any two sensors and the timestamps at which the two sensors are hit by executing a one-time-use worker called `Stamper` spawned by the TC
-`shortDistance TrainID Distance` which theoretically stops the train within that distance after using our model to convert the distance to the number of ticks required to Delay between a SetSpeed(max) and a SetSpeed(0). A jump table is required to extrapolate NumTicks given our calibrated data, but it is not working as expected as of now. This operation runs by executing a one-time-use worker called `ShortDistance` spawned by the TC

### struct Train (new)

```
typedef struct{
  int TrainID;
  int Index;
  float Tick14;  // calibrated value
  float Tick10;  // calibrated value
  float Dist14;  // calibrated value
  float Dist10;  // calibrated value
  float DJerk;   // calculated from Tick14, Tick10, Dist14, Dist10
  float DTDiff;  // Tick14 - Tick10
  float Vel;     // constant velocity used to estimate stopping distance
  float StopDist; // calculated from Tick14, Tick10, Dist14, Dist10
  float StopTick; // calculated from Tick14, Tick10, Dist14, Dist10
  float DymVel;   // current Velocity
  float alpha;    // alpha value for calculating dynamic velocity
  int SDMinTick;  //tick to move train for 1 cm
  int SDMaxTick;  
  int Speed;          // current Speed of Train
  int LocOff;         // offset in mm from LocationBase Node
  int TimeStamp;      //when last Sensor hits
  int Dist;           // distance to next Sensor
  int MyDog;          // watchdog's taskID
  TrackNode* Last;    // the Node before last Sensor
  TrackNode* LocBase; // most recent Sensor Node hit
  TrackNode* Next;    // expected Next Sensor
  TrackNode* NNext;   // expected Next Next Sensor
  char bRegistered;   // if Train is registered
  char bTimedOut;     // set to 1 when NextSensor is not triggered but NextNextSensor is
  int RouteLength;    // dijkstra's
  int NumReserved;    // number of Nodes reserved TC2
}Train;
```

### Train Controller (TC) Server

The Train Controller Server is the main server, in charge of handling SetSpeed, Reverse, QuitProgram, RegisterTrain, SetTrack, UpdateTrain, UpdateWhenSwitchFlipped, StopAfterSensor, VelocityBetweenTwoSensors, WhenNextSensorHits.

To do so, it houses in its body: 

- a `Train` array for 5 Trains
- an int array `TCTS` of LocationBase indices for 5 trains that is used to get the corresponding Nodes from the Track Server
- a TrackNode array `TCTSReply` of LocationBase and NextSensorNode of size 10 (first 5 LocationBase, last 5 NextSensorNode, each for each Train) to keep the Nodes gotten from the TrackServer
- an int array `DinnerTime` of size 5 to store the time the NextSensor expected to be hit for each train.

When we get a report from the TSTCCourier that a turn-out is switched, the function `UpdateNextOrNot` is invoked to determine if this switch affects the Train's NextSensor and NNextSensor.

- If the switch's Last Sensor being the same as the Train's LocationBase or the Train's Next Sensor
    - If the Train's Offset is greater than the distance between the switch and its last sensor, then this turn-out is behind the train and its status does not affect the train's next expected Sensor or next next.
    - Otherwise, get the two next Sensors on the two edges stemming from the switch branch. If the switch's next Sensor is the same as the train's next Sensor then the switch did not affect the train. Otherwise, update the Train's next Sensor and next next Sensor, DinnerTime, and Dist to next Sensor fields in the Train struct.

If there is a watchdog available in our reservoir buffer, it will be used to "watch" for the next Sensor hit; if there isn't, a new `TCDog` task is spawned. 

### Track Server

The Track Server's structure is overlooks the track layout, keeps ahold of all 144 `TrackNodes` in an array and is in charge of 

- handling `init_tracka` or `init_trackb` based on a ReceiveMsg from the Display Task (UserInput).
- recording a switch flip in its TrackNode array whenever `SetSwitch` is called, and replying to `TSTCCourier` about this new change so it can be relayed back to the TC 
- getting the nodes corresponding to the Location Base indices and their nextSensor Nodes, given by the TCTSCourier for all trains registered (max 5 trains for now).


### Path Router Server, PRTask, Dijkstra's (modified)

We use Min Heap to implement Dijkstra's algorithm to find the shortest path from nodeA to nodeB.

A PathRouter Server is used to receive Routing orders from the TC via the TCPRCourier and spawns PRTask as needed (max 5, one for each train). PRTask is reused after it's done calculating the shortest path from nodeA to nodeB. The shortest path is the shortest among:

- nodeA to nodeB
- nodeA->reverse to nodeB
- nodeA to nodeB->reverse
- nodeA->reverse to nodeB->reverse

### Self-destroy 

is a new syscall implemented by manipulating the TaskID int as follows:

- the first bit is set to 1 to reflect that the Task is available
- the generation bits (the next 15 bits) are incremeneted by 1 to reflect how many times the Task has been destroyed

### Calibration

1. Dynamic Calibration

    Whenever a new sensor poll is reported by the `TCSensorCourier`, all the trains are updated using `UpdateTrain`:
- If the next Sensor is hit within the expected time, the LocationBase field is updated to be that SensorNode; the fields Dist (distance to next Sensor), `NextSensor` and `NNext` are also updated accordingly.

- To support the possibility of every other sensor being broken, if the next expected Sensor is not hit, but the Next Next Sensor is hit, the LocationBase field if updated to be the NNext; the fields Dist (distance to next Sensor), NextSensor and NNext are also updated accordingly. 

    Using the difference in distances and timestamps from last Sensor to this new Sensor, the new `dynamic velocity dymVel` field is re-calculated using the running average formula `v = (1 − α)v + αv'` where alpha = 0.25 for all trains. The DinnerTime array is then updated with new timestamps using the current timestamp, the new `Dist` and `dymVel` values to reflect the expected time the Next Sensor will be hit for all trains.

2. Stopping Distance 
    is measured manually by issuing the command `stopafter` for each train at speeds 10 and 14. A model is derived from these values to estimate stopping distances at the other speeds.

3. Short Moves
    We only modeled short distance moves that accelerate a train to speed 14 for less than 300 ticks and stop it after that many ticks for train 24 and 78.
    The model we use to estimate the relation between time and distance is $d=f(t) = 10\times 7 ^{(\log_2^{t/50})} $ where d is distance measured in mm, t is time measured in ticks.
    Solving $t=g(d)$ from this to obtain $t = 50 \times 2^{log_7^{10d}}$.
    Where $d \in [10,1200]$.
    We obtained this model by the following process:
    1. Send command `tr trainID 14`, Delay for $t_1$ ticks, and send `tr trainID 0`, after train stops, measure the distance. It moves 10 mm for 50 ticks Delay.
    2. Repeat step 1 but Delay for $2\times t_1$ ticks
    We found out that as the time doubles, the distance that it multiplies by 7 for ticks below 300 on trains 24 and 78. 
    This functionality is implemented but not working as expected due to the absence of a look up table for non-integer power of 2 and logarithm look up table for log based 7. 

4. Current Location of Trains
    is estimated using the LocationBase TrackNode and the dynamic velocity in the Train structure.



### Track Reservation (new)

A new field `TrainOwn` is added in the TrackNode structure to indicate which Train owns it. A `GoTo` function is also added to support an interface command to route Train to a chosen sensor.

- **RegTrack** A Train registers for nodes by `owning` the next two sensors (if possible) or all the nodes covered by its stopping distance, whichever value is larger (for first case, the distance is measured from the location base of train to the next next sensor; the stopping distance is acquired by calibration and estimation and kept in the Train struct). For example, everytime a Sensor is hit, the Train is updated with its new location base, next, next Sensor, Dymamic Velocity, TimeStamp of last Sensor hit and the DinnerTime (time expected for next Sensor to hit). Then the Train tries to register for TrackNodes by calling **RegTrack**. If it fails, then the Train is set to stop within its stopping distance using `UpdateTrainSpeed` with speed 0.

- **FreeTrack** uses the location base of a Train to free Nodes that it has passed by that has `TrainOwn == TrainID`.

These two functions are called whenever the Train is reversed, a Sensor is hit, a turn-out is switched, the Speed is updated. 

# Program Output (new)

As of now, our program cannot sustain for longer than 15 seconds to perform any of those functionalities listed above. We have tried tweaking the priorities for tasks, but to no avail. After such shor time, the Sensor Server would crash, along with the UART1 Server for some reason we have not been able to nailed down. From time to time, we would also get a print comment from a function that we never call in our codebase (we triple checked this fact), which suggests that there is a bug in our program that is messing with memory. Context Switch is a prime suspect. If given more time, we would look into that first. 

![refer-to-tc2-png-in-same-dir-if-photo-not-rendered](tc1.png)

