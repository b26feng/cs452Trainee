#include <buffer.h>
#include <bwio.h>
#include <event-itr.h>
#include <io-api.h>
#include <k4.h>
#include <kernel.h>
#include <priority-q.h>
#include <syscall.h>
#include <syscall-handler.h>
#include <scheduler.h>
#include <td-kernel.h>
#include <timer.h>
#include <types.h>

void Init_BWIO(){
	bwsetfifo(COM2,OFF);
	bwsetspeed(COM2,115200);
	bwsetfifo(COM1,OFF);
	bwsetspeed(COM1,2400);
}

int main()
{
  Init_BWIO();
  //bwprintf(COM1,"main: InitBWIO\n\r");
  KernelStruct Colonel;
  int req, QuitProg = 0;
  
  Buffer* SendQs[MAX_NUM_TD];
  Colonel.AllSendQ = SendQs;
  
  EventAwaitTask WaitEvents[NUM_EVENTS];
  InitWaitEventTable(WaitEvents);
  Colonel.WaitTable = WaitEvents;
  
  //Buffer* SendQs[MAX_NUM_TD];
  HN SchedulerStorage[MAX_NUM_TD];
  MinHeap SchedulerHeap;
  SchedulerHeap.Storage = SchedulerStorage;
  Colonel.Scheduler = &SchedulerHeap;
  InitSwi();
  kernelInit(&Colonel); 
  TD* FUT = CreateTask(&Colonel, 1, (void *) (&FirstUserTask));
  //bwprintf(COM2,"M: %d, %d\n\r",FUT,Colonel.Tasks);
  pushToScheduler(&Colonel, FUT);
  TD* Task;
  int ReturnValue = 0;

  // enable all the interrupts
  //EnableInterrupt(25);
  //EnableInterrupt(26);
  //EnableUART(COM1);
  //EnableInterrupt(52);
  EnableUART(COM2);
  //bwputc(COM1,"U");
  //bwprintf(COM1,"Main AllSendQ Address is %d\n\r",SendQs);
  FOREVER {
    //bwprintf(COM1,"main: begin of loop, active: %d\n\r",Colonel.Active);
    if(Colonel.Active == NULL){
      ReturnValue = 0;
      //bwprintf(COM1,"main: before get new from scheduler \n\r");
      //Task = getNextTaskScheduler(&Colonel);
      LoadNextTask(&Colonel);
      Task = Colonel.Active;
    }
    else{
      Task = Colonel.Active;
      ReturnValue = Task->RetVal;
    }
    
    if(Task == NULL){
      //bwprintf(COM2,"main: no active task, break");
      break;
    }
    //bwprintf(COM1,"main: Task%d, Priority: %d\n\r", Task->TaskID,Task->TaskPriority);
    req = Activate(&Colonel,Colonel.Active);
    Handle(&Colonel, req);
    //bwprintf(COM1,"main: back to main from handler, active %d\n\r",Colonel.Active);
  }
  bwprintf(COM2,"%c[2J",27);
  bwprintf(COM2,"Heap Num: %d, Heap Last: %d\n\r",(Colonel.Scheduler)->Num,(Colonel.Scheduler)->Last);
  bwprintf(COM2, "\n\r");
  bwprintf(COM2, "Program Exited.\n\r");
  return 0;  
}
