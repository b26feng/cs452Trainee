#include "context-switch.h"


void InitSWHandler() {
    asm volatile(
        "ldr r4, =kerent\n\t"
        "add r4, r4, #0x218000\n\t"
        "mov r5, #0x28\n\t"
        "str r4, [r5]\n\t"
    );
}

void SetUpRegs() {

}

//r0 request struct
//r1 spsr onto kernel stack
//r2 sp onto kernel stack
//r3 swi n

void kerent() {
    asm volatile (
        "@ get swi number\n\t"
        "ldr r3, [lr, #-4]\n\t"
        "bic r3, r3, #0xff000000\n\t"

        "@ enter system mode\n\t"
	    "msr cpsr_c, #0xDF\n\t"

	    "@ push register contents onto users stack\n\t"
	    "stmfd sp!, {r0-r12,r14}\n\t"

        "@ save user's sp\n\t" 
	    "mov r2, sp\n\t"

        "sub sp, sp, #8\n\t"

	    "@ switch back to svc mode\n\t"
	    "msr cpsr_c, #0xD3\n\t"

        "@ get spsr of user\n\t"  
        "msr r1, spsr\n\t"

        "@ push user's spsr and lr_svc onto user's stack\n\t"
        "stmfd r2!, {r1, r14}\n\t"

        "@ somehow save r2 which is sp_user into user structure"

        "@ pop kernels stack\n\t"
        "ldmfd sp!, {r1-r12, pc}\n\t"

    )
}

void kerxit() {
    asm volatile (
    "@push kernels regs"
    "stmfd sp!, {r3-r12, r14}\n\t"

	"@ switching to system mode\n\t"
	"msr cpsr_c, #0xDF\n\t" 

	"@ restore user's lr and spsr in that order\n\t"
	"ldr r1, [sp, #0]\n\t"
	"ldr r2, [sp, #4]\n\t"
	"sub sp, sp, #4\n\t"

	"@ restore user's registers\n\t"
	"ldmfd sp!, {r4-r14}\n\t"

	"@ switch back to svc mode\n\t"
	"msr cpsr, #0xD3\n\t"
    
	"@ restore user's spsr\n\t"
	"msr spsr, r2\n\t"
	
	"@ restore user's lr to continue execution; also put spsr into cpsr; RUN user\n\t"
	"movs pc, r1\n\t"
    );
}

