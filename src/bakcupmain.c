// #include "kernel.h"
#include "task-descriptor.h"
#include "scheduler.h"
#include "k1.h"
#include "bwio.h"
#include "request.h"

#define FOREVER while(1)


int main(int argc, char const *argv[])
{
    KernelStruct Colonel;  
    int i, Status, req;
    TD* Next;

    int* kernelEntry = (int *)(0x8);
    *kernelEntry = (int)&kerent;
    
    SetUpRegs();
    kerent();
    kernelInit(&Colonel); 
    schedulerInit();
    
    // First User Task
    Colonel->Active = Create(4, &firstUserTaskChildren);
    pushToScheduler(&Colonel);

    FOREVER {
        Task = getNextTaskScheduler(&Colonel);
        req = Activate(&Colonel, Task);
        Handle(&Colonel, req);
    }
    return 0;

}
