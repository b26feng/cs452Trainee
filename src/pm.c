#include <buffer.h>
#include <bwio.h>
#include <k1.h>
#include <kernel.h>
#include <priority-q.h>
#include <k2PerfTest.h>
#include <syscall.h>
#include <syscall-handler.h>
#include <scheduler.h>
#include <timer.h>
#include <td.h>
#include <types.h>

extern Buffer*** TotalSendQ;
int main()
{
  
  KernelStruct Colonel;
  int i, Status, req;
  TD* Next;
  
  static Buffer* AllSendQ[MAX_NUM_TD];
  *(TotalSendQ) = AllSendQ;
  //Buffer* SendQs[MAX_NUM_TD];
  bwprintf(COM2,"Main AllSendQ Address%d\n\r",AllSendQ);
  InitSwi();
  kernelInit(&Colonel); 
  TD* FirstUserTask= CreateTask(&Colonel, 5, (void *) (&PerfTest));

  
  pushToScheduler(&Colonel, FirstUserTask);
  TD* Task;
  int ReturnValue = 0;
  Clock* C;
  ClockInit(C);
  int Start = 0;
  FOREVER {
    if(Colonel.Active == NULL){
      ReturnValue = 0;
      Task = getNextTaskScheduler(&Colonel);
    }
    else{
      Task = Colonel.Active;
      ReturnValue = Task->RetVal;
    }
    ClockUpdate(C);
    if(Task == NULL) break;
    //bwprintf(COM2,"main: Task%d, Priority: %d\n\r", Task->TaskID,Task->TaskPriority);
    req = Activate(&Colonel,Task);
    ClockUpdate(C);
    Handle(&Colonel, req);
  }
  int end = C->ClockCount;
  bwprintf(COM2,"Clock Total: %d\n\r",end-Start);
  return 0;  
}
