#include <buffer.h>
#include <bwio.h>
#include <k3.h>
#include <kernel.h>
#include <priority-q.h>
#include <syscall.h>
#include <syscall-handler.h>
#include <scheduler.h>
#include <td.h>
#include <types.h>
#include <timer.h>

void InitBWIO(){
  bwsetfifo(COM2, OFF);
  bwsetspeed(COM2, 115200);
}

int main() {
  
  KernelStruct Colonel;
  TD* Task, firstUserTask;
  int Req, ReturnValue = 0;
  
  Buffer* SendQs[MAX_NUM_TD];
  Colonel.AllSendQ = SendQs;

  InitBWIO();
  InitSwi();
  kernelInit(&Colonel); 
 
  
  firstUserTask= CreateTask(&Colonel, 5, (void *) (&FirstUserTask));
  pushToScheduler(&Colonel, firstUserTask);

  InitNameServer();

  FOREVER {
    if(Colonel.Active == NULL){
      ReturnValue = 0;
      Task = getNextTaskScheduler(&Colonel);
    }
    else {
      Task = Colonel.Active;
      ReturnValue = Task->RetVal;
    }
    
    if(Task == NULL) break;

    Req = Activate(&Colonel,Task);
    Handle(&Colonel, Req);
  }
  return SUCCESS;  
}
