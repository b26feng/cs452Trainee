	.file	"main.c"
	.section	.rodata
	.align	2
.LC0:
	.ascii	"One Insert\012\015\000"
	.align	2
.LC1:
	.ascii	"TaskID: %d, DelayedTick:%d.\012\015\000"
	.text
	.align	2
	.global	main
	.type	main, %function
main:
	@ args = 0, pretend = 0, frame = 260
	@ frame_needed = 1, uses_anonymous_args = 0
	mov	ip, sp
	stmfd	sp!, {sl, fp, ip, lr, pc}
	sub	fp, ip, #4
	sub	sp, sp, #260
	ldr	sl, .L4
.L3:
	add	sl, pc, sl
	mov	r3, #0
	str	r3, [fp, #-36]
	mov	r3, #0
	str	r3, [fp, #-32]
	sub	r3, fp, #276
	str	r3, [fp, #-28]
	mov	r0, #1
	ldr	r3, .L4+4
	add	r3, sl, r3
	mov	r1, r3
	bl	bwprintf(PLT)
	sub	r3, fp, #36
	mov	r0, r3
	mov	r1, #15
	mov	r2, #5
	bl	InsertMinHeap(PLT)
	sub	r3, fp, #36
	mov	r0, r3
	bl	printMinHeap(PLT)
	sub	r3, fp, #36
	mov	r0, r3
	bl	RemoveMinMinHeap(PLT)
	sub	r3, fp, #36
	mov	r0, r3
	bl	printMinHeap(PLT)
	sub	r3, fp, #36
	sub	r2, fp, #20
	sub	ip, fp, #24
	mov	r0, r3
	mov	r1, r2
	mov	r2, ip
	bl	GetMinMinHeap(PLT)
	ldr	r2, [fp, #-20]
	ldr	ip, [fp, #-24]
	mov	r0, #1
	ldr	r3, .L4+8
	add	r3, sl, r3
	mov	r1, r3
	mov	r3, ip
	bl	bwprintf(PLT)
	mov	r3, #0
	mov	r0, r3
	sub	sp, fp, #16
	ldmfd	sp, {sl, fp, sp, pc}
.L5:
	.align	2
.L4:
	.word	_GLOBAL_OFFSET_TABLE_-(.L3+8)
	.word	.LC0(GOTOFF)
	.word	.LC1(GOTOFF)
	.size	main, .-main
	.ident	"GCC: (GNU) 4.0.2"
