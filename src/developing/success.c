
#include <bwio.h>
#include <test.h>

void kernel_enter(){
  //bwprintf(COM2,"kernel\n\r");
  asm volatile("movs pc, lr");
}
/*
void swi_init(){
  // put the branching instruction into 0x8
  

}
*/
int main(){
  //swi_init();
  
  //asm volatile("mov r2, #8");
  // asm volatile ("ldr r3, kernel_enter");
  //asm volatile ("str r3, [r2]");

  asm volatile("ldr r1, =kernel_enter");
  asm volatile("add r1, r1, #0x218000");
  asm volatile("mov r2, #0x28");
  asm volatile("str r1, [r2]");
  asm volatile("swi 12");
  
  /*
  // prepare return value
  asm volatile ("mov r0 ,#0");
  // return
  asm volatile ("ldmfd sp, {fp,sp,pc}");
    // these will never get executed
  asm volatile (".SWIHANDLE:");
  asm volatile ("ldr pc, =kernel_enter");
  */
  return 0;
}
