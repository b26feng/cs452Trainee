#include <bwio.h>
/*
void kernel_enter(){
  //bwprintf(COM2,"kernel\n\r");
  asm volatile("ldr r3, [lr, #-4]\n\t"
	       "bic r3, r3, #0xff000000\n\t"
	       "msr cpsr_c, #0xdf\n\t"
	       "stmfd sp!, {r0-r12,r14}\n\t"
	       "mov r2, sp\n\t"
	       "msr r1,spsr\n\t"
	       "msr cpsr_c, #0xd3\n\t"
	       "stmfd r2!, {r1}\n\t"
	       "ldmfd sp1!, {r4-r14}"
	       );
  int b = 456;
  bwprintf(COM2,"%d\n\r",b);
  asm volatile("movs pc, lr");
  }
void kernel_exit(){
  asm volatile();
}


void kernel_enter(){
  asm volatile(// sysmode
	       "msr cpsr_c, #0xdf\n\t"
	       // store user registers 
	       "stmfd sp!, {r0-r12,lr}\n\t"
	       // save sp_usr to r1
	       "mov r1, sp\n\t"
	       // move sp_usr down 2 slot
	       "sub sp, sp, #8\n\t"
	       // svc mode
	       "msr cpsr, #0xd3\n\t"
	       // save spsr( = cpsr_usr) to r2
	       "mrs r2, spsr\n\t"
	       // store spsr, lr_svc to sp_usr
	       "stmfd r1!, {r2,lr}\n\t"
	       // push sp_usr onto kernel
	       "stmfd sp!, {r1}\n\t"
	       /* ----- 
	       // restore sp_usr from sp_svc
	       "ldmfd sp!, {r1}\n\t"
	       // restore lr, spsr from sp_usr
	       "ldmfd r1!, {r2, lr}\n\t"
	       // write spsr as r2
	       "msr spsr, r2\n\t"
	       // sys mode
	       "msr cpsr, #0xdf\n\t"
	       // move sp_usr up 2 slot
	       "add sp, sp, #8\n\t"
	       // restore usr_status
	       "ldmfd sp!, {r0-r12,lr}\n\t"
	       // svc mode
	       "msr cpsr_c, #0xd3\n\t"
	       "movs pc, lr\n\t");
}
*/

void kernel_exit(){
  asm volatile(// save all kernel registers
	       "stmfd sp!, {r0-r12, lr}\n\t"
	       "mrs r3, cpsr\n\t"
	       "msr spsr, r3\n\t"
	       "msr spsr_c, #0xd0\n\t"
	       // sys mode
	       "msr cpsr_c, #0xdf\n\t"
	       "mov sp, r1\n\t"
	       ""
	       // svc mode
	       "msr cpsr_c, #0xd3\n\t"
	       "movs pc, r0\n\t");
}

void print(){
  register unsigned int r1 asm("r1");
  bwprintf(COM2,"%d\n\r",r1);
  asm("swi 12");
  //kernel_exit();
}

void kernel_enter(){
  asm volatile(// sys mode
	       "msr cpsr_c, #0xdf\n\t"
	       //save user registers
	       "stmfd sp!, {r0-r12,lr}\n\t"
	       "mov r1, sp\n\t"
	       "sub sp, sp, #8\n\t"
	       // svc mode
	       "msr cpsr_c, #0xd3\n\t"
	       "mrs r2, spsr\n\t"
	       "stmfd r1!, {r2,lr}\n\t"
	       "ldmfd sp!, {r0-r12,pc}\n\t");
}
int main(){
  
  /*asm volatile ("mov r0, sp\n\t"
    "mov r1, lr\n\t"
    "mov r2, fp\n\t"
		"mov r3, ip\n\t"
		"mov r4, sl\n\t"
		"msr cpsr_c, #0xd3\n\t"
		"mov sp, #0x1e00000\n\t"
		"msr cpsr_c, #0xd0\n\t"
		"sub r5, r0, sp\n\t"
		"mov sp, r0\n\t"
		"mov lr, r1\n\t"
		"mov fp, r2\n\t"
		"mov ip, r3\n\t"
		"mov sl, r4\n\t");*/
  
  int a = 234;
  bwprintf(COM2, "%d\n\r",a);
  asm volatile("ldr r4, =kernel_enter\n\t"
	       //"add r4, r4, #0x218000\n\t"
	       "mov r5, #0x28\n\t"
	       "str r4, [r5]\n\t"
	       //"mov r0, #233\n\t"
	       //"swi 4\n\t"
	       //"mov r0, #234\n\t"
	       //"swi 0\n\t"
	       "ldr r0, =print\n\t"
	       "mov r1, #0x1e00000\n\t"
	       );
  kernel_exit();
  int b = 123;
  bwprintf(COM2, "%d\n\r", b);
  return 0;
}
