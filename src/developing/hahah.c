#include <types.h>
#include <bwio.h>
/*
void test(TD* T){
  asm volatile(//"mov r0, #0\n\t"
	       "mov r1, #11\n\t"
	       "mov r2, #22\n\t"
	       "mov r3, #33\n\t"
	       "mov r4, #44\n\t"
	       "mov r5, #55\n\t"
	       "add r0, r0,#20\n\t"
	       "stmfd r0!, {r1-r5}\n\t"
	       //"ldmfd sp!, {r4}\n\t"
	       );


  asm volatile("ldmfd r0!,{r4}\n\t"
	       "ldmfd r0!,{r5-r8}\n\t");
  //register int r0 asm("r0");
  //int zero = r0;
  register int r4 asm("r4");
  int four = r4;
  int *t=(int *)T;
  //bwprintf(COM2,"r0:%d, t1:%d\n\r",T->sp,t[0]);
  register int r5 asm("r5");
  int five = r5;
  register int r6 asm("r6");
  int six = r6;
  register int r7 asm("r7");
  int seven = r7;
  register int r8 asm("r8");
  int eight = r8;
  
  bwprintf(COM2,"sp:%d, lr:%d, spsr:%d, retval:%d, args:%d\n\r",T->sp,T->lr,T->spsr,T->RetVal,T->Args);
  bwprintf(COM2,"r4:%d, r5:%d, r6:%d, r7:%d, r8:%d\n\r",four,five,six,seven,eight);
}
*/
int main(){
/*
  //TD T;
  //T.sp = 0x1e000000;
  //T.lr = 222;
  //T.spsr = 333;
  //T.RetVal = 444;
  //T.Args = 555;
  //register int r4 asm("r4");
	       
  // bwprintf(COM2,"T:%d\n\r",r4);
  asm volatile("mov r0, #10\n\t"
	       "mov r1, #11\n\t"
	       "mov r2, #22\n\t"
	       "mov r3, #33\n\t"
	       "mov r4, #44\n\t"
	       "mov r5, #55\n\t"
	       "stmfd sp!, {r0-r5}\n\t"
	       "ldmfd sp!, {r3}\n\t");
  //test(&T);
  register int r3 asm("r3");
  int three = r3;
  bwprintf(COM2,"r3: %d\n\r",three);
*/  
return 0;
}
