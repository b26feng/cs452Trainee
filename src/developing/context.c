#include <bwio.h>
/*
void kernel_enter(){
  //bwprintf(COM2,"kernel\n\r");
  asm volatile("ldr r3, [lr, #-4]\n\t"
	       "bic r3, r3, #0xff000000\n\t"
	       "msr cpsr_c, #0xdf\n\t"
	       "stmfd sp!, {r0-r12,r14}\n\t"
	       "mov r2, sp\n\t"
	       "msr r1,spsr\n\t"
	       "msr cpsr_c, #0xd3\n\t"
	       "stmfd r2!, {r1}\n\t"
	       "ldmfd sp1!, {r4-r14}"
	       );
  int b = 456;
  bwprintf(COM2,"%d\n\r",b);
  asm volatile("movs pc, lr");
  }
*/
void kernel_enter(){
  asm volatile(
	       "msr cpsr_c, #0xdf\n\t"
	       "stmfd sp!, {lr}\n\t"
	       "mov r1, sp\n\t"
	       "sub sp, sp, #8\n\t"
	       "msr cpsr, #0xd3\n\t"
	       "mrs r2, spsr\n\t"
	       "stmfd r1!, {r2,lr}\n\t"
	       /* ----- */
	       "ldmfd r1!, {r2, lr}\n\t"
	       "msr spsr, r2\n\t"
	       "msr cpsr, #0xdf\n\t"
	       "add sp, sp, #8\n\t"
	       "ldmfd sp!, {lr}\n\t"
	       "msr cpsr_c, #0xd3\n\t"
	       "movs pc, lr\n\t");
}

void swi_init(){
  // put the branching instruction into 0x8
  

}

int main(){
  
  /*asm volatile ("mov r0, sp\n\t"
		"mov r1, lr\n\t"
		"mov r2, fp\n\t"
		"mov r3, ip\n\t"
		"mov r4, sl\n\t"
		"msr cpsr_c, #0xd3\n\t"
		"mov sp, #0x1e00000\n\t"
		"msr cpsr_c, #0xd0\n\t"
		"sub r5, r0, sp\n\t"
		"mov sp, r0\n\t"
		"mov lr, r1\n\t"
		"mov fp, r2\n\t"
		"mov ip, r3\n\t"
		"mov sl, r4\n\t");*/
  int a = 456;
  bwprintf(COM2, "%d\n\r",a);
  asm volatile("ldr r4, =kernel_enter\n\t"
	       "add r4, r4, #0x218000\n\t"
	       "mov r5, #0x28\n\t"
	       "str r4, [r5]\n\t"
	       "swi 12\n\t");
  int b = 123;
  bwprintf(COM2, "%d\n\r", b);
  return 0;
}
