#include <bwio.h>
#include <types.h>

void kerent(){
	int somvar = *(int *)(VIC1_BASE);
	// check to see whats in r3 containing 0x800b0000
	asm volatile("mov r0, #1\n\t"
			"ldr r1, [r3]\n\t"
			"bl bwputr\n\t"
		    );
}

void generateHWI(){
	bwprintf(COM2,"before interrupt\n\r");
	int *enable = (int *)(VIC1_BASE + VIC_INTERRUPT_ENABLE_OFFSET);
	int *protect = (int *)(VIC1_BASE + 0x20);
	int *SFTINT = (int *)(VIC1_BASE + 0x18);

	*protect = 1; // this actually only enables protection for the device in the last bit
	*enable = 1;
	*SFTINT = 1;
	bwprintf(COM2,"after interrupt\n\r");
}

int main(){
	bwprintf(COM2,"before setup\n\r");
	asm volatile("mov r0, #0x38\n\t"
			"ldr r1, =kerent\n\t"
			"add r1, r1, #0x218000\n\t"
			"str r1, [r0]\n\t");
	generateHWI();
	return 0;
}
