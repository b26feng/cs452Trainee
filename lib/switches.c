#include <types.h>
#include <ts7200.h>
#include <switches.h>
#include <interrupt-io.h>
#include <io-api.h>
#include <canvas.h>

void PrintSwitch(int SwitchNum, char Dir) {
  int ColPos,RowPos,Color;
  Color = (Dir == 'C')? 33:36;
 	if (SwitchNum <= 18) {
		ColPos = 11 + (SwitchNum - 1)*3;
		RowPos = 20;
		
	}
	else {
		ColPos = 11 + (SwitchNum - 153)*5;
		RowPos = 24;
		
	}	
	Printf(COM2, "%s%c[%d;%dH%c[%dm%c%s%s",SAVE_CURSOR,ESC,RowPos,ColPos,ESC,Color,Dir,RESET_COLOR,RESTORE_CURSOR);
	//Printf(COM2, "%s", RESTORE_CURSOR);
}

void OffSolenoid() {
	Putc(COM1, SOLENOID_OFF);
}

char getSwitchOffset(char Switch){
  if(Switch<153) return ((Switch-1)<<1)+80;
  return (((Switch-153)+19)<<1) + 80;
}

void TrainSetSwitch(int ClkServer, int TSServer,int SwitchNum, int SwitchDir) {
//void TrainSetSwitch(int ClkServer,int SwitchNum, int SwitchDir) {
  char Package[2];
  TSMsg SendMsg;
  int Result; 
  Package[0] = (char)SwitchDir;
  Package[1] = (char)SwitchNum;
  PutStr(COM1, Package, 2);
  Delay(ClkServer,20); // 300ms //TODO 150ms to 500ms see notes
  OffSolenoid();
  SendMsg.Type = FlipSwitch;
  SendMsg.Src = (TrackNode *)(getSwitchOffset((char)SwitchNum));
  SendMsg.TrackAB = (SwitchDir);
  Send(TSServer,(void*)&SendMsg,sizeof(TSMsg),(void*)&Result,sizeof(int));
  if (SwitchDir == STRAIGHT) PrintSwitch(SwitchNum, 'S');
  else PrintSwitch(SwitchNum, 'C');
}

void InitSwitches(int ClkServer,int TSServer) {
  //void InitSwitches(int ClkServer) {
	int i;
	for (i = 0; i < 18; i++) {
	  if(i == 0||i==1||i==3||i==7||i==8||i==10||i==17){
	    TrainSetSwitch(ClkServer,TSServer,i+1, STRAIGHT);
	    //TrainSetSwitch(ClkServer,i+1, STRAIGHT);
	  }else{
	    //TrainSetSwitch(ClkServer,i+1, CURVED);
	    TrainSetSwitch(ClkServer, TSServer,i+1, CURVED);
	  }
	  Delay(ClkServer, 1);
	}
	TrainSetSwitch(ClkServer, TSServer,153, STRAIGHT);
	//TrainSetSwitch(ClkServer,153, STRAIGHT);
	Delay(ClkServer,1);
	TrainSetSwitch(ClkServer, TSServer,154, CURVED);
	//TrainSetSwitch(ClkServer,154, CURVED);
	Delay(ClkServer, 1);
	TrainSetSwitch(ClkServer, TSServer,155, STRAIGHT);
	//TrainSetSwitch(ClkServer,155, STRAIGHT);
	Delay(ClkServer, 1);
	TrainSetSwitch(ClkServer, TSServer,156, CURVED);
	//TrainSetSwitch(ClkServer,156, CURVED);
	Delay(ClkServer, 1);
}
