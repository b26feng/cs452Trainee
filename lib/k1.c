#include <bwio.h>
#include <types.h>
#include <syscall.h>

void someK1Fn() {
//	bwprintf(COM2, "someK1Fn: just in someK1Fn\n\r");
	bwprintf(COM2, "My TaskID: <%d> and My ParentID: <%d>\n\r", MyTid(), MyParentTid());
//	bwprintf(COM2, "someK1Fn: before calling Pass\n\r");
	Pass();
	bwprintf(COM2, "My TaskID: <%d> and My ParentID: <%d>\n\r", MyTid(), MyParentTid());
//	bwprintf(COM2, "someK1Fn: after finished calling Pass; about to call Exit\n\r");
	Exit();
}

void firstUserTaskChildren() {
//	bwprintf(COM2, "In FirstUserTaskChildren\n\r");
	//  bwprintf(COM2,"First User Task\n\r");
	int TaskID;
	AP Args1, Args5;
	//bwprintf(COM2,"ARG location %d\n\r",&Args1);
	Args1.arg0 = (void *) 1;
	Args1.arg1 = (void *)(&someK1Fn);
	Args1.arg2 = 0;
	Args1.arg3 = 0;

	Args5 = Args1;
	Args5.arg0 = (void *) 5;
	//bwprintf(COM2,"firstUserTask %d\n\r",&someK1Fn);
	//bwprintf(COM2,"Args = %d\n\r",&Args1);

	TaskID = Create(&Args1);
	bwprintf(COM2, "Created: <%d>\n\r", TaskID);
	  
	TaskID = Create(&Args1);
	bwprintf(COM2, "Created: <%d>\n\r", TaskID);

	TaskID = Create(&Args5);
	bwprintf(COM2, "Created: <%d>\n\r", TaskID);

	TaskID = Create(&Args5);
	bwprintf(COM2, "Created: <%d>\n\r", TaskID);
	

	bwprintf(COM2, "FirstUserTask: exiting\n\r");
	
//	bwprintf(COM2,"firstUserTaskChildren: before calling Exit\n\r");
	Exit();
//	bwprintf(COM2,"after exit() being called at the end of FirstUserTaskChildren\n\r");
			}
