#include <nameserver.h>
#include <ioserver.h>
#include <td-kernel.h>
#include <kernel.h>
#include <bwio.h>

TD* CreateTask(KernelStruct* Colonel, int Priority, void* code) {
    if (Colonel->NumTDsAvail == 0) return NULL;
    //TD* NewTask = Colonel->FirstFree;

    //Colonel->FirstOfFreeList = NewTask->NextFree;
    //NewTask->NextFree = NULL;

    TD* NewTask = GetFreeTD(Colonel);
    NewTask->TaskState = Ready;
    NewTask->TaskPriority = Priority;
    NewTask->NextPriority = Priority;
    unsigned int MASK = 4294967295 >> 1; // all 1's 
    NewTask->TaskID = MASK & (NewTask->TaskID);
    if (Colonel->Active == NULL) NewTask->ParentID = 0;
    else NewTask->ParentID = (Colonel->Active)->TaskID;
    // init task stack
    //NewTask->sp -= 11;
    
    //bwprintf(COM2, "CreateTask  -TID:%d\n\r",GetMemoryIndex(NewTask));
    //bwprintf(COM2, "CreateTask sp address:%d\n\r", NewTask->sp);
    
    //(NewTask->sp)[0] = NewTask->spsr;
    NewTask->spsr = 0x60000050;
    NewTask->lr = (int *)(code + 0x218000);
    if(code == (void *)&NameServer){
      Colonel->NameServerID = NewTask->TaskID;
      //bwprintf(COM2,"CreateTask: Name Server id: %d\n\r",Colonel->NameServerID);
    }else if(code == (void *)(&IServer1)){
      Colonel->IServer1ID = NewTask->TaskID;
    }else if(code == (void *)(&OServer1)){
      Colonel->OServer1ID = NewTask->TaskID;
    }else if(code == (void *)(&IServer2)){
      Colonel->IServer2ID = NewTask->TaskID;
    }else if(code == (void *)(&OServer2)){
      Colonel->OServer2ID = NewTask->TaskID;
    }
    Colonel->NumTDsAvail -= 1;
    //bwprintf(COM2,"CreateTask, function pointer: %d\n\r",NewTask->lr);
    return NewTask;
}

void RemoveTask(KernelStruct* Colonel, TD* Task) {} // IF NECESSARY
