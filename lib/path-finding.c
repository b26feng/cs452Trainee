#include <buffer.h>
#include <bwio.h>
#include <canvas.h>
#include <clockserver-api.h>
#include <courier.h>
#include <display.h>
#include <interrupt-io.h>
#include <path-finding.h>
#include <syscall.h>
#include <types.h>
#include <tn-heap.h>
#include <track_data.h>
#include <tools.h>

int Dijkstra(TrackNode* Trk, int Src, int Dest, Buffer* Path) {
  BufferClear(Path);
  MinHeap Heap;
  int Index, i;
  TrackNode* NextNode; 
  TrackNode* MinNode = NULL;
  
  Buffer TempBuf;
  int Storage[TRACK_MAX];
  InitBuffer(&TempBuf,(void *)Storage,(int)(TRACK_MAX));
  
  TrackNode* SrcNode = &(Trk[Src]);
  TrackNode* DestNode = &(Trk[Dest]);
  
  TrackEdge Edge[2];
  
  TrackNode* tPath[TRACK_MAX];
  Heap.Storage = (void*)tPath;
  Heap.Num = 0;
  Heap.Last = 0;
  
  for (i = 0; i < TRACK_MAX; i++) {
    Trk[i].PathDistance = INT_MAX;
    Trk[i].Parent = NULL;
    Trk[i].bVisited = 0;
    Trk[i].State = 'C';
  }
  
  SrcNode->PathDistance = 0;
  InsertHeap(&Heap, SrcNode);
  
  while(Heap.Num > 0) {
    Index = 0;
    
    MinNode = GetMinHeap(&Heap);
    if (MinNode == NULL) {
      //bwprintf(COM2,"MinNode is NUll\n\r");
      break;
    }
    
    RemoveMinHeap(&Heap);
    if (MinNode->name[0] == 'E' && MinNode->name[1]=='X') continue;
    
    MinNode->bVisited = 1;
    
    if (stringCompare(DestNode->name, MinNode->name) == 0) {
      DestNode->bVisited= 1;
      break;
    }
    
    switch(MinNode->type) {
    case NODE_BRANCH:
      Edge[Index++] = MinNode->edge[DIR_STRAIGHT];
      Edge[Index++] = MinNode->edge[DIR_CURVED];
      break;
    case NODE_SENSOR:
    case NODE_MERGE:
    case NODE_ENTER:
      Edge[Index++] = MinNode->edge[DIR_AHEAD];
      break;
    default: // including NODE_EXIT
      break;
    }
    
    for (i = 0; i < Index; i++){
      NextNode = Edge[i].dest;
      if (NextNode->bVisited == 0 && (MinNode->PathDistance + Edge[i].dist < NextNode->PathDistance)) {
	NextNode->PathDistance = MinNode->PathDistance + Edge[i].dist;
	if (NextNode->Parent == NULL) InsertHeap(&Heap, NextNode);
	else DecreaseKey(&Heap, NextNode);
	NextNode->Parent = MinNode; 
      }
    }
  }
  
  if (DestNode->Parent == NULL) return -1;
  
  TrackNode* Tmp = DestNode->Parent;
  while (stringCompare(Tmp->name, SrcNode->name) != 0) {
    FeedBuffer(&TempBuf,(void*)Tmp);
    Tmp = Tmp->Parent;
  }
  
  FeedBuffer(Path,(void*)SrcNode);
  TrackNode* T;
  while(TempBuf.Length > 0) {
    BufferFILO(&TempBuf, (void*)&T);
    BufferPopLast(&TempBuf);
    FeedBuffer(Path, (void*)T);
  }
  FeedBuffer(Path, (void*)DestNode);
  
  return DestNode->PathDistance;
}

void PRTask(){
  
  int PRID = MyParentTid();
  int ClkID = WhoIs("ClockServer");
  int MyID = MyTid();
  const int* Universal = GetShared();
  
  int i,j;
  PRMsg SendMsg, ReplyMsg;
  
  int Len1, Len2, Len3, Len4;
  Train* Trn;
  TrackNode* DestNode, *Trk;
  
  Buffer Path1;
  int P1Storage[TRACK_MAX];
  InitBuffer(&Path1,(void *)P1Storage,(int)(TRACK_MAX));
  
  Buffer Path2;
  int P2Storage[TRACK_MAX];
  InitBuffer(&Path2,(void *)P2Storage,(int)(TRACK_MAX));
  
  Buffer Path3;
  int P3Storage[TRACK_MAX];
  InitBuffer(&Path3,(void *)P3Storage,(int)(TRACK_MAX));
  
  Buffer Path4;
  int P4Storage[TRACK_MAX];
  InitBuffer(&Path4,(void *)P4Storage,(int)(TRACK_MAX));
  
  int min;
  FOREVER {
    SendMsg.Type = PRTASK;
    PRINTAT(44,80,"PRTask: BEFORE");
    Send(PRID, (void*)&SendMsg, sizeof(PRMsg), (void*)&ReplyMsg, sizeof(PRMsg));
    //Printf(COM2,"%s%c[%d;%dHPRTask: dest: %d %s",SAVE_CURSOR,ESC,45,80,ReplyMsg.Dest,RESTORE_CURSOR);
    
    i = 0, j = 0;
    Trn = ReplyMsg.Trn;
    Trn->MyPR = MyID;
    Trk = Trn->LocBase - (Trn->LocBase->num);
    DestNode = Trn->LocBase + (ReplyMsg.Dest - (Trn->LocBase->num));
    Len1 = Dijkstra(Trk, Trn->LocBase->num, ReplyMsg.Dest,&Path1);
    min = Len1;
    SendMsg.Buf = &Path1;

    
    //Printf(COM2,"%s%c[%d;%dHPRTask: SRT: %s SRC:%s DN: %s size: %d%s",SAVE_CURSOR,ESC,45,80,Trk->name,Trn->LocBase->name,DestNode->name,sizeof(*(Trn->LocBase)),RESTORE_CURSOR);
    
    Len2 = Dijkstra(Trk, Trn->LocBase->reverse->num, ReplyMsg.Dest, &Path2);
    SendMsg.Buf = (min < Len2 && min >= 0)?  SendMsg.Buf:&Path2;
    min = (min < Len2 && min >= 0)? min:Len2;
    
    Len3 = Dijkstra(Trk, Trn->LocBase->reverse->num, DestNode->reverse->num, &Path3);
    SendMsg.Buf = (min<Len3 && min >= 0)? SendMsg.Buf:&Path3;
    min = (min < Len3 && min >= 0)? min:Len3;
    
    
    Len4 = Dijkstra(Trk, Trn->LocBase->num, DestNode->reverse->num, &Path4);
    SendMsg.Buf = (min<Len4 && min>= 0)? SendMsg.Buf:&Path4;
    min = (min < Len4 && min >= 0)? min:Len4;
    Printf(COM2,"%s%c[%d;%dH Min Path Len: %d, %d,%d,%d,%d%s",SAVE_CURSOR,ESC,50,80,min,Len1,Len2,Len3,Len4,RESTORE_CURSOR);
    Trn->MyPR = 0;
    // SendMsg.Type = PRTASK_RESULT;
    // Send(PRID, (void*)&SendMsg, sizeof(PRMsg), (void*)&ReplyMsg, sizeof(PRMsg));
  }
}

void PathRouter(){
  int Result,Dest;
  char ServerName[] = "PathServer";
  RegisterAs(ServerName);
  
  int MyID = MyTid();
  int MyIndex = GetMemoryIndexINT(MyID);
  Buffer** AllSendQ = GetAllSendQ();
  
  Buffer SendQ;
  int QStorage[MAX_NUM_TD];
  InitBuffer(&SendQ,(void*)QStorage,MAX_NUM_TD);
  AllSendQ[MyIndex] = &SendQ;
  
  Buffer WorkerQ;
  int WQStorage[5];
  InitBuffer(&WorkerQ,(void*)WQStorage, 5);
  
  
  int TaskToReceive, WorkerID;
  PRMsg ReceiveSlot,ReplySlot;
  Message ReceiveMsg;
  ReceiveMsg.Addr = (void *)&ReceiveSlot;
  
  
  Train* Trn;
  char bReceived = 0;

  AP Args;
  Buffer* Path;
  
  Args.arg0 = (void*)6;
  Args.arg1 = (void*)&TCPRCourier;
  WorkerID = Create(&Args);

  Args.arg0 = (void*)3;
  Args.arg1 = (void*)&PRTask;
  WorkerID = Create(&Args);

  //Printf(COM2,"%s%c[%d;%dH PRID:%d %s",SAVE_CURSOR,ESC,43,80,MyID,RESTORE_CURSOR);
  FOREVER{
    if(!bReceived){	
      Result = BufferFIFO(&SendQ,(void *)(&TaskToReceive));
      BufferPopHead(&SendQ);
    }
    if(Result == 0){
      if(bReceived == 0){
	Result = Receive(TaskToReceive,(void *)&ReceiveMsg,sizeof(PRMsg));
	TaskToReceive = ReceiveMsg.SenderID;
      }
      bReceived = 0;
      
      switch(ReceiveSlot.Type){
      case ORDER_FROM_TC: // sent by Courier
	Dest = ReceiveSlot.Dest;
	Trn = ReceiveSlot.Trn;
	//Printf(COM2,"%s%c[%d;%dHPRDest:%d len:%d%s",SAVE_CURSOR,ESC,42,80,Dest,WorkerQ.Length,RESTORE_CURSOR);
	if (WorkerQ.Length == 0) {
	  Args.arg0 = (void*)3;
	  Args.arg1 = (void*)&PRTask;
	  WorkerID = Create(&Args);
	} else {
	  Result = BufferFIFO(&WorkerQ, &WorkerID);
	  BufferPopHead(&WorkerQ);
	  ReplySlot.Dest = Dest;
	  ReplySlot.Trn = Trn;
	  Reply(WorkerID, (void*)&ReplySlot, sizeof(PRMsg));
	  Dest = -1;
	  Trn = NULL;
	  Path = NULL;
	}
	Result = 0;
	Reply(TaskToReceive,(void *)&Result,sizeof(int));
	break;
      case PRTASK:
	if (Dest == -1) FeedBuffer(&WorkerQ,(void*)TaskToReceive);
	else {
	  ReplySlot.Dest = Dest;
	  ReplySlot.Trn = Trn;
	  Reply(TaskToReceive, (void*)&ReplySlot, sizeof(PRMsg));
	  Dest = -1;
	  Trn = NULL;
	}
	break;
	// case PRTASK_RESULT: //worker 
	// 	ReplySlot.Type = PATH_RESULT;
	// 	ReplySlot.Buf = ReceiveSlot.Buf;
	// 	Reply(PRTCCourier, (void*)&ReplySlot, sizeof(PRMsg)); 
	// 	FeedBuffer(&WorkerQ,(void*)TaskToReceive);
	// 	break;
      }
      
    }else{
      bReceived = 1;
      Receive(-1, (void*)&ReceiveMsg,sizeof(PRMsg));
      TaskToReceive = ReceiveMsg.SenderID;
			Result = 0;
    }
  }
}

/*
int GoTo(int TCID,int ClkID,Train* T, Buffer* Path,int Len,const int* Universal) {
  
  //char TCName[] = "TCServer";
  //int TCID = WhoIs(TCName);
  TCMsg SendMsg;
  int i;
  TrackNode* Temp,*Next;
  TempLast
  TrackNode* Start = T->LocBase - (T->LocBase->num);
  int* Arr = Path->Storage;
  int D,Res,Ticks,StopTime;
  // if its less than 2 times stopdistance,  short move
  if(Len < (int)(2*(T->StopDist))){
    
    }
  StopTime = (Len - (int)((T->Dist)*2))/(T->DymVel);
  StopTime += (*Universal + 200);
  for(i=0;i<Path->Length;i++){
    //if(*Universal < (StopTime + 20) || (*Universal > (StopTime - 20))){
    // issue stop command
    //}
    if(Arr[i] == T->LocBase->num) continue;
    Temp = Start[Arr[i]];
    
    switch (Temp->type){
    case NODE_SENSOR:// to wait till this sensor is triggered
      Next = ((Temp->edge)[DIR_AHEAD].dest);
      if(Next->type == NODE_BRANCH){
	D = GetDist(T->LocBase,Temp);
	D -= T->LocOff;
	D -= 200;// train length
	Ticks = (int) (D/(T->DymVel));
	Ticks -= 3;// estimate delay
	Temp = Next;
	Delay(ClkID,Ticks);
	// flip all the switch before next sensor
	FlipSwitch(ClkID,T,Path);
      }else{
	SendMsg.Type = PR_SR;
	SendMsg.Num = T->TrainID;
	SendMsg.Offset = Arr[i];
	Send(TCID,(void *)&SendMsg,sizeof(TCMsg),(void *)&Res,sizeof(int));
      }
      break;
      //if(Arr[i]!=T->LocBase && (Temp->edge)){
    default:
      break;
    }
    
  }
  return 0;
}
*/
