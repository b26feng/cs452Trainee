#include <bwio.h>
#include <buffer.h>
#include <canvas.h>
#include <clock.h>
#include <error.h>
#include <interrupt-io.h>
#include <min-heap.h>
#include <nameserver.h>
#include <td-shared.h>
#include <timer.h>
#include <ts7200.h>
#include <types.h>
#include <syscall.h>


void TurnOnTimerIRQ(){
	//*(int *)(VIC1_BASE + INT_ENABLE) |= (1<<4|1<<5);
	*(int *)(VIC2_BASE + INT_ENABLE) |= (1<<19);
}

void ClockServer() {
  //bwprintf(COM2,"CLKSERV: I get run time\n\r");
  int Res, ClientID, ClockServerID, Index,  SenderID;
  int SysTick, DelayedTick;
  ClkMsg ReceiveMsg, ReplyMsg;
  Message ReceiveSlot;
  ReceiveSlot.Addr = (void *)(&ReceiveMsg);
  AP Args;
  MinHeap Heap;

  RecordShared((void*)&SysTick);
  //bwprintf(COM2,"CLKSERV: before register\n\r");
  char ClockServerName[] = "ClockServer";
  Res = RegisterAs(ClockServerName);
  if (Res == FAILURE) {
    //bwprintf(COM2, "ClockServer: Cannot register name.\n\r");
  }
  //bwprintf(COM2,"CLKSERV: registered\n\r");
  
  ClockServerID = MyTid();
  if (ClockServerID < 0) {
    bwprintf(COM2, "ClockServer: Call to MyTid failed.\n\r");
  }
  
  
  // init SendQ
  Buffer SendQ;
  Buffer** AllSendQ;
  int QStorage[MAX_NUM_TD];
  SendQ.Storage = (void*)(&QStorage);
  SendQ.Size = MAX_NUM_TD;
  SendQ.Length = 0;
  SendQ.Head = 0;
  SendQ.Tail = 0;
  
  AllSendQ = GetAllSendQ();
  Index = GetMemoryIndexINT(ClockServerID);
  AllSendQ[Index] = &SendQ; 
  
  
  HN HeapStorage[MAX_NUM_TD];
  Heap.Num = 0;
  Heap.Last = 0;
  Heap.Storage = (void*)HeapStorage;
  
  SysTick = 0; 
  
  Clock C;
  ClockInit(&C);
  //bwprintf(COM2,"Clock Started\n\r");
  
  int bReceived = 0;
  FOREVER {
    //ClientID = -1;
    if(!bReceived){
      Res = BufferFIFO(&SendQ, (void*)(&ClientID));
      BufferPopHead(&SendQ);
      //bwprintf(COM2,"CLK: SQ %d,%d\n\r",ClientID,Res);
    }
    if (Res == 0) {
      if(!bReceived){
	Res = Receive(ClientID, (void*)(&ReceiveSlot), sizeof(ReceiveMsg));
	ClientID = ReceiveSlot.SenderID;
	if (Res != sizeof(ReceiveMsg)) {
	  //bwprintf(COM2, "ClockServer.\n\r");
	  break;
	}
	//bwprintf(COM2,"CLK:R %d\n\r",ClientID);
      }
      bReceived = 0;
      //bwprintf(COM2,"CLK: G %d,%d\n\r",ClientID,ReceiveMsg.Type);
      switch(ReceiveMsg.Type) {
      case DELAY:
	DelayedTick = ReceiveMsg.Tick + SysTick;
	InsertMinHeap(&Heap, ClientID, DelayedTick);
	break;
      case TIME:
	ReplyMsg.Type = TIME;
	ReplyMsg.Tick = SysTick;
	Reply(ClientID, (void*)&ReplyMsg, sizeof(ReplyMsg));
	break;
      case NOTIFIER:
	//if (ClientID == NotifierID) {
	SysTick++;
	//bwprintf(COM2,"CLKS:Tick %d\n\r",SysTick);
	ClearAllTimer();
	//TurnOnTimerIRQ();
	ReplyMsg.Type = CONTINUE;
	Reply(ClientID, (void*)&ReplyMsg, sizeof(ReplyMsg));
	
	// if (SysTick % 10 == 0) TimeDisplay(SysTick);
	// loops the heap till minTask's DelayedTick is no longer smaller than SysTick ;;; first Time in Do will Fail to execute
	DelayedTick = SysTick;
	//bwputc(COM2,'B');
	while (Heap.Num>0){
	  Res = GetMinMinHeap(&Heap,&SenderID,&DelayedTick);
	  //bwprintf(COM2,"CLKSERV: heap while result: %d\n\r",Res);
	  if (DelayedTick <= SysTick){
	    RemoveMinMinHeap(&Heap);
	    ReplyMsg.Type = READY;
	    Reply(SenderID, (void*)&ReplyMsg, sizeof(ReplyMsg));
	  }else break;
	}
	//bwputc(COM2,'A');
	//}
	break;
      default:
	break;
	//bwprintf(COM2, "ClockServer: ReceiveMsg Type is undefined.\n\r");
      }
    }
    else {
      //bwprintf(COM2,"CLKSRV: picked %d\n\r",ClientID);
      //if (Res < 0) //bwprintf(COM2, "ClockServer: BufferEmpty.\n\r");
      //Pass();
      // bwprintf(COM2, "CK:-1\n\r");
      Receive(-1, (void*)&ReceiveSlot, sizeof(ReceiveMsg));
      bReceived = 1;
      Res = 0;
      ClientID = ReceiveSlot.SenderID;
      // bwprintf(COM2,"CLK:%d<-\n\r",ClientID);
    }
  }
}


void Client(int N) {
	int i, Res, ClockServerID, MyTID, TimeDelay, ParentID;
	ClkMsg SendMsg, ReplyMsg;
	char CSName[] = "ClockServer";
	char Name[] = "ClockClient0";

	Name[11] = N+'0';

	//bwprintf(COM2,"%s: Before Register\n\r",Name);
	Res = RegisterAs(Name);
	if (Res == FAILURE) {
		bwprintf(COM2, "ClockClient%d: Cannot register name.\n\r",N);
	} 

	MyTID = MyTid();
	ParentID = MyParentTid();

	SendMsg.Type = DELAY_REQUEST;
	//bwprintf(COM2,"%s: Before send to parent\n\r",Name);
	Res = Send(ParentID, (void*)(&SendMsg), sizeof(SendMsg), (void*)(&ReplyMsg), sizeof(ReplyMsg));
	//bwprintf(COM2,"%s: After Send to parent \n\r",Name);
	ClockServerID = WhoIs(CSName);
	if (ClockServerID == FAILURE) {
		bwprintf(COM2, "ClockClient: Lookup for Clock Server ID failed.\n\r");
	}
	//bwprintf(COM2,"%s: Got Clock Server id:%d\n\r",Name,ClockServerID);
	TimeDelay = ReplyMsg.Tick;
	for (i = 0; i < ReplyMsg.NumDelays; i++) {
		Res = Delay(ClockServerID, TimeDelay); //TODO delay is calling on myself right?
		if (Res == -1) {
			bwprintf(COM2, "ClockClient%d: Clock Server Task ID is invalid.",N);
		}
		else if (Res == -2) {
			bwprintf(COM2, "ClockClient%d: The delay was zero or negative.",N);
		}
		bwprintf(COM2, "ClockClient%d with TaskID %d has completed %d times of delay interval %d.\n\r",N, MyTID, i+1, TimeDelay);
	}

}

void ClockClient1() {
	Client(1);
	Exit();
}

void ClockClient2() {
	Client(2);
	Exit();
}

void ClockClient3() {
	Client(3);
	Exit();
}

void ClockClient4() {
	Client(4);
	Exit();
}

void TimeDisplay() {
  int Sec,Minute,Hour;
  int ClkId = WhoIs("ClockServer");
  Sec = Minute = Hour = 0;
  FOREVER{
    Delay(ClkId,100);
    Sec ++;
    if(Sec>=60){
      Minute++;
      Sec -= 60;
    }
    if(Minute>=60){
      Hour++;
      Minute -= 60;
    }
    Printf(COM2,"%s%c[%d;%dH%d:%d:%d%s",SAVE_CURSOR,ESC,16,14,Hour,Minute,Sec,RESTORE_CURSOR);
    //Printf(COM2,"%c[%d;%dH",ESC,9,14);
    //Printf(COM2, "%c7%c[%d;%dH%d:%d:%d:%d%c8", ESC,ESC,9,14,Hour,Minute,Sec,Dsec,ESC);
    //CURSOR(9,14);
    //Printf(COM2, "%d:%d:%d:%d", Hour,Minute,Sec,Dsec);
    //Printf(COM2,"%d:%d",Sec, Dsec);
    //Printf(COM2,"%s",RESTORE_CURSOR);
  }
  Exit();
}
