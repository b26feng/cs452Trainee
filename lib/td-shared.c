#include <td-shared.h>
#include <kernel.h>
#include <bwio.h>

int isTaskAvailable(int TaskID) {
  return TaskID >> 31;
}

int GetGeneration(TD* Task) {
  int TID = Task->TaskID;
  // shift right to get rid of free bit
  // shift left to get rid of memory index
  return ((TID << 1) >> 17);
}

int GetGenerationINT(int TaskID){
  return ((TaskID<<1)>>17);
}

// to see if this task is availale
int GetAvailability(TD* Task) {
  int mask = 1 <<31;
  return (mask & (Task->TaskID));
}

int GetMemoryIndexINT(int TaskID){
  int mask = (1<<16)-1;
  return (mask & TaskID);
}
  
// for sp calculation
int GetMemoryIndex(TD* Task){
  int index = GetMemoryIndexINT(Task->TaskID);
  //bwprintf(COM2,"GetMemoryIndex: TID: %d, index: %d\n\r",Task->TaskID,index);
  return index;
}
