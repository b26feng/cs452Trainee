#include <bwio.h>
#include <buffer.h>
#include <canvas.h>
#include <courier.h>
#include <clockserver-api.h>
#include <io-api.h>
#include <interrupt-io.h>
#include <nameserver.h>
#include <syscall.h>
#include <sensors.h>
#include <td-shared.h>
#include <types.h>
#include <ts7200.h>


void ProcessData (char* SensorInput,Buffer* Buf) {
  int i;
  char SensorChar;
  char Temp = 72;
  char offset = 0;
  //Printf(COM2, "%s%c[61;80HSPD%s", SAVE_CURSOR, ESC, RESTORE_CURSOR);
  for (i = 0; i < NUM_SENSORS_READ; i++) {
    SensorChar = SensorInput[i];
    while(offset <8){
      if(SensorChar & (128 >> offset)){
	FeedBufferByte(Buf,Temp + offset);
	//Printf(COM2,"%s%c[%d;%dH%d %d%s",SAVE_CURSOR,ESC,63,80+(i<<1),Temp,offset,RESTORE_CURSOR);
	//}else{
	//FeedBufferByte(Buf,0);
      }
      offset += 1;
    }
    Temp = (Temp+8)%80;
    offset = 0;
  }
  if(Buf->Length == 0) FeedBufferByte(Buf,0);
  // Printf(COM2,"In array:%d\n\r", Temp);
}

void SensorPrintTask () {//char* SensorChars, int* SensorNums) {
  SSMsg SendMsg, ReplyMsg;
  int i, ColPos,Hex,Ten,One;
  char* SensorInput;
  char SensorNums[80];
  // set all 0
  for(i = 0;i<80;i++){
    SensorNums[i] = 0;
  }
  Buffer PrintBuf;
  InitBuffer(&PrintBuf,(void*)SensorNums,80);
  
  char SensorChars[] = "AABBCCDDEE";
  char SensorServerName[] = "SensorServer";
  int SensorServerID = WhoIs(SensorServerName);//MyParentTid();
  char Cur;
  char Index;

  AP Args;
  Args.arg0 = (void *)0;
  Args.arg1 = (void *)&TCSensorCourier;
  Create(&Args);
  //	char ClkServerName[] = "ClockServer";
  //	int ClkServerID = WhoIs(ClkServerName);
  //Printf(COM2, "%s%c[60;80HSPT%s", SAVE_CURSOR, ESC, RESTORE_CURSOR);
  FOREVER {
    SendMsg.Type = SENSOR_QUERY;
    Send(SensorServerID, (void*)&SendMsg, sizeof(SSMsg), (void*)&ReplyMsg, sizeof(ReplyMsg));
    
    SensorInput = ReplyMsg.SensorInput;
    ProcessData(SensorInput,&PrintBuf);
    for (i = 0; i < NUM_SENSORS_DISPLAYED; i++) {
      Index = (PrintBuf.Length > 0)? (PrintBuf.Tail+PrintBuf.Size-1-i)%80:0;
      Cur = SensorNums[Index];
      //Printf(COM2, "%s%c[62;80H%d %d%s", SAVE_CURSOR, ESC,Index,Cur,RESTORE_CURSOR);
      ColPos = 11 + i*5;
      if(Cur == 0){
	Printf(COM2, "%s%c[26;%dH000%s", SAVE_CURSOR, ESC, ColPos, RESTORE_CURSOR);
      }else{
	Hex = Cur-((Cur>>4)<<4)+1;// Cur % 16 without divide/multiply/modulous
	Ten = Hex / 10;
	One = Hex % 10;
	Printf(COM2, "%s%c[26;%dH%c%d%d%s", SAVE_CURSOR, ESC, ColPos, SensorChars[Cur>>3],Ten,One, RESTORE_CURSOR);
      }
      //		Printf(COM2,"SPT %c%d\n\r",SensorInput[i],SensorNums[i]);	
    }
    PrintBuf.Length = 0;
    PrintBuf.Head = PrintBuf.Tail;
    
  }
  
}

void SensorServer() {
  
  char SensorServerName[] = "SensorServer";
  RegisterAs(SensorServerName);
  
  int SenderID, Res;
  SSMsg RcvMsg,ReplyMsg;
  Message RcvSlot;
  RcvSlot.Addr = (void *)(&RcvMsg);
  
  char* SensorInput;
  char bHasChar = 0;
  int i, ColPos,TimeStamp;
  AP Args;
  
  Buffer WL;
  int WLStorage[MAX_NUM_TD];
  InitBuffer(&WL,(void*)WLStorage, (int)(MAX_NUM_TD));
  
  Buffer** AllSendQ = GetAllSendQ();
  Buffer SendQ;
  int QStorage[MAX_NUM_TD];
  InitBuffer(&SendQ,(void *)QStorage,MAX_NUM_TD);
  int MyID = MyTid();
  int MyIndex = GetMemoryIndexINT(MyID);
  AllSendQ[MyIndex] = &SendQ;
  
  char bReceived = 0;

  int counter = 0;
  FOREVER {
    if(!bReceived){
      Res = BufferFIFO(&SendQ, (void*)(&SenderID));
      BufferPopHead(&SendQ);
    }
    if (Res == 0) {
      if (bReceived == 0){
	Res = Receive(SenderID, (void*)&RcvSlot, sizeof(SSMsg));
	SenderID = RcvSlot.SenderID;
      }
      bReceived = 0;
      switch (RcvMsg.Type){
      case SENSOR_QUERY:
	FeedBuffer(&WL, (void*)SenderID);
	break;
      case SENSOR_NOTIFIER:
	//counter++;
	SensorInput = RcvMsg.SensorInput;
	TimeStamp = RcvMsg.TimeStamp;
	//Printf(COM2,"%s%c[%d;%dH%d: %d%s",SAVE_CURSOR,ESC,18,80,SensorInput,counter,RESTORE_CURSOR);
	//if(SensorInput[2]){
	//Printf(COM2,"%s%c[%d;%dH%d%s",SAVE_CURSOR,ESC,19,80,SensorInput[2],RESTORE_CURSOR);
	//}
	
	Res = 0;
	Reply(SenderID, (void*)&Res, sizeof(int));
	/*for(i = 0;i<10;i++){
	  bHasChar |= SensorInput[i];
	  if(bHasChar) break;
	  }*/
	//if(bHasChar){
	  ReplyMsg.SensorInput = SensorInput;
	  ReplyMsg.TimeStamp = TimeStamp;
	  while (WL.Length > 0) {
	    BufferFIFO(&WL, (void*)&SenderID);
	    Reply(SenderID, (void*)&ReplyMsg, sizeof(SSMsg));
	    BufferPopHead(&WL);
	  }
	  //bHasChar = 0;
	  //}
	break;
      default:
	break;
	
      }
    }
    else {
      bReceived = 1;
      Receive(-1, (void*)&RcvSlot, sizeof(SSMsg));
      SenderID = RcvSlot.SenderID;
      Res = 0;
    }
  }
}

void SensorNotifier() {
  int i;
  char Sensors[NUM_SENSORS_READ];
  SSMsg SendMsg;
  int ReplyMsg;

  int Myid = MyTid();
  char bRead = 0;
  char SensorServerName[] = "SensorServer";
  int SensorServerID = WhoIs(SensorServerName);
  int ClkID = WhoIs("ClockServer");
  int Num = 0;
  int counter = 0;
  const int* Universal = GetShared();
  Printf(COM2,"%s%c[%d;%dHSensor Notifier On%s",SAVE_CURSOR,ESC,20,80,RESTORE_CURSOR);
  FOREVER {
    Printf(COM1, "%c%c",SENSOR_RESET,SENSOR_REQUEST);
    //Putc(COM1,SENSOR_RESET);

    //Putc(COM1,SENSOR_REQUEST);
    SendMsg.TimeStamp = *Universal;
    /*
    for (i = 0; i < NUM_SENSORS_READ; i++) {
      Sensors[i] = Getc(COM1);
      }*/
    while(Num != 10){
    //GetStr(COM1,Sensors,10);
      Num += GetStr(COM1,(Sensors+Num),10-Num);
    //Printf(COM2,"%s%c[%d;%dH%d Num:%d,%s",SAVE_CURSOR,ESC,23,80,Myid,Num,RESTORE_CURSOR);
    }
    Num = 0;
    //Printf(COM2,"%s%c[%d;%dH%d:%d,%d,%d,%d,%d,%d,%d%s",SAVE_CURSOR,ESC,21,80,counter++,Sensors[0],Sensors[1],Sensors[2],Sensors[3],Sensors[4],Sensors[5],Sensors[6],RESTORE_CURSOR);

    SendMsg.Type = SENSOR_NOTIFIER;
    SendMsg.SensorInput = Sensors;
    for(i=0;i<10;i++){
      bRead |= Sensors[i];
    }
    if(bRead){
      Send(SensorServerID, (void*)&SendMsg, sizeof(SSMsg), (void*)&ReplyMsg, sizeof(int));
      bRead = 0;
    }
    Delay(ClkID,10);
  }	
  
}
