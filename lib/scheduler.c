#include <min-heap.h>
#include <scheduler.h>
#include <priority-q.h>
#include <td-shared.h>
#include <bwio.h>

int pushToScheduler (KernelStruct* Colonel, TD* Task) {
  InsertMinHeap(Colonel->Scheduler,Task->TaskID,Task->NextPriority);
  //Task->NextPriority = Task->TaskPriority;
  //int returncode= PushToTDPQ(&(Colonel->ArrayPQ)[Task->TaskPriority],Task);
  //bwprintf(COM1,"PUSH: pushed %d(prio: %d) to scheuler result%d\n\r",Task->TaskID,Task->TaskPriority,returncode);
  return 0;
}

void LoadNextTask(KernelStruct* Colonel){
  int TaskID,Priority;
  if(Colonel->Scheduler->Num == 0){
    Colonel->Active = NULL;
  }else if(GetMinMinHeap(Colonel->Scheduler,&TaskID,&Priority)){
    RemoveMinMinHeap(Colonel->Scheduler);
    if(!isTaskAvailable(TaskID)){
      //if(GetGenerationINT(TaskID) > 0 && GetMemoryIndexINT(TaskID)==18) bwputx(COM2,TaskID);
      Colonel->Active = (Colonel->Tasks)+ (GetMemoryIndexINT(TaskID));
      //bwprintf(COM2,"LNT: %d\n\r",(Colonel->Tasks)+ (GetMemoryIndexINT(TaskID)));
    }
    //bwprintf(COM2,"LNT:%d, %d\n\r",TaskID,Priority);
 
  }
}


/*
TD* getNextTaskScheduler (KernelStruct* Colonel) {
  TD *Task;
  TD *First;
  int i = 0;
  TDPQ *PQ;
  int Len;
  for(; i < MAX_NUM_PRIORITY; i++){
    PQ = &(Colonel->ArrayPQ)[i];
    //bwprintf(COM1,"scheduler: priority %d, Len:%d\n\r",i,PQ->Length);
    if(TDPQReady(PQ) && PQ->Length >0){
      Len = PQ->Length;
      First = NULL;
      Task = TDPQGetStart(PQ);
      TDPQPopStart(PQ);
      //bwprintf(COM1,"scheduler: Task %d, Ready %d,First %d",Task->TaskID,Task->TaskState,First);
      while ((Len > 0) && Task->TaskState != Ready) {
	if(First == Task) break;
	if(First == NULL) First = Task;
	PushToTDPQ(PQ, Task);
	Task = TDPQGetStart(PQ);
	TDPQPopStart(PQ);
	Len --;
      }
      if(Task != First){
	//bwprintf(COM1,"scheduler: picked Task%d, state:%d, prio: %d\n\r",Task->TaskID,Task->TaskState,i);
	return Task;
      }
    }
  }
  return NULL;
}
*/
