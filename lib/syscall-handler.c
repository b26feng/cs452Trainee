#include <buffer.h>
#include <bwio.h>
#include <context-switch.h>
#include <event-itr.h>
#include <ipc.h>
#include <IRQ.h>
#include <priority-q.h>
#include <scheduler.h>
#include <syscall-handler.h>
#include <td-kernel.h>
#include <ts7200.h>
#include <types.h>
#include <timer.h>
#include <interrupt-io.h>

void handlerAwaitEvent(KernelStruct* Colonel) {
  AP* Arg;
  int EventID, Res;
  
  Arg = Colonel->Active->Args;
  EventID = (int)(Arg->arg0);
  //bwprintf(COM2,"handlerAwaitEvent: EventID %d\n\r",EventID);
  Res = doAwaitEvent(Colonel, EventID);
  Colonel->Active->RetVal = Res;
  Colonel->Active = NULL;
  if (Res != SUCCESS) bwprintf(COM1, "handlerAwaitEvent: do AwaitEvent failed.\n\r");

  // Colonel->Active->RetVal = Res;
}

void handlerCreate(KernelStruct* Colonel) {

  AP *Args = Colonel->Active->Args;
  int A0 = (int)Args->arg0;
  int A1 = (int)Args->arg1;

  int P = A0;
  TD* NewTask = CreateTask(Colonel,P,(void *)A1);
  
  pushToScheduler(Colonel, NewTask);

  
  Colonel->Active->RetVal = NewTask->TaskID;
  // if (NewTask->TaskID >= 13) bwprintf(COM2,"HC %d\n\r", NewTask->TaskID);

}

int handlerSend(KernelStruct* Colonel) {
  int result;
  AP* Args = Colonel->Active->Args;

  int A0 = (int)Args->arg0;  
  int A2 = (int)Args->arg2;
  int A4 = (int)Args->arg4;
  //bwprintf(COM1,"handlerSend: task: %d doSend\n\r",Colonel->Active->TaskID);
  result = doSend(Colonel, A0, Args->arg1, A2, Args->arg3,A4);
  return result;
}
 
int handlerReceive(KernelStruct* Colonel) {
  AP* Args = Colonel->Active->Args;
  int A0 = (int)Args->arg0;
  int A2 = (int)Args->arg2;
  /*if(Colonel->Active->TaskID == 13){
    bwprintf(COM2,"hR: break\n\r");
    }*/
  int Result = doReceive(Colonel, A0, Args->arg1, A2);
  return Result;
}

int handlerReply(KernelStruct* Colonel) {
  AP* Args = Colonel->Active->Args;
  
  int A0 = (int)Args->arg0; 
  int A1 = (int)Args->arg1; 
  int A2 = (int)Args->arg2;
  
  void* Reply = (void*)A1;
  int ReplyLen = (int)A2;
  
  int Result = doReply(Colonel, A0, Reply, ReplyLen);
  //bwprintf(COM2,"handlerReply: doReply:%d\n\r",Result);
  return Result;
}

void handlerSendWrapper(KernelStruct* Colonel,int ServerID){
  AP* Args = Colonel->Active->Args;
  //bwprintf(COM2,"handlerWhoIsRegas: Message :%s\n\r",((NSReq *)(Args->arg1))->Msg);
  //bwprintf(COM2,"handlerWhoIsRegas: MsgType:%d\n\r",((NSReq *)(Args->arg1))->MsgType);
  //bwprintf(COM2,"SendWrapper: Server ID %d\n\r",ServerID);
  int result = doSend(Colonel,ServerID,Args->arg1,(int)Args->arg2,Args->arg3,(int)Args->arg4);
  Colonel->Active->RetVal = result;
}

void handlerGetAllSendQ(KernelStruct* Colonel){
  //AP* Args = Colonel->Active->Args;
  Colonel->Active->RetVal = (int)(Colonel->AllSendQ);
}

void handlerDestroy(KernelStruct* Colonel,TD* Task)
{
  Colonel->Active = NULL;
  int newTID = Task->TaskID;
  newTID |= ((newTID >> 16)+1)<<16;
  newTID |= 1<<31;
  Task->TaskID = newTID;
}

/*
void TurnOffIRQ(int IRQ1, int IRQ2){
  asm volatile(".VIC1ENCLR:\n\t"
	       ".word 0x800b0014\n\t"
	       ".VIC2ENCLR:\n\t"
	       ".word 0x800c0014\n\t"
	       "ldr r2,.VIC1ENCLR\n\t"
	       "ldr r3,.VIC2ENCLR\n\t"
	       "str r0, [r2]\n\t"
	       "str r1, [r3]\n\t");
}
*/

int Handle(KernelStruct* Colonel, int n) {
//	bwprintf(COM2,"Handle: n=%d\n\r",n);
  int result;
  AP* Arg;
  IOReq* IOR;
  int TaskID;
  switch(n) {
  case IRQ_MAGIC:
    // bwprintf(COM2,"Handle: IRQ!!!");
    // Putc(COM2,'H');
    pushToScheduler(Colonel,Colonel->Active);
    Colonel->Active = NULL;
    int *IRQ2 = (int *)(VIC2_BASE);
    int *IRQ1 = (int *)(VIC1_BASE);
    int EventID = GetEventID(*IRQ1,*IRQ2);
    int Mask = 1;
    // bwprintf(COM2,"H: %d\n\r", EventID);

    //bwprintf(COM1,"Handle: int IRQ1: %d, int IRQ2: %d\n\r",*(int *)(VIC1_BASE | 0x10),*(int *)(VIC2_BASE | 0x10));
    if(EventID >= 32){
      Mask <<= (EventID-32);
      //bwprintf(COM2,"Handle: IRQ2 :%d\n\r",(*(int *)(VIC2_BASE | INT_ENABLE)));
      if(!(EventID == UART1_INT)){
	ClearIRQ(Mask,IRQ2);
	//bwprintf(COM2,"Handle: event %d, disable irq\n\r",EventID);
      }
      //bwprintf(COM2,"Handle: IRQ2 :%d\n\r",(*(int *)(VIC2_BASE | INT_ENABLE)));
      //bwprintf(COM1,"Handle: Disabled IRQ2: %d\n\r",*(int *)(VIC2_BASE | 0x10));
    }else{
      Mask <<= EventID;
      ClearIRQ(Mask,IRQ1);
      //bwprintf(COM1,"Handle: Disabled IRQ1: %d\n\r",*(int *)(VIC1_BASE | 0x10));
    }
    //bwprintf(COM1,"Handle: int IRQ1: %d, int IRQ2: %d\n\r",*(int *)(VIC1_BASE | 0x10),*(int *)(VIC2_BASE | 0x10));
    // recursively process all events
    //HandleEvents(Colonel, IRQ2,IRQ1);
    // if (EventID == 51) bwprintf(COM2,"Handle: %d\n\r",EventID);
    ProcessEvent(Colonel,EventID);
    break;
  case SYS_Create:
    //bwprintf(COM2,"before, Args = %d\n\r",Args);
    handlerCreate(Colonel); 
    break;
  case SYS_MyTid:
    (Colonel->Active)->RetVal = (Colonel->Active)->TaskID;
    //pushToScheduler(Colonel,Colonel->Active);
    //Colonel->Active = NULL;
    break;
  case SYS_ParentTid:
    (Colonel->Active)->RetVal = (Colonel->Active)->ParentID;
    // pushToScheduler(Colonel,Colonel->Active);
    //Colonel->Active = NULL;
    break;
  case SYS_Pass:
    //bwprintf(COM2,"Handle: Task %d Passed\n\r",Colonel->Active->TaskID);
    //PushToTDPQ(&((Colonel->ArrayPQ)[MAX_NUM_PRIORITY-2]),Colonel->Active);
    Colonel->Active->NextPriority = MAX_NUM_PRIORITY -2;
    pushToScheduler(Colonel,Colonel->Active);
    Colonel->Active = NULL;
    break;
  case SYS_Exit:
    (Colonel->Active)->TaskState = Zombie;
    Colonel->Active = NULL;
    break;
  case SYS_Send:
    result = handlerSend(Colonel);
    break;
  case SYS_Receive:
    /*if(Colonel->Active->TaskID == 13){
      bwprintf(COM2,"Handle: break\n\r");
      }*/
    result = handlerReceive(Colonel);
    //bwprintf(COM2,"Handle: handlerReceive: %d\n\r",result);
    break;
  case SYS_Reply:
    result = handlerReply(Colonel);
    break;
  case SYS_WHOIS:
  case SYS_REGAS:
    handlerSendWrapper(Colonel,Colonel->NameServerID);
    break;
  case SYS_SENDQ:
    handlerGetAllSendQ(Colonel);
    break;
  case SYS_WAITEVENT:
    handlerAwaitEvent(Colonel);
    break;
  case SYS_GETC:
    Arg = Colonel->Active->Args;
    IOR = (IOReq*)Arg->arg1;
    TaskID =(IOR->Chan)? Colonel->IServer2ID:Colonel->IServer1ID;
    handlerSendWrapper(Colonel,TaskID);
    break;
  case SYS_PUTC:
    //bwprintf(COM2,"Handle: putc\n\r");
    Arg = Colonel->Active->Args;
    IOR = (IOReq*)Arg->arg1;
    TaskID =(IOR->Chan)? Colonel->OServer2ID:Colonel->OServer1ID;
    handlerSendWrapper(Colonel,TaskID);
    break;
  case SYS_SHARED:
    Colonel->Shared = Colonel->Active->Args->arg1;
    break;
  case SYS_GETSHARED:
    Colonel->Active->RetVal = (int )(Colonel->Shared);
    break;
  case SYS_QUIT:
    Colonel->Active = NULL;
    Colonel->Scheduler->Num = 0;
    Colonel->Scheduler->Last = 0;
    //bwprintf(COM2,"HD: quit\n\r");
    break;
  case SYS_SDES:
    handlerDestroy(Colonel,Colonel->Active);
    break;
  }
  //bwprintf(COM2,"Handle: Before returning\n\r");
  return 0;
}

void fakeExit(TD* Task){
  asm volatile("ldmfd r0, {r0-r3}\n\t");
  register int r1 asm("r0");
  int one = r1;
  register int r2 asm("r1");
  int lr = r2;
  register int r3 asm("r2");
  int two = r3;
  register int r4 asm("r3");
  int zero = r4;
  
  //bwprintf(COM2,"Fake Exit:r1 %d, lr %d, r2 %d, r0 %d\n\r",one,lr,two,zero);
}

int Activate(KernelStruct *Colonel,TD* Task) {
  Task->NextPriority = Task->TaskPriority;
  //if(Colonel->Active == NULL) Colonel->Active = Task;
	//fakeExit(Task);
//	bwprintf(COM2, "Activate: about to kerxit\n\r");
	//  bwprintf(COM2,"Activate: sp %d, lr %d\n\r",Task->sp, Task->lr);
	kerxit(Task);
	//register int r1 asm("r1");
	//((Colonel->Active)->Arg) =(AP *)(r1);
	register int r0 asm("r0");
	int SWI = r0;
	//bwprintf(COM2,"Actvate: SWI number %d\n\r",SWI);
//	bwprintf(COM2,"Back to activate, SWI=%d\n\r", SWI);
	return SWI;
	//return r0;
}