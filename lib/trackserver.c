#include <buffer.h>
#include <bwio.h>
#include <canvas.h>
#include <courier.h>
#include <nameserver.h>
#include <syscall.h>
#include <td-shared.h>
#include <types.h>
#include <ts7200.h>
#include <track_data.h>
#include <trackserver.h>

void TrackServer(){
  //Printf(COM2,"TrackServer\n\r");
  int Result;
  // Registration
  char ServerName[] = "TrackServer";
  Result = RegisterAs(ServerName);

  const int *Universal = (int *)(GetShared());
  int MyID = MyTid();
  int MyIndex = GetMemoryIndexINT(MyID);
  Buffer** AllSendQ = GetAllSendQ();

  Buffer SendQ;
  int QStorage[MAX_NUM_TD];
  InitBuffer(&SendQ,(void*)QStorage,MAX_NUM_TD);
  AllSendQ[MyIndex] = &SendQ;
  
  TrackNode Track[TRACK_MAX];
  
  int TaskToReceive,Index,T1,T2,i;
  TSMsg ReceiveSlot,ReplySlot;
  Message ReceiveMsg;
  ReceiveMsg.Addr = (void *)&ReceiveSlot;

  char bReceived = 0;
  char bTrackInit = 0;
  char TrackSwitch = -1;

  int SelfAndNext[10];
  int TrainsToUpdate[5] = {0,0,0,0,0};
  int* SenderInfo;

  int Courier;

  AP Args;
  Args.arg0 = 8;
  Args.arg1 = (void *)(&TSTCCourier);
  Create(&Args);

  FOREVER{
    if(!bReceived){	
      Result = BufferFIFO(&SendQ,(void *)(&TaskToReceive));
      BufferPopHead(&SendQ);
    }
    if(Result == 0){
      if(bReceived == 0){
	Result = Receive(TaskToReceive,(void *)&ReceiveMsg,sizeof(TSMsg));
	TaskToReceive = ReceiveMsg.SenderID;
      }
      bReceived = 0;
      TrackSwitch = ReceiveSlot.TrackAB;
      //Printf(COM2,"TSServer: ID:%d type:%d switch:%d\n\r",MyID,ReceiveSlot.Type,TrackSwitch);
      switch(ReceiveSlot.Type){
      case FlipSwitch:
	TrackSwitch = (TrackSwitch == 33 || TrackSwitch=='s' || TrackSwitch =='S')? DIR_STRAIGHT:DIR_CURVED;
	Index = (int)ReceiveSlot.Src;
	// check which trains need to update
	
	Track[Index].State = TrackSwitch;
	Track[Index+1].State = TrackSwitch;
	if(Track[Index].TrainOwn) FreeFromSwitch(&Track[Index]);
	//Printf(COM2,"%s%c[%d;%dHI:%d,S:%d,st:%d%s",SAVE_CURSOR,ESC,38,80,Index,TrackSwitch,Track[Index].State,RESTORE_CURSOR);
	Reply(TaskToReceive,(void*)&Result,sizeof(int));
	ReplySlot.Src = &Track[Index];
	Reply(Courier,(void *)&ReplySlot,sizeof(TSMsg));
	Courier = 0;
	break;
      case InitTrack:
	//Printf(COM2,"TSServer:InitTrack %d\n\r",TrackSwitch);
	Result = -4;
	if(TrackSwitch == 'A'){
	  //CURSOR(31,80);
	  //PRINTAT(40,80"Initializing Track A . . .")
	  //Printf(COM2,"Initializing Track A . . .");
	  T1 = *Universal;
	  init_tracka(Track);
	  T2 = *Universal;
	  Result = 0;
	  //CURSOR(32,80);
	  //Printf(COM2,"Track A Ready! Time spent: %d ticks",T2-T1);
	}else{
	  //CURSOR(31,80);
	  //Printf(COM2,"Initializing Track B . . .");
	  T1 = *Universal;
	  init_trackb(Track);
	  T2 = *Universal;
	  Result = 0;
	  //CURSOR(32,80);
	  //Printf(COM2,"Track B Ready! Time spent: %d ticks",T2-T1);
	}
	for(i=0;i<TRACK_MAX;i++){
	  Track[i].TrainOwn = 0;
	}
	bTrackInit = 1;
	Reply(TaskToReceive,(void*)&Result,sizeof(int));
	break;
	
      case TCTS_GETNODE:
	SenderInfo = (int *)ReceiveSlot.Addr;
	for(i=0;i<5;i++){
	  SelfAndNext[i] = SenderInfo[i];
	  
	  SenderInfo[i+5] = (SenderInfo[i] == -1)? (int)NULL:(int)GetNextSensor(&Track[SenderInfo[i]],1);
	  //if(SenderInfo[i] != -1) Printf(COM2,"%s%c[%d;%dH %d %d is %d %s%s",SAVE_CURSOR,ESC,38,80,i,SenderInfo[i],Track[i].num,Track[i].name,RESTORE_CURSOR);
	  SenderInfo[i] = (SenderInfo[i] == -1)? (int)NULL:(int)&Track[SenderInfo[i]];
	}
	ReplySlot.Addr = (void *)(SenderInfo);
	Reply(TaskToReceive,(void*)&Result,sizeof(TSMsg));
	break;
      case TSTC_WAITSWITCH:
	Courier = TaskToReceive;
	break;
      }
      
    }else{
      bReceived = 1;
      Receive(-1, (void*)&ReceiveMsg,sizeof(TSMsg));
      TaskToReceive = ReceiveMsg.SenderID;
      Result = 0;
    }
  }
}
