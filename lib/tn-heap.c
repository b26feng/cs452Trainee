#include <types.h>
#include <tn-heap.h>
#include <interrupt-io.h>
#include <tools.h>
// #include <bwio.h>

int printHeap(MinHeap* Heap) {
	if (Heap == NULL || Heap->Num == 0) {
		// bwprintf(COM2,"Heap empty\n\r");
		return FAILURE;
	}
	int i;
	TrackNode** Storage = (TrackNode**)(Heap->Storage);
	for (i = 0; i < Heap->Num; i++) {
		TrackNode* Temp = Storage[i];
				// bwprintf(COM2,"crash after this line?\n\r");
		// bwprintf(COM2,"(%s:%d:%d) ", Temp->name, Temp->PathDistance, Temp);
		// bwprintf(COM2, "i = %d : %d \n\r",i, Storage[i]);
	}
	// bwprintf(COM2, "\n\r");
	return SUCCESS;
}

TrackNode* GetMinHeap(MinHeap* Heap) {
  if (Heap->Num <= 0) return 0;
  TrackNode** Storage = (TrackNode**)Heap->Storage;
//   MinNode = Storage[0];
//   bwprintf(COM2,"GMH Storage %d &Storage %d\n\r", Storage, &Storage);

// int i =0;
// while(i < Heap->Num) {
// 	TrackNode* Temp = Storage[i];
// 	i++;
// 	// bwprintf(COM2,"GMH: Temp %d ", Temp);
// }

//    printHeap(Heap);
//   bwprintf(COM2,"GMH: Storage[0] %d\n\r", Storage[0]);
  return Storage[0];
}

// One Tough Cookie: Move Last to Top then Bubble Down
void RemoveMinHeap(MinHeap* Heap) {

	TrackNode** Storage = (TrackNode**)(Heap->Storage);
	TrackNode* LastNode = Storage[Heap->Last-1];
	TrackNode* MinNode = Storage[0];
	// bwprintf(COM2,"RMH: Heap->Last=%d \n\r", Heap->Last);
	// bwprintf(COM2,"RMH: MinNode %d \n\r", MinNode);

	// bwprintf(COM2,"RMH: LastNode %d \n\r", LastNode);

	MinNode = NULL;
	// *MinNode = *LastNode;
	Storage[0] = LastNode;
    // if (Storage[0]==NULL)bwprintf(COM2,"Storage[0] first time %d \n\r");
	// bwprintf(COM2,"RMH Storage[0] %d \n\r", Storage[0]);

	Heap->Num -= 1;
	Heap->Last -= 1;

	// bwprintf(COM2,"RemoveMinHeap: before bbd, Num %d ", Heap->Num);
	// bwprintf(COM2,"RMH ");
	// printHeap(Heap);
	//bwprintf(COM2,"RM: ")
	if (Heap->Num > 1) BubbleDownHeap(Heap, 0); 
	// bwprintf(COM2,"RMH ");
	// printHeap(Heap);
}

// inserts into Min Heap
void InsertHeap(MinHeap* Heap, TrackNode* NewNode) {
  //bwprintf(COM2,"HInsert:%d\n\r",TaskID);
	TrackNode** Storage= (TrackNode**)(Heap->Storage);

	Storage[Heap->Last] = NewNode;
    // bwprintf(COM2,"Storage[Heap->Lastttt] %d Heap->Last %d\n\r", Storage[Heap->Last], Heap->Last);
	Heap->Num += 1;
	// printHeap(&Heap); //TODO why is this line wrong?
	if (Heap->Num > 1)BubbleUpHeap(Heap, Heap->Last);
	Heap->Last += 1;
}

void BubbleDownHeap(MinHeap* Heap, int StartIndex) {
    // bwprintf(COM2,"bbd \n\r");

	int LeftChildIndex, RightChildIndex, NextIndex;
	TrackNode* Temp;

	LeftChildIndex = StartIndex*2 + 1;
	RightChildIndex = StartIndex*2 + 2;

	TrackNode** Storage = (TrackNode**)(Heap->Storage);
	TrackNode* Cur = Storage[StartIndex];
	TrackNode* Left = Storage[LeftChildIndex];
	TrackNode* Right = Storage[RightChildIndex];

	Temp = Cur;
	if (Right == NULL || (Left->PathDistance < Right->PathDistance)) { // Right Branch is NULL
		if (Cur->PathDistance > Left->PathDistance && Left != NULL) {
			// bwprintf(COM2,"actual bbd left \n\r");
			// *Cur = *Left; // TODO hmm shallow copy?
			Storage[StartIndex]=Left;
			Storage[LeftChildIndex]=Temp;
			// *Left = Temp;
			NextIndex = LeftChildIndex;
		}
	}
	else {

		if (Cur->PathDistance > Right->PathDistance) {
			// bwprintf(COM2,"actual bbd right\n\r");
			// *Cur = *Right;
			Storage[StartIndex]=Right;
			// *Right = Temp;
			Storage[RightChildIndex]=Temp;
			NextIndex = RightChildIndex;
		}
	}
	//bwprintf(COM2,"BDW:");
	//printMinHeap(Heap);
	if ((2*NextIndex+1) < Heap->Last) BubbleDownHeap(Heap, NextIndex);
}

void BubbleUpHeap(MinHeap* Heap, int Last) {
    // bwprintf(COM2,"bbu ");
	// printHeap(&Heap);
	int ParentIndex = (Last-1)/2;
	TrackNode *Temp;

	if (ParentIndex < 0) return;
	TrackNode** Storage = (TrackNode**)(Heap->Storage);
	TrackNode* Parent = Storage[ParentIndex];
	TrackNode* LastNode = Storage[Last];
	// bwprintf(COM2, "Parent %d LastNode %d\n\r", Parent, LastNode);

	if (Parent->PathDistance > LastNode->PathDistance) {
		// Temp = *Parent;
		// *Parent = *LastNode;
		// *LastNode = Temp;
		Temp = Parent; 
		// bwprintf(COM2, "Temp %d \n\r", Temp);
		Storage[ParentIndex]= LastNode;
		// bwprintf(COM2, "Storage[PI] %d \n\r", Storage[ParentIndex]);
		Storage[Last] = Temp;
		// bwprintf(COM2, "Storage[Last] %d \n\r", Storage[Last]);

	}
	if (ParentIndex != 0) BubbleUpHeap(Heap, ParentIndex);
}

void DecreaseKey(MinHeap* Heap, TrackNode* NewDistNode) {
	// bwprintf(COM2,"in decrease key \n\r");
	int Index = 0;
	TrackNode** Storage = (TrackNode**)(Heap->Storage);

	while (Index < Heap->Num) {
		if (stringCompare(Storage[Index]->name, NewDistNode->name) == 0) break;
		Index++;
	}
	Storage[Index]->PathDistance = NewDistNode->PathDistance;
	BubbleUpHeap(Heap, Index);
}