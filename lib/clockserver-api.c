#include <types.h>
#include <clock.h>
#include <syscall.h>
#include <bwio.h>


int Delay (int TaskID, int Ticks) {
  int Res;
  ClkMsg SendMsg, ReplyMsg;
  
  if (Ticks <= 0) {
    bwprintf(COM2, "Delay: The delay was zero or negative.\n\r");
    return NON_POSITIVE_DELAY;
  }
  
  
  SendMsg.Type = DELAY;
  SendMsg.Tick = Ticks;
  
  //bwprintf(COM2,"Delay: Send To %d, Ticks %d\n\r",TaskID, Ticks);
  Res = Send(TaskID, (void*)(&SendMsg), sizeof(SendMsg), (void*)(&ReplyMsg), sizeof(ReplyMsg));
  if (Res == TASK_DNE) {
    bwprintf(COM2, "Delay: The Clock Server Task ID is invalid \n\r.");
    return CLOCK_SERVER_ID_INVALID;
  }
  else if (Res == MSG_TRUNCATED || Res == SRR_FAILED) { 
    bwprintf(COM2, "Delay: Send failed.\n\r");
    return FAILURE;
  }
  return SUCCESS;
}

int Time (int TaskID) {
  int Res;
  ClkMsg SendMsg, ReplyMsg;
  SendMsg.Type = TIME;

  //bwprintf(COM2,"Time: before send\n\r");
  Res = Send(TaskID, (void*)(&SendMsg), sizeof(SendMsg), (void*)(&ReplyMsg), sizeof(ReplyMsg));
  if (Res == TASK_DNE) {
    bwprintf(COM2, "Time: The Clock Server Task ID is invalid \n\r.");
    return CLOCK_SERVER_ID_INVALID;
  }
  else if (Res == MSG_TRUNCATED || Res == SRR_FAILED) { 
    bwprintf(COM2, "Time: Send failed.\n\r");
    return FAILURE;
  }
  else return ReplyMsg.Tick;
}

int DelayUntil (int TaskID, int Ticks) {
  int Res, SysTick;
  
  if (Ticks <= 0) {
    bwprintf(COM2, "DelayUntil: The delay was zero or negative.\n\r");
    return NON_POSITIVE_DELAY;
  } 
  
  SysTick = Time(TaskID);
  
  if (SysTick <= Ticks) Res = Delay(TaskID, Ticks - SysTick + 1);
  else {
    return -2;
  }
  return Res;
}
