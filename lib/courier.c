#include <canvas.h>
#include <courier.h>
#include <nameserver.h>
#include <syscall.h>
#include <interrupt-io.h>
#include <types.h>

void TCTSCourier(){
  TCMsg TCSend,TCRep;
  TSMsg TSSend,TSRep;
  
  int TSID = WhoIs("TrackServer");
  int TCID = WhoIs("TCServer");
  int i;
  //int Store[10];
  TCSend.Type = TCTS_FIRST;
  PRINTAT(27,80,"FIRST Call");
  Send(TCID,(void*)&TCSend,sizeof(TCMsg),(void*)&TCRep,sizeof(TCMsg));
  
  int *ReplyArray = (int *)TCRep.Addr;
  Printf(COM2,"%s%c[%d;%dH%d,%d,%d,%d,%d%s",SAVE_CURSOR,ESC,28,80,ReplyArray[0],ReplyArray[1],ReplyArray[2],ReplyArray[3],ReplyArray[4],RESTORE_CURSOR);
  // to make it wait on tc for next sensors to quiry
  FOREVER{
    TSSend.Type = TCTS_GETNODE;
    TSSend.Addr = (void *)ReplyArray;
    Send(TSID,(void*)&TSSend,sizeof(TSMsg),(void*)&TSRep,sizeof(TSRep));
    // get the sensor update
    TCSend.Type = TCTS_ORDER;
    Send(TCID,(void*)&TCSend,sizeof(TCMsg),(void*)&TCRep,sizeof(TCRep));
    // make it wait on tc
  }
}


void TCSensorCourier(){
  TCMsg TCSend;
  SSMsg SSSend,SSRep;
  
  //PRINTAT(28,80,"TCS Call");
  int SSID = WhoIs("SensorServer");
  int TCID = WhoIs("TCServer");
  int i,TCRep;
  char Store[10];
  SSSend.Type = SENSOR_QUERY;
  //PRINTAT(27,80,"FIRST Call");
  //Printf(COM2,"%s%c[%d;%dH%d,%d,%d,%d,%d%s",SAVE_CURSOR,ESC,28,80,ReplyArray[0],ReplyArray[1],ReplyArray[2],ReplyArray[3],ReplyArray[4],RESTORE_CURSOR);
  // to make it wait on tc for next sensors to quiry
  char *ReplyArray;
  FOREVER{
    
    Send(SSID,(void*)&SSSend,sizeof(SSMsg),(void*)&SSRep,sizeof(SSMsg));
    
    ReplyArray = (char *)SSRep.SensorInput;
    for(i = 0;i<10;i++){
      Store[i] = ReplyArray[i];
    }
    TCSend.Offset = SSRep.TimeStamp;
    TCSend.Type = TCS_UPDATE;
    TCSend.Addr = (void *)Store;
    //TCSend.Offset = TimeStamp;
    Send(TCID,(void*)&TCSend,sizeof(TCMsg),(void*)&TCRep,sizeof(int));
    // get the sensor update
    // make it wait on tc
  }
}

void TSTCCourier(){
  TCMsg TCSend;
  TSMsg TSSend,TSRep;
  
  int TSID = WhoIs("TrackServer");
  int TCID = WhoIs("TCServer");
  int i,TCRep;
  //int Store[10];
  //PRINTAT(27,80,"FIRST Call");
  //Printf(COM2,"%s%c[%d;%dH%d,%d,%d,%d,%d%s",SAVE_CURSOR,ESC,28,80,ReplyArray[0],ReplyArray[1],ReplyArray[2],ReplyArray[3],ReplyArray[4],RESTORE_CURSOR);
  // to make it wait on tc for next sensors to quiry
  FOREVER{
    
    TSSend.Type = TSTC_WAITSWITCH;
    Send(TSID,(void*)&TSSend,sizeof(TSMsg),(void*)&TSRep,sizeof(TSMsg));
    //Printf(COM2,"%s%c[%d;%dHTSTC:back from TS%s",SAVE_CURSOR,ESC,55,80,RESTORE_CURSOR);
    // copy all those pointers to local
    // first 5 are switch node,last 5 are next sensor extimation

    TCSend.Addr = (void *)TSRep.Src;
    TCSend.Type = TSTC_UPDATE;
    Send(TCID,(void*)&TCSend,sizeof(TCMsg),(void*)&TCRep,sizeof(int));
    // get the sensor update
    // make it wait on tc
  }
}

void TCSensorDog(){
  TCMsg TCSend,TCReply;
  
  int SSID = WhoIs("SensorServer");
  int TCID = WhoIs("TCServer");
  int ClkID = WhoIs("ClockServer");
  int i;
  const int* Universal = GetShared();
  
  TCSend.Type = DOGINIT;
  Send(TCID,(void *)&TCSend.Type,sizeof(TCMsg),(void *)&TCReply,sizeof(TCMsg));
  //TrainIndex = TCRep;
  TCSend.Num = TCReply.Num;
  FOREVER{
    TCSend.Type = DOGBACK;
    Printf(COM2,"%s%c[%d;%dH%d's dog wait till %d now %d %s",SAVE_CURSOR,ESC,35,80,TCReply.Num,TCReply.Offset,*Universal,RESTORE_CURSOR);
    if((*Universal) < (TCReply.Offset)){
      Delay(ClkID,(TCReply.Offset - *Universal)+10);
    }else{
      TCSend.Num = 99;
    }
    Send(TCID,(void *)&TCSend.Type,sizeof(TCMsg),(void *)&TCReply,sizeof(int));

  }
}

void TCPRCourier() {
  int PRID = MyParentTid();
  
  int TCID = WhoIs("TCServer");
  
  TCMsg SendMsg, ReplyMsg;
  PRMsg SendPR; 
  int ReplyPR;
  
  FOREVER {
    SendMsg.Type = GET_PF_ORDER;
    Send(TCID, (void*)&SendMsg, sizeof(TCMsg), (void*)&ReplyMsg, sizeof(TCMsg));

    //Printf(COM2,"%s%c[%d;%dHTCPR: PRID: %d %s",SAVE_CURSOR,ESC,52,80,PRID,RESTORE_CURSOR);
    SendPR.Type = ORDER_FROM_TC;
    SendPR.Trn = (Train*)ReplyMsg.Addr;
    SendPR.Dest = (int )ReplyMsg.Num; 
    // SendPR.Buf = (Buffer*) ReplyMsg.Offset;
    Send(PRID, (void*)&SendPR, sizeof(PRMsg), (void*)&ReplyPR, sizeof(int));
  }
  Exit();
}
