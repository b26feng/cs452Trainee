#include <syscall.h>
#include <context-switch.h>
#include <bwio.h>

int Create (AP* Args) {
	//bwprintf(COM2,"Create: Arg location:%d\n\r",Args);
	asm volatile ("swi 0\n\t" );
	//  bwprintf(COM2,"Back to Create\n\r");
	register int RetVal asm("r0");
	int Return = RetVal;
	//bwprintf(COM2,"Create: Created %d\n\r", Return);
	return Return;
}

int MyTid() {
	asm volatile ("swi 1\n\t");
	register int RetVal asm("r0");
	int R = RetVal;
	//bwprintf(COM2,"Back to MyTid, my tid is %d, My Address is %d\n\r",R);
	return R;
}

int MyParentTid() {
	asm volatile ("swi 2\n\t");
	register int RetVal asm("r0");
	return RetVal;

}

void Pass() {
  //bwprintf(COM2,"Pass: before swi\n\r");
  asm volatile ("swi 3\n\t");
}

void Exit() {
	//bwprintf(COM2,"Before SWI exit\n\r");
	asm volatile ("swi 4\n\t");
}

int ActualSend(AP* Args){
	asm volatile ("swi 5\n\t");
	register int RetVal asm("r0");
	int R = RetVal;
	return R;
}

// tid is that of the one its sending to
int Send (int TaskID, void* Msg, int MsgLen, void* Reply, int ReplyLen) {
  AP Args;
  Args.arg0 = (void *)(TaskID);
  Args.arg1 = Msg;
  //bwprintf(COM1,"Send_USR: %d Msg: %d\n\r",TaskID,Msg);
  Args.arg2 = (void *)(MsgLen);
  Args.arg3 = Reply;
  Args.arg4 = (void *)(ReplyLen);

  //int MyID = MyTid();
  //bwprintf(COM1,"Send: send to %d send MsgLen %d\n\r",TaskID,MsgLen);
  int Return = ActualSend(&Args);
  
  return Return;
}

int ActualReceive(AP* Args){
	asm volatile ("swi 6\n\t");
	register int RetVal asm("r0");
	int Return = RetVal;
	return Return;
}

int Receive (int TaskID, void* Msg, int MsgLen) {
  /*if(MsgLen = sizeof(TCMsg)){
    bwprintf(COM2,"RCV: T %d L:%d\n\r",TaskID,MsgLen);
 }*/
  AP Args;
  Args.arg0 = (void *)(TaskID);
  Args.arg1 = (void *)(Msg);
  Args.arg2 = (void *)(MsgLen);
 
  int Return = ActualReceive(&Args);
  return Return;
}

int ActualReplay(AP* Args){
	asm volatile ("swi 7\n\t");
	register int RetVal asm("r0");
	int Return = RetVal;
	return Return;
}

/* 
   The calling task and the sender return at the same logical time. If they are of the same priority the sender runs first.
 */
int Reply (int TaskID, void* Reply, int ReplyLen) {
  //bwprintf(COM1,"Reply: ReplyMessage %d\n\r",Reply);
  AP Args;
	Args.arg0 = (void *)(TaskID);
	Args.arg1 = (void *)(Reply);
	Args.arg2 = (void *)(ReplyLen);

	int Return = ActualReplay(&Args);
	return Return;
}

Buffer** GetAllSendQ(){
	asm volatile("swi 10");
	register int RetVal asm("r0");
	Buffer** Return = (Buffer **)RetVal;
	return Return;
}

int ActualAwait(AP* Args){
  asm volatile ("swi 11\n\t");
  register int RetVal asm("r0");
  int Return = RetVal;
  return Return;
}

int AwaitEvent(int EventID) {
  //bwprintf(COM2,"AwaitEvent: waiting on %d\n\r",EventID);
  AP Args;
  Args.arg0 = (void*) EventID;
  int Ret = ActualAwait(&Args);
  return Ret;
}

int ActualRecordShared(AP* Args){
  asm volatile("swi 14\n\t");
  register int RetVal asm("r0");
  int Return = RetVal;
  return Return;
}

int RecordShared(void * Shared){
  AP Args;
  Args.arg1 = Shared;
  int Ret = ActualRecordShared(&Args);
  return Ret;
}

int* GetShared(){
  asm volatile("swi 15\n\t");
  register int RetVal asm("r0");
  int* Ret = RetVal;
  return Ret;
}

void QuitProgram(){
  asm volatile("swi 16\n\t");
}

void SelfDestroy(){
  asm volatile("swi 17\n\t");
}
