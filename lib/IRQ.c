#include <IRQ.h>
#include <bwio.h>
void ClearAllIRQ(int IRQ1, int IRQ2){
  asm volatile(".VIC1ENCLR:\n\t"
	       ".word 0x800b0014\n\t"
	       ".VIC2ENCLR:\n\t"
	       ".word 0x800c0014\n\t"
	       "ldr r2,.VIC1ENCLR\n\t"
	       "ldr r3,.VIC2ENCLR\n\t"
	       "str r0, [r2]\n\t"
	       "str r1, [r3]\n\t");
}

void ClearIRQ(int Source, int IRQ){
  asm volatile("add r1, r1, #0x14\n\t"
	       "str r0, [r1]\n\t");
}

void DisableIRQ(int Source, int IRQ){
  asm volatile("add r1, r1, #0x10\n\t"
	       "ldr r2, [r1]\n\t"
	       "bic r2, r2, r0\n\t"
	       "str r2, [r1]\n\t");
  //bwprintf(COM2,"DisableIRQ: %d",IRQ);
}

void EnableIRQ(int Source,int IRQ){
  asm volatile("add r1, r1, #0x10\n\t"
	       "str r0, [r1]\n\t");
  
  //bwprintf(COM2,"EnableIRQ: %d",IRQ);
}
