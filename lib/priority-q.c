#include <bwio.h>
#include <priority-q.h>

int ASCII2NUM(char ch){
  if(ch >= '0' && ch<= '9') return ch-'0';
  if(ch >= 'a' && ch<= 'f') return ch-'a'+10;
  if(ch>= 'A' && ch<= 'F') return ch-'A'+10;
  return -1;
}

int TDPQReady(TDPQ* PQ)
{
  int ready =((PQ!=0))? 1:0;
  return ready;
}

int PushToTDPQ(TDPQ *PQ, TD* Task)
{
  //bwprintf(COM1,"PushToTDPQ: PQ:%d,task:%d\n\r",PQ,Task);
  if(TDPQReady(PQ)){
    if(PQ->Head == NULL){
      PQ->Head = Task;
      PQ->Tail = Task;
    }else{
      PQ->Tail->NextInPQ = Task;
      PQ->Tail = Task;
    }
    PQ->Length = PQ->Length + 1;
    //bwprintf(COM2,"PQ->Len %d, PQ->head %d\n\r",PQ->Length, PQ->Head);
    return 0;
  }else return -1;
}

TD* TDPQGetStart(TDPQ* PQ)
{
  if(TDPQReady(PQ) || PQ->Length > 0){
    return PQ->Head;
  }else return NULL;
}

int TDPQPopStart(TDPQ* PQ)
{
  TD* temp = PQ->Head;
  if(TDPQReady(PQ)||PQ->Length == 0){
    PQ->Head = PQ->Head->NextInPQ;
    temp->NextInPQ = NULL;
    PQ->Length = PQ->Length - 1;
  }else return -1;
}
