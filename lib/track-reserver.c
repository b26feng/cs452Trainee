#include <types.h>
#include <interrupt-io.h>
#include <nameserver.h>
#include <track-reserver.h>
#include <syscall.h>
#include <path-finding.h>

int ReserveOrFreeTrack(TrackNode* Src, int Distance, int TrainID, char Reserve) {
	if (NumSegments < 0) return FAILURE;

	int i = 0, bFailed = 0;
	TrackNode* Temp;
	Temp = Src;

	if (Reserve == 'Y') {
		while (Distance > 0) {
			if (Temp->TrainOwn == -1) {
				Temp->TrainOwn = TrainID;
				if (Temp->type == NODE_BRANCH) {
					Distance -= Temp->edge[Temp->State].dist;
					Temp = Temp->edge[Temp->State].dest;
				}
				else if (Temp->type == NODE_EXIT) {
          bFailed = 1;
					break;
				} else {
					Distance -= Temp->edge[DIR_AHEAD].dist;
					Temp = Temp->edge[DIR_AHEAD].dest;
				}
				i++;
			} else {
        bFailed = 1;
        break;
      }
		}
	}
	if (Reserve == 'N' || bFailed) {
		Temp = Src;
		while (i > 0) {
			Temp->TrainOwn = -1;
			if (Temp->type == NODE_BRANCH) Temp = Temp->edge[Temp->State].dest;
			else Temp = Temp->edge[DIR_AHEAD].dest;
			i--;
		}
		if (bFailed) return FAILURE;
	}
	return 0; 
}


void TrackRsvr(){
	int Result;
	char ServerName[] = "TrackRsrvr";
	Result = RegisterAs(ServerName);

	const int *Universal = (int *)(GetShared());

	int MyID = MyTid();
	int MyIndex = GetMemoryIndexINT(MyID);
	Buffer** AllSendQ = GetAllSendQ();

	Buffer SendQ;
	int QStorage[MAX_NUM_TD];
	InitBuffer(&SendQ,(void*)QStorage,MAX_NUM_TD);
	AllSendQ[MyIndex] = &SendQ;


	int TaskToReceive,i;
	TrkRsrvrMsg ReceiveSlot,ReplySlot;
	Message ReceiveMsg;
	ReceiveMsg.Addr = (void *)&ReceiveSlot;

  TrackNode Track[TRACK_MAX];

	char bReceived = 0;

	FOREVER{
		if(!bReceived){	
			Result = BufferFIFO(&SendQ,(void *)(&TaskToReceive));
			BufferPopHead(&SendQ);
		}
		if(Result == 0){
			if(bReceived == 0){
				Result = Receive(TaskToReceive,(void *)&ReceiveMsg,sizeof(TrkRsrvrMsg));
				TaskToReceive = ReceiveMsg.SenderID;
			}
			bReceived = 0;

			switch(ReceiveSlot.Type){
				case RESERVE_TRACK:
					Result = ReserveOrFreeTrack(&Track[ReceiveSlot.Src], ReceiveSlot.Distance, ReceiveSlot.TrainID, 'Y');
					ReplySlot.Type = (Result == FAILURE) ? FAIL2RESERVE : YES;
					Reply(TaskToReceive, (void*)&ReplySlot, sizeof(TrkRsrvrMsg));
					break;

				case FREE_TRACK:
					ReserveOrFreeTrack (&Track[ReceiveSlot.Src], ReceiveSlot.Distance, ReceiveSlot.TrainID, 'N');
					ReplySlot.Type = YES;
					Reply(TaskToReceive, (void*)&ReplySlot, sizeof(TrkRsrvrMsg));
					break;

				case IS_RESERVED:
					ReplySlot.Type = (Track[ReceiveSlot.Src].TrainOwn != -1) ? YES : NOT_RESERVED;
					Reply(TaskToReceive, (void*)&ReplySlot, sizeof(TrkRsrvrMsg));
					break;
			}

		}else{
			bReceived = 1;
			Receive(-1, (void*)&ReceiveMsg,sizeof(TrkRsrvrMsg));
			TaskToReceive = ReceiveMsg.SenderID;
			Result = 0;
		}
	}
}
