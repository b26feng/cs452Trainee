#include <bwio.h>
#include <canvas.h>
#include <interrupt-io.h>
#include <tools.h>
#include <types.h>
// Reference: http://clc-wiki.net/wiki/C_standard_library:string.h:strlen
int stringLen(const char* Str) {    int i;
    for (i = 0; Str[i] != '\0'; i++);
    return i;
} 

// Reference: http://clc-wiki.net/wiki/C_standard_library:string.h:strncpy
int stringCopy(char* Dest, const char* Src, int Length) {
  int i = 0;
  for(;i<Length;i++){
    Dest[i] = Src[i];
    if(Src[i] == 0) break;
  }
  return 0;
}


// Reference: http://clc-wiki.net/wiki/C_standard_library:string.h:strcmp
int stringCompare(const char* Str1, const char* Str2) {
  //bwprintf(COM2,"StringComp: str1:%d,str2:%d\n\r",Str1,Str2);
  while (*Str1 && (*Str1==*Str2)){
    //bwprintf(COM2,"%c %c ",*Str1,*Str2);
    Str1++,Str2++;
  }
  //bwprintf(COM2,"%c %c ",*Str1, *Str2);
  return *(const unsigned char*)Str1-*(const unsigned char*)Str2;
}

/*
 * Reference: https://www.geeksforgeeks.org/write-memcpy/
 */
int byteCopy(void* Dest, void* Src, int DestLen, int SrcLen) {
    if (SrcLen > DestLen) return FAILURE;
    int i;
    char* cDest = (char*)Dest;
    char* cSrc = (char*)Src;
    for (i = 0; i < SrcLen; i++) cDest[i] = cSrc[i];
    return 0;
}


/*
 * Reference https://www.geeksforgeeks.org/write-memcpy/
 */
void memcpy(void* Dest, void* Src, int Size){
  char* cDest = (char *)Dest;
  char* cSrc = (char *)Src;
  int i = 0;
  for(;i<Size;i++){
    cDest[i] = cSrc[i];
  }
}

void setDJerk(Train* A){
  // v^2/vt = a
  float V14 = (785.0/A->Tick14);
  float V10 = (785.0/A->Tick10);
  float D14 =  (3*V14*V14) / (2*A->Dist14);
  float D10 =  (3*V10*V10) / (2*A->Dist10);
  A->DJerk = (D14-D10)/4;
}

void setDTDiff(Train* A){
  // 3vt / 3v = t
  
  float V14 = (785.0/A->Tick14);
  float V10 = (785.0/A->Tick10);
  float T14 = ((A->Dist14)*2) / (3*V14);
  float T10 = ((A->Dist10)*2) / (3*V10);
  A->DTDiff = (T14-T10)/4;
}

void setStopTick(Train* A){
  //setDTDiff(A);
  float T10 = (A->Dist10 *2) / (3*(785.0/A->Tick10));
  A->StopTick = (A->Speed - 10)*(A->DTDiff) + T10;
}

void setVel(Train* A){
  setStopTick(A);
  // v10+(s-10)*DJerk
  float V10 = (785.0/A->Tick10);
  float D10 = (3*V10*V10)/(2*A->Dist10);
  float Deacc = D10 + (A->Speed - 10)*(A->DJerk);
  A->Vel = Deacc * A->StopTick;
}

void setStopDist(Train* A){
  if(A->Speed == 0){
    A->StopDist = 0;
    A->Vel = 0;
  }else{
    setVel(A);
    A->StopDist = 3*((A->Vel * A->StopTick)/2);
  }
}

int SDGetDistance(Train* A,int Tick){
  int logresult = 0;
  int frac = Tick/(A->SDMinTick);
  int i = 1<<31;
  int counter = 31;
  for(;i|0;i>>=1){
    if(i&frac) break;
    counter --;
  }
  //Printf(COM2,"frac:%d Tick:%d Log: %d\n\r",frac,Tick,counter);
  int Seven = 1;
  for(i = counter; i>0; i--){
    Seven *=7;
  }
  return 10*Seven;
  
}

int ABS(int Num){
  if(Num<0) return -Num;
  return Num;
}

int SDGetTicks(Train* A,int Distance){
  int Upper = A->SDMaxTick;
  int Lower = A->SDMinTick;
  int last_dist = SDGetDistance(A,Lower);
  int error = ABS(last_dist - Distance);
  //int last_error = error;
  //int last_tick = Lower;
  int ticks = Lower;
  while(1){
    Printf(COM2,"%d %d %d %d %d\n\r",Upper,Lower,last_dist,error,ticks);
    if(Lower>=Upper || Upper-Lower==1) return ticks;
    if(last_dist < Distance){
      Lower = ticks;
      ticks = (ticks+Upper)/2;
    }else if(last_dist > Distance){
      Upper = ticks;
      ticks = (Lower+ticks) /2;
    }else if(last_dist == Distance) return ticks;
    last_dist = SDGetDistance(A,ticks);
    error = ABS(last_dist - Distance);
    //last_tick = ticks;
    //last_error = error;
  }
}

TrackNode* GetNextSensor(TrackNode *Src,char Normal){
  TrackNode* Temp;
  if(Normal){
    Temp = (Src->type == NODE_BRANCH)? (Src->edge)[Src->State].dest:(Src->edge)[DIR_AHEAD].dest;
  }else{
    Temp = (Src->type == NODE_BRANCH)? (Src->edge)[(Src->State+1)%2].dest:(Src->edge)[DIR_AHEAD].dest;
  }
  while(Temp->type != NODE_SENSOR){
    switch(Temp->type){
    case NODE_BRANCH:
      Temp = (Temp->edge)[Temp->State].dest;
      break;
    case NODE_MERGE:
    case NODE_ENTER:
      Temp = (Temp->edge)[DIR_AHEAD].dest;
      break;
    case NODE_EXIT:
      return NULL;
      break;
    }
  }
  return Temp;
}

int GetNextSensorD(TrackNode *Src){
  int D = (Src->type == NODE_BRANCH)? (Src->edge)[Src->State].dist:(Src->edge)[DIR_AHEAD].dist;
  //int adder;
  //int line = 38;
  //TrackNode * New;
  
  TrackNode* Temp = (Src->type == NODE_BRANCH)? (Src->edge)[Src->State].dest:(Src->edge)[DIR_AHEAD].dest;
  while(Temp->type != NODE_SENSOR){
    switch(Temp->type){
    case NODE_BRANCH:
      D += (Temp->edge)[Temp->State].dist;
      Temp = (Temp->edge)[Temp->State].dest;
      break;
    case NODE_MERGE:
    case NODE_ENTER:
      D += (Temp->edge)[DIR_AHEAD].dist;
      Temp = (Temp->edge)[DIR_AHEAD].dest;
      break;
    case NODE_EXIT:
      return D;
      break;
    default:
      break;
    }
    //Printf(COM2,"%s%c[%d;%dHTemp:%s, Next:%s,adder:%d, D:%d%s",SAVE_CURSOR,ESC,line++,80,Temp->name,New->name,adder,D,RESTORE_CURSOR);
    //D += adder;
    //Temp = New;
  }
  return D;
}


TrackNode* SetNextSensorD(TrackNode *Src,Train* T){
  int D = (Src->type == NODE_BRANCH)? (Src->edge)[Src->State].dist:(Src->edge)[DIR_AHEAD].dist;
  //int adder;
  //int line = 38;
  //TrackNode * New;
  
  TrackNode* Temp = (Src->type == NODE_BRANCH)? (Src->edge)[Src->State].dest:(Src->edge)[DIR_AHEAD].dest;
  while(Temp->type != NODE_SENSOR){
    switch(Temp->type){
    case NODE_BRANCH:
      D += (Temp->edge)[Temp->State].dist;
      Temp = (Temp->edge)[Temp->State].dest;
      break;
    case NODE_MERGE:
    case NODE_ENTER:
      D += (Temp->edge)[DIR_AHEAD].dist;
      Temp = (Temp->edge)[DIR_AHEAD].dest;
      break;
    case NODE_EXIT:
      T->Dist = D;
      return NULL;
      break;
    default:
      break;
    }
    //Printf(COM2,"%s%c[%d;%dHTemp:%s, Next:%s,adder:%d, D:%d%s",SAVE_CURSOR,ESC,line++,80,Temp->name,New->name,adder,D,RESTORE_CURSOR);
    //D += adder;
    //Temp = New;
  }
  T->Dist = D;
  T->bChanged = 1;
  return Temp;
}


TrackNode* GetLastSensor(TrackNode *Src){
  TrackNode* Reverse = Src->reverse;
  //int i = 0;
  //Printf(COM2,"%s%c[%d;%dHGLS:%s%s",SAVE_CURSOR,ESC,40+i,80,Reverse->name,RESTORE_CURSOR);
  //i++;
  switch(Reverse->type){
  case NODE_BRANCH:
    Reverse = (Reverse->edge)[Reverse->State].dest;
    break;
  case NODE_EXIT:
    return NULL;
    break;
  default:
    Reverse = (Reverse->edge)[DIR_AHEAD].dest;
    break;
  }
  //Printf(COM2,"%s%c[%d;%dHGLS:%s%s",SAVE_CURSOR,ESC,40+i,80,Reverse->name,RESTORE_CURSOR);
  //i++;
  while(Reverse->type != NODE_SENSOR){
    switch(Reverse->type){
    case NODE_EXIT:
      return NULL;
      break;
    case NODE_BRANCH:
      Reverse = (Reverse->edge)[Reverse->State].dest;
      break;
    default:
      Reverse = (Reverse->edge)[DIR_AHEAD].dest;
      break;
    }
    //Printf(COM2,"%s%c[%d;%dHGLS:%s%d%s",SAVE_CURSOR,ESC,40+i,80,Reverse->name,RESTORE_CURSOR);
    //i++;
  }
  return Reverse->reverse;
}

int GetDist(TrackNode* Src,TrackNode* Dest){
  int D = (Src->type == NODE_BRANCH)? (Src->edge)[Src->State].dist:(Src->edge)[DIR_AHEAD].dist;
  //int adder;
  //int line = 38;
  //TrackNode * New;
  
  TrackNode* Temp = (Src->type == NODE_BRANCH)? (Src->edge)[Src->State].dest:(Src->edge)[DIR_AHEAD].dest;
  while(Temp->num != Dest->num){
    switch(Temp->type){
    case NODE_BRANCH:
      D += (Temp->edge)[Temp->State].dist;
      Temp = (Temp->edge)[Temp->State].dest;
      break;
    case NODE_MERGE:
    case NODE_ENTER:
    case NODE_SENSOR:
      D += (Temp->edge)[DIR_AHEAD].dist;
      Temp = (Temp->edge)[DIR_AHEAD].dest;
      break;
    case NODE_EXIT:
      return -1;
      break;
    default:
      break;
    }
    //Printf(COM2,"%s%c[%d;%dHTemp:%s, Next:%s,adder:%d, D:%d%s",SAVE_CURSOR,ESC,line++,80,Temp->name,New->name,adder,D,RESTORE_CURSOR);
    //D += adder;
    //Temp = New;
  }
  return D;
}

char RegTrack(Train* T){
  int SensorD = GetDist(T->LocBase,T->NNext) - T->LocOff;
  char bReg = 1;
  int TrainID = T->TrainID;
  int D = (int)(T->StopDist) + 1;
  TrackNode* Temp;
  char Dir = DIR_AHEAD;
  Printf(COM2,"%s%c[%d;%dHRegTrack:SensorD:%d D:%d%s",SAVE_CURSOR,ESC,40,80,SensorD,D,RESTORE_CURSOR);
  if(SensorD > D){
    Temp = T->LocBase;
    while(Temp->num != T->NNext->num){
      bReg = (!(Temp->TrainOwn))||(Temp->TrainOwn == TrainID);
      if(bReg){
	Temp->TrainOwn = TrainID;
	Temp->reverse->TrainOwn = TrainID;
      }else return bReg;
      if(Temp->type == NODE_EXIT) break;
      switch(Temp->type){
      case NODE_BRANCH:
	Dir = Temp->State;
	break;
      default:
	Dir = DIR_AHEAD;
	break;
      }
      Temp = (Temp->edge)[Dir].dest;
    }
  }else{
    Temp = T->LocBase;
    do{
      bReg = (Temp->TrainOwn) == 0 || (Temp->TrainOwn) == TrainID;
      if(bReg){
	Temp->TrainOwn = TrainID;
	Temp->reverse->TrainOwn = TrainID;
	Temp = (Temp->edge)[Dir].dest;
	if(Temp->type = NODE_EXIT) break;
	Dir = (Temp == NODE_BRANCH)? Temp->State:DIR_AHEAD;
      }else break;
    }while((D -= ((Temp->edge)[Dir].dist))>0);
  }
  T->bChanged = 1;
  return bReg;
}

void FreeTrack(Train* T){
  TrackNode* Temp = T->LocBase->reverse;
  char Dir =DIR_AHEAD;
  Temp = (Temp->edge)[Dir].dest;
  int TrainID = T->TrainID;
  int counter = 0;
  while(Temp->TrainOwn == TrainID){
    Printf(COM2,"%s%c[%d;%dHFreeT:%s %s",SAVE_CURSOR,ESC,48,80+(counter<<2),Temp->name,RESTORE_CURSOR);
    counter ++;
    Temp->TrainOwn = 0;
    Temp->reverse->TrainOwn = 0;
    switch(Temp->type){
    case NODE_BRANCH:
      Dir = DIR_STRAIGHT;
      if(((Temp->edge)[Dir].dest)->TrainOwn == TrainID){
	Temp = (Temp->edge)[Dir].dest;
      }else{
	Dir = DIR_CURVED;
	Temp = (Temp->edge)[Dir].dest;
      }
    case NODE_EXIT:
      return;
      break;
    default:
      Dir = DIR_AHEAD;
      Temp = (Temp->edge)[Dir].dest;
    }
  }
}

void FreeFromSwitch(TrackNode* S){
  int Owner = S->TrainOwn;
  S->TrainOwn = 0;
  S->reverse->TrainOwn = 0;
  TrackNode* Temp = (S->edge)[(S->State+1)%2].dest;
  char Dir;
  while(Temp->TrainOwn == Owner){
    Temp->TrainOwn = 0;
    Temp->reverse->TrainOwn = 0;
    Dir = (Temp->type == NODE_BRANCH)? (Temp->State):DIR_AHEAD;
    if(Temp->type != NODE_EXIT){
      Temp = (Temp->edge)[Dir].dest;
    }else{
      break;
    }
  }
}

TrackEdge* GetCurrentEdge(Train* T){
  TrackEdge* Edge = &((T->LocBase->edge)[DIR_AHEAD]);
  int Offset = T->LocOff;
  TrackNode* TempDest;
  while(Edge->dist < Offset){
    Offset -= Edge->dist;
    TempDest = Edge->dest;
    switch (TempDest->type){
    case NODE_BRANCH:
      Edge = &((Edge->dest)[Edge->dest->State]);
      break;
    case NODE_EXIT:
      return Edge;
      break;
    default:
      Edge = &((Edge->dest)[DIR_AHEAD]);
      break;
    }
    TempDest = Edge->dest;
  }
  return Edge;
}
