#include <bwio.h>
#include <canvas.h>
#include <interrupt-io.h>
#include <types.h>
void DrawLine_V(const char Elem, const int Length){
  int i = 0;
  for(;i<Length;i++){
    Putc(Elem);
    MOVE_DOWN(1);
    MOVE_LEFT(1);
  }
}
void InitLayout(){
  int i;
	CLEARALL;
	CURSOR(1,1);
	//Printf(COM2, "_________1_________2_________3_________4_________5_________6_________");
	//Printf(COM2, "123456789012345678901234567890123456789012345678901234567890123456789");
	Printf(COM2, "+-------------------------------------------------------------------+\n\r"); // 1
	Printf(COM2,"|");
	CURSOR(2,14);
	Printf(COM2,"CS 452 S18 - Real-time Programming CHOO CHOO");
	CURSOR(2,69);
	Printf(COM2,"|\n\r");
	//Printf(COM2, "|            CS 452 S18 - Real-time Programming CHOO CHOO           |\n\r"); // 2
	Printf(COM2,"|---------------------------------+---------------------------------|\n\r"); // 3
	Printf(COM2,"|Commands: tr train_num train_speed");
	CURSOR(4,69);
	Printf(COM2,"|\n\r|");
	//Printf(COM2, "|Commands: tr train_num train_speed                                 |\n\r"); // 4
	//Printf(COM2,"|");

	CURSOR(5,12);
	Printf(COM2,"tk track");
	CURSOR(5,69);
	Printf(COM2,"|\n\r|");

	CURSOR(6,12);
	Printf(COM2,"rg train_number");
	CURSOR(6,69);
	Printf(COM2,"|\n\r|");
	
	CURSOR(7,12);
	Printf(COM2,"rv train_number");
	CURSOR(7,69);
	Printf(COM2,"|\n\r|");
	//Printf(COM2, "|          rv train_number                                          |\n\r"); // 5
	CURSOR(8,12);
	Printf(COM2,"sw switch_num switch_direction");
	CURSOR(8,69);
	Printf(COM2,"|\n\r|");
	//Printf(COM2, "|          sw switch_num switch_direction                           |\n\r"); // 6
	CURSOR(9,12);
	Printf(COM2,"sa train_number sensor_number");
	CURSOR(9,69);
	Printf(COM2,"|\n\r|");
	
	CURSOR(10,12);
	Printf(COM2,"vl train_number sensor_start sensor_end");
	CURSOR(10,69);
	Printf(COM2,"|\n\r|");

	
	CURSOR(11,12);
	Printf(COM2,"dc train_number");
	CURSOR(11,69);
	Printf(COM2,"|\n\r|");
	
	CURSOR(12,12);
	Printf(COM2,"sd train_number distance");
	CURSOR(12,69);
	Printf(COM2,"|\n\r|");

	
	CURSOR(13,12);
	Printf(COM2,"sb train_number sensor distance");
	CURSOR(13,69);
	Printf(COM2,"|\n\r|");

	CURSOR(14,12);
	Printf(COM2,"q quit");
	CURSOR(14,69);
	Printf(COM2,"|\n\r");
	//Printf(COM2, "|          q                                                        |\n\r"); // 7
	Printf(COM2, "+-------------------------------------------------------------------+\n\r"); // 8
	//Printf(COM2, "_________1_________2_________3_________4_________5_________6_________")
	//Printf(COM2, "123456789012345678901234567890123456789012345678901234567890123456789")
	Printf(COM2,"|");
	CURSOR(16,9);
	Printf(COM2,"Time:");
	CURSOR(16,50);
	Printf(COM2,"Idle:");
	CURSOR(16,69);
	Printf(COM2,"|\n\r");
	//Printf(COM2, "|       Time:                                    Idle:              |\n\r"); // 9
	Printf(COM2, "+-------------------------------------------------------------------+\n\r"); // 10
	Printf(COM2,"|Switch |01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18");
	MOVE_RIGHT(6);
	Printf(COM2,"|\n\r");
	//Printf(COM2, "|Switch |01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18      |\n\r"); // 11
	Printf(COM2, "|-------|-----------------------------------------------------------|\n\r"); // 12
	//Printf(COM2, "_________1_________2_________3_________4_________5_________6_________")
	//Printf(COM2, "123456789012345678901234567890123456789012345678901234567890123456789")
	// Printf(COM2, "| State |                                                     ");
	Printf(COM2, "| State | C  C  C  C  C  C  C  C  C  C  C  C  C  C  C  C  C  C");
	MOVE_RIGHT(6);
	Printf(COM2,"|\n\r");
	//Printf(COM2, "| State | C  C  C  C  C  C  C  C  C  C  C  C  C  C  C  C  C  C      |\n\r"); // 13
	Printf(COM2, "|-------|-----------------------------------------------------------|\n\r"); // 14
	Printf(COM2, "|Switch |153  154  155  156");
	MOVE_RIGHT(41);
	Printf(COM2,"|\n\r");
	//Printf(COM2, "|Switch |153  154  155  156                                         |\n\r"); // 15
	Printf(COM2, "|-------|-----------------------------------------------------------|\n\r"); // 16
	// Printf(COM2, "_________1_________2_________3_________4_________5_________6_________")
	//Printf(COM2, "123456789012345678901234567890123456789012345678901234567890123456789")
	Printf(COM2, "| State | S    C    S    C");
	// Printf(COM2, "| State |                 ");
	MOVE_RIGHT(42);
	Printf(COM2,"|\n\r");
	//Printf(COM2, "| State | S    C    S    C                                          |\n\r"); // 17
	Printf(COM2, "|-------|-----------------------------------------------------------|\n\r"); // 18
	Printf(COM2, "|Sensors|");
	MOVE_RIGHT(59);
	Printf(COM2,"|\n\r");
	//Printf(COM2, "|Sensors|                                                           |\n\r"); // 19
	Printf(COM2, "|-------|-----------------------------------------------------------|\n\r"); // 20
	for(i = 0;i<5;i++){
	  Printf(COM2,"|    At:");
	  MOVE_RIGHT(13);
	  Printf(COM2,"Next:      S:   V:  D:");
	  MOVE_RIGHT(25);
	  Printf(COM2,"|\n\r");
	}
	//Printf(COM2, "|       Current Location:  A00            Speed: 00                 |\n\r"); // 21
	Printf(COM2, "+-------------------------------------------------------------------+\n\r"); // 22
	CURSOR(60,1);
	Printf(COM2, "> Prompt: ");//DelayNopass: ");
}

void DrawTrack(char A){
	if (A == 'b' || A == 'B') {
		Printf(COM2,"%s%c[%d;%dH",SAVE_CURSOR,ESC,1,80);
		Printf(COM2,"======5[//]=======================18[\\\\]=======3[\\\\]=============");
		CURSOR(2,85);
		Printf(COM2,"7[//]==========================6[\\\\]=======2[\\\\]============");
		CURSOR(3,86);
		Printf(COM2,"//");
		MOVE_RIGHT(32);
		Printf(COM2,"\\\\");
		MOVE_RIGHT(7);
		Printf(COM2,"1[\\\\]===========");
		CURSOR(4,83);
		Printf(COM2,"8[//]========17[\\\\]==16[//]========15[\\\\]");
		MOVE_RIGHT(7);
		Printf(COM2,"||");
		CURSOR(5,84);
		Printf(COM2,"//");
		MOVE_RIGHT(14);
		Printf(COM2,"\\\\    //");
		MOVE_RIGHT(14);
		Printf(COM2,"\\\\       ||");
		CURSOR(6,83);
		Printf(COM2,"//");
		MOVE_RIGHT(16);
		Printf(COM2,"\\\\  //");
		MOVE_RIGHT(16);
		Printf(COM2,"\\\\      ||");
		CURSOR(7,82);
		Printf(COM2,"|/");
		MOVE_RIGHT(18);
		Printf(COM2,"\\\\//");
		MOVE_RIGHT(18);
		Printf(COM2,"\\|     ||");
		CURSOR(8,82);
		Printf(COM2,"||");
		MOVE_RIGHT(13);
		Printf(COM2,"4[\\\\] || 3[//]");
		MOVE_RIGHT(13);
		Printf(COM2,"||     ||");
		CURSOR(9,82);
		Printf(COM2,"||");
		MOVE_RIGHT(19);
		Printf(COM2,"||");
		MOVE_RIGHT(19);
		Printf(COM2,"||     ||");
		CURSOR(10,82);
		Printf(COM2,"||");
		MOVE_RIGHT(13);
		Printf(COM2,"5[//] || 6[\\\\]");
		MOVE_RIGHT(13);
		Printf(COM2,"||     ||");
		CURSOR(11,82);
		Printf(COM2,"|\\");
		MOVE_RIGHT(18);
		Printf(COM2,"//\\\\");
		MOVE_RIGHT(18);
		Printf(COM2,"/|     ||");
		CURSOR(12,83);
		Printf(COM2,"\\\\");
		MOVE_RIGHT(16);
		Printf(COM2,"//  \\\\");
		MOVE_RIGHT(16);
		Printf(COM2,"//      ||");
		CURSOR(13,84);
		Printf(COM2,"\\\\");
		MOVE_RIGHT(14);
		Printf(COM2,"//    \\\\");
		MOVE_RIGHT(14);
		Printf(COM2,"//       ||");
		CURSOR(14,83);
		Printf(COM2,"9[\\\\]========10[//]==13[\\\\]========14[//]=====4[//]===========");
		CURSOR(15,86);
		Printf(COM2,"\\\\");
		MOVE_RIGHT(32);
		Printf(COM2,"//");
		MOVE_RIGHT(8);
		Printf(COM2,"//");
		CURSOR(16,87);
		Printf(COM2,"\\\\===========================11[//]====12[//]=============");
		Printf(COM2,"%s",RESTORE_CURSOR);
	} else {
		Printf(COM2,"%s%c[%d;%dH",SAVE_CURSOR,ESC,1,80);
// 	//  Printf(COM2, "_________1_________2_________3_________4_________5_________6_________")
// 	//  Printf(COM2, "123456789012345678901234567890123456789012345678901234567890123456789")
		Printf(COM2, "========12S=11S===================================\\"); //1
		CURSOR(2,80);
		Printf(COM2, "=====4S/");
		CURSOR(2,93);
		Printf(COM2,"/");
		CURSOR(2,96);
		Printf(COM2, "/========13S=====10S============\\");
		CURSOR(2,131);
		Printf(COM2, "\\");
		CURSOR(3,80);
		Printf(COM2,"======/");
		CURSOR(3,90);
		Printf(COM2,"14S==/");
		CURSOR(3,108);
		Printf(COM2, "\\");
		CURSOR(3,111);
		Printf(COM2,"|");
		CURSOR(3,114);
		Printf(COM2,"/");
		CURSOR(3,129);
		Printf(COM2,"\\");
		CURSOR(3,132);
		Printf(COM2,"\\");
		CURSOR(4,91);
		Printf(COM2,"/");
		CURSOR(4,109);
		Printf(COM2,"\\");
		CURSOR(4,111);
		Printf(COM2,"| /155S");
		CURSOR(4,130);
		Printf(COM2,"\\==9S");
		CURSOR(5,90);
		Printf(COM2,"|");
		CURSOR(5,106);
		Printf(COM2, "156S\\|/");
		CURSOR(5,134);
		Printf(COM2,"|");
		CURSOR(6,90);
		Printf(COM2,"|");
		CURSOR(6,112);
		Printf(COM2,"|");
		CURSOR(6,111);
		Printf(COM2,"|\\154S");
		CURSOR(6,134);
		Printf(COM2,"|");
		CURSOR(7,91);
		Printf(COM2,"\\");
		CURSOR(7,106);
		Printf(COM2,"153S/|");
		CURSOR(7,113);
		Printf(COM2,"\\");
		CURSOR(7,130);
		Printf(COM2,"/==8S");
		CURSOR(8,90);
		Printf(COM2,"15S==\\");
		CURSOR(8,109);
		Printf(COM2,"/");
		CURSOR(8,111);
		Printf(COM2,"|");
		CURSOR(8,114);
		Printf(COM2,"\\");
		CURSOR(8,129);
		Printf(COM2,"/");
		CURSOR(8,132);
		Printf(COM2,"/");
		CURSOR(9,80);
		Printf(COM2,"=========\\");
		CURSOR(9,93);
		Printf(COM2,"\\");
		CURSOR(9,96);
		Printf(COM2,"\\==========16S=====17S==========/");
		CURSOR(9,131);
		Printf(COM2,"/");
		CURSOR(10,80);
		Printf(COM2,"========1S\\");
		CURSOR(10,94);
		Printf(COM2,"\\========6S===========7S============/");
		CURSOR(11,80);
		Printf(COM2,"=========2S\\");
		CURSOR(11,105);
		Printf(COM2,"\\");
		CURSOR(11,116);
		Printf(COM2,"/");
		CURSOR(12,80);
		Printf(COM2,"============3S==========18S=======5S============");  
	}
}

