#include <types.h>
#include <min-heap.h>
#include <bwio.h>

int printMinHeap(MinHeap* Heap) {
	if (Heap == NULL || Heap->Num == 0) return FAILURE;
	int i;
	for (i = 0; i < Heap->Num; i++) {
		HN* Temp = (HN*)(Heap->Storage) + i;
		bwprintf(COM2,"(%d: %d)", Temp->TaskID, Temp->DelayedTick);
	}
	bwprintf(COM2, "\n\r");
	return SUCCESS;
}

// returns min Tick
int GetMinMinHeap(MinHeap* Heap, int* TaskID, int* DelayedTick) {
  if (Heap->Num <= 0) return 0;
  HN* MinNode = (HN*)Heap->Storage;
  
  *TaskID =  MinNode->TaskID;
  *DelayedTick = MinNode->DelayedTick;
  
  return 1; // don't change this to SUCCESS, need 1 for ClockServer to work
}

// One Tough Cookie: Move Last to Top then Bubble Down
void RemoveMinMinHeap(MinHeap* Heap) {
	HN* LastNode = (HN*)(Heap->Storage) + Heap->Last -1;
	HN* MinNode = (HN*)(Heap->Storage);

	MinNode->TaskID = LastNode->TaskID;
	MinNode->DelayedTick = LastNode->DelayedTick;

	LastNode->TaskID = -1;
	LastNode->DelayedTick = -1;

	Heap->Num -= 1;
	Heap->Last -= 1;

	//bwprintf(COM2,"RM: ");
	//printMinHeap(Heap);
	BubbleDownMinHeap(Heap, 0); 
}

// inserts into Min Heap
void InsertMinHeap(MinHeap* Heap, int TaskID, int DelayedTick) {
  //bwprintf(COM2,"HInsert:%d\n\r",TaskID);
	HN* newNode = (HN*)(Heap->Storage) + Heap->Last;

	newNode->TaskID = TaskID;
	newNode->DelayedTick = DelayedTick;

	Heap->Num += 1;
	BubbleUpMinHeap(Heap, Heap->Last);
	Heap->Last += 1;
}

void BubbleDownMinHeap(MinHeap* Heap, int StartIndex) {
	int LeftChildIndex, RightChildIndex, NextIndex;
	HN Temp;

	LeftChildIndex = StartIndex*2 + 1;
	RightChildIndex = StartIndex*2 + 2;

	HN* Cur = (HN *)(Heap->Storage) + StartIndex;
	HN* Left = (HN *)(Heap->Storage) + LeftChildIndex;
	HN* Right = (HN *)(Heap->Storage) + RightChildIndex;

	Temp = *Cur;
	if (Right->DelayedTick == -1 || (Left->DelayedTick < Right->DelayedTick)) { // Right Branch is NULL
		if (Cur->DelayedTick > Left->DelayedTick && Left->DelayedTick!=-1) {
			*Cur = *Left;
			*Left = Temp;
			NextIndex = LeftChildIndex;
		}
	}
	else {
		if (Cur->DelayedTick > Right->DelayedTick) {
			*Cur = *Right;
			*Right = Temp;
			NextIndex = RightChildIndex;
		}
	}
	//bwprintf(COM2,"BDW:");
	//printMinHeap(Heap);
	if ((2*NextIndex+1) < Heap->Last) BubbleDownMinHeap(Heap, NextIndex);
}

void BubbleUpMinHeap(MinHeap* Heap, int Last) {
	int ParentIndex = (Last-1)/2;
	HN Temp;

	if (ParentIndex < 0) return;
	HN* Parent = (HN*)(Heap->Storage) + ParentIndex;
	HN* LastNode = (HN*)(Heap->Storage) + Last;
	if (Parent->DelayedTick > LastNode->DelayedTick) {
		Temp = *Parent;
		*Parent = *LastNode;
		*LastNode = Temp;
	}
	//bwprintf(COM2,"BUP: ");
	//printMinHeap(Heap);
	if (ParentIndex != 0) BubbleUpMinHeap(Heap, ParentIndex);
}

