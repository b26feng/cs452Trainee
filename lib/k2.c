#include <bwio.h>
#include <hash-table.h>
#include <nameserver.h>
#include <rps.h>
#include <types.h>
#include <syscall.h>

// void someK1Fn() {
//   //	bwprintf(COM2, "someK1Fn: just in someK1Fn\n\r");
//   bwprintf(COM2, "My TaskID: <%d> and My ParentID: <%d>\n\r", MyTid(), MyParentTid());
//   //	bwprintf(COM2, "someK1Fn: before calling Pass\n\r");
//   Pass();
//   bwprintf(COM2, "My TaskID: <%d> and My ParentID: <%d>\n\r", MyTid(), MyParentTid());
//   Exit();
// }

void firstUserTaskChildren() {
  int TaskID;
  AP Args1, Args5;
  Args5.arg0 = (void *) 5;
  Args5.arg1 = (void *)(&NameServer);
  Args5.arg2 = 0;
  Args5.arg3 = 0;

  int NameServerID = Create(&Args5);
  bwprintf(COM2,"NameServer: %d\n\r",NameServerID);
  Args5.arg0 = (void *)(NameServerID);
  RecordNameServer(&Args5);
  bwprintf(COM2,"NameServer Registered\n\r");
  
  Pass();
    
  Args1.arg0 = (void *) 1;
  Args1.arg1 = (void *)(&RPSServer);
  int RPSServerID = Create(&Args1);

  Args1.arg1 = (void *)(&RPSClient1);
  int RPSClient1ID = Create(&Args1);
  Args1.arg1 = (void *)(&RPSClient2);
  int RPSClient2ID = Create(&Args1);

  //bwprintf(COM2,"RPSServer %d, C1 %d, C2 %d\n\r",RPSServerID, RPSClient1ID, RPSClient2ID);
  
  bwprintf(COM2, "FirstUserTask: exiting\n\r");
  Exit();
}
