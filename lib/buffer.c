#include <buffer.h>
#include <bwio.h>
#include <error.h>
#include <types.h>


int BufferReady(Buffer* Buf){
  int ready = ((Buf!=NULL) &&(Buf->Size >0))? 1:0;
  return ready;
}

void BufferClear(Buffer* Buf){
  if(BufferReady(Buf)){
    Buf->Head = 0;
    Buf->Tail = 0;
    Buf->Length = 0;
  }
}

int BufferSlots(Buffer* Buf){
  return Buf->Size - Buf->Length;
}

int InitBuffer(Buffer* Buf,void* Store,int size){
  if(Buf!=NULL){
    Buf->Storage = Store;
    Buf->Size = size;
    Buf->Length = 0;
    Buf->Head = 0;
    Buf->Tail = 0;
    return 0;
  }else return BUFF_NOTREADY;
}


int FeedBuffer(Buffer* Buf,void* Word)
{
  if(BufferReady(Buf)){
    if(Buf->Length == Buf->Size) return BUFF_FULL;
    int* Store = (int *)(Buf->Storage);
    Store[Buf->Tail] = (int)Word;
    Buf->Tail = (Buf->Tail + 1) % (Buf->Size);
    Buf->Length = Buf->Length + 1;
    return 0;
  }else return BUFF_NOTREADY;
}

int FeedBufferByte(Buffer* Buf, void *Byte)
{
  if(BufferReady(Buf)){
    if(Buf->Length == Buf->Size) return BUFF_FULL;
    char *Store = (char *)(Buf->Storage);
    Store[Buf->Tail] = (char)Byte;
    Buf->Tail = (Buf->Tail + 1)%(Buf->Size);
    Buf->Length = Buf->Length + 1;
    return 0;
  }else return BUFF_NOTREADY;
}

int FeedBufferStr(Buffer* Buf, void* Str, int Len)
{
  if(BufferReady(Buf)){
    if(Buf->Length == Buf->Size) return BUFF_FULL;
    if((Buf->Size - Buf->Length)<Len) return BUFF_INSUFFICIENT;
    char *Store = (char *)(Buf->Storage);
    int i = 0;
    int tail = Buf->Tail;
    int size = Buf->Size;
    char *s = (char *)Str;
    for(;i<Len;i++){
      Store[(i+tail)%size] = s[i];
    }
    Buf->Tail = (tail + Len)%size;
    Buf->Length += Len;
    return 0;
  }else return BUFF_NOTREADY;
}
      

int BufferFIFO(Buffer* Buf,void* Dest)
{
  if(BufferReady(Buf)){
    if(Buf->Length == 0) return BUFF_EMPTY;
    int* Store = (int *)(Buf->Storage);
    *(int *)Dest = Store[Buf->Head];
    // bwprintf(COM2,"BufferFIFO: at bufhead = %s", ((TrackNode*)Store[Buf->Head])->name);
    return 0;
  }else return BUFF_NOTREADY;
}


int BufferFIFOByte(Buffer* Buf,void *Dest)
{
  //bwprintf(COM1,"FIFOByte: Buf:%d \n\r",Buf);
  if(BufferReady(Buf)){
    //bwprintf(COM1,"FIFOByte:  Ready \n\r");
    if(Buf->Length == 0)return BUFF_EMPTY;
    //bwprintf(COM1,"FIFOByte:  Not Empty \n\r");
    char* Store = (char *)(Buf->Storage);
    //bwprintf(COM1,"FIFOByte:  Store %d \n\r",Store);
    *(char *)Dest = Store[Buf->Head];
    //bwprintf(COM1,"FIFOByte:  Copied %c \n\r",Store[Buf->Head]);
    return 0;
  }else return BUFF_NOTREADY;
}

int BufferFIFOStr(Buffer* Buf, void* Dest,int len){
  //bwprintf(COM2,"FIFOStr\n\r");
  if(BufferReady(Buf)){
    if(Buf->Length == 0) return BUFF_EMPTY;
    char* Store = (char *)(Buf->Storage);
    char* dest = (char *)Dest;
    int i = 0;
    int size = Buf->Size;
    int head = Buf->Head;
    //bwprintf(COM2,"FIFOStr before while\n\r");
    while(i<len){
      dest[i] = Store[(head + i)%size];
      i++;
    }
    //bwprintf(COM2,"FIFOStr af\n\r");
    Buf->Head = (head + len)%size;
    Buf->Length -= len;
    return 0;
  }else return BUFF_NOTREADY;
}

int BufferPopHead(Buffer* Buf)
{
  //bwprintf(COM2,"BufferPopHead: Buf is %d\n\r",Buf);
  if(BufferReady(Buf)){
    if(Buf->Length == 0) return BUFF_EMPTY;
    Buf->Head = (Buf->Head +1) % Buf->Size;
    Buf->Length = (Buf->Length) - 1;
    return 0;
  }else return BUFF_NOTREADY;
}

int BufferFILO(Buffer* Buf, void* Dest)
{
  // bwprintf(COM2,"BufferFILO: BufTail is %d\n\r",Buf->Tail);
  if(Buf->Size == 0) return  BUFF_NOTREADY;
  if(Buf->Length == 0) return BUFF_EMPTY;
  int* Store = (int *)(Buf->Storage);
  *(int *)Dest = Store[Buf->Tail-1];
  return 0;
}

int BufferPopLast(Buffer* Buf)
{
  if(BufferReady(Buf)){
    if(Buf->Length == 0) return BUFF_EMPTY;
    Buf->Tail = (Buf->Tail - 1 + Buf->Size) % Buf->Size;
    Buf->Length = (Buf->Length) - 1;
    return 0;
  }else return BUFF_NOTREADY;
}

int ClearBuffer(Buffer* Buf)
{
  if(BufferReady(Buf)){
    Buf->Length = 0;
    Buf->Tail = Buf->Head;
    return 0;
  }else return BUFF_NOTREADY;
}

int BufferShrinkFIFO(Buffer* Buf, int Num)
{
  if(BufferReady(Buf)){
    int ActualShrink = (Num > Buf->Length)? (Buf->Length): Num;
    Buf->Length = Buf->Length - ActualShrink;
    Buf->Head = (Buf->Head + ActualShrink) % Buf->Size;
    return 0;
  }else return BUFF_NOTREADY;
}
