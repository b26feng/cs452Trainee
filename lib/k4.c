#include <buffer.h>
#include <bwio.h>
#include <clock.h>
#include <display.h>
#include <idle-task.h>
#include <io-api.h>
#include <ioserver.h>
#include <interrupt-io.h>
#include <k4.h>
#include <kernel.h>
#include <nameserver.h>
#include <notifier.h>
#include <path-finding.h>
#include <sensors.h>
#include <syscall.h>
#include <tools.h>
#include <trackserver.h>
#include <train-controller.h>
#include <types.h>
#include <ts7200.h>
#include <train-controller.h>

void InitNameServer() {
	AP Args;
	Args.arg0 = (void *) 13;
	Args.arg1 = (void *)(&NameServer);
	Args.arg2 = 0;
	Args.arg3 = 0;

	int NameServerID = Create(&Args);
	//bwprintf(COM2,"NS created %d \n \r", NameServerID);
	bwprintf(COM2,"FUT: name server created, before pass\n\r");
	Pass();
}

void PrintTask(){
	char c = 'd';
	int clkid = WhoIs("ClockServer");
	/*
	   char str[] = "GAAABBBCCC";
	   str[0] = 96;
	   str[2] = 1;
	   str[1] = str[4] = str[7] = 33;
	   str[3] = str[6] = str[9] = 32;
	//str[2] = str[5] = str[8] = 33;
	str[5] = 2;
	str[8] = 3;
	 */
	char str[] = ">>";
	char etr[] = "\n\r";
	str[0] = 34;
	str[1] = 5;
	char Magic = 32;
	int len = stringLen(str);
	int len2 = stringLen(etr);
	int i = 0;
	char bChange = 0;
	char printtask[] = "PrintTask:\n\r";
	PutStr(COM2,printtask,stringLen(printtask));
	//bwprintf(COM2,"PrintTask:\n\r");
	/*
	   while(1){
	   PutStr(COM2,str,len);
	   c = Getc(COM2);
	   Putc(COM2,c);
	//PutStr(COM2,&c,1);
	PutStr(COM2,etr,len2);
	//PutStr(COM2,str,len);
	}
	 */
	//bwprintf(COM2,"PrintTask: before print\n\r");
	while(1){
		c = Getc(COM2);
		//Putc(COM2,c);
		//PutStr(COM2, str, stringLen(str));
		//PutStr(COM2, etr, stringLen(etr));
		if(bChange){
			bChange = 0;
			str[0] = 34;
			/*
			   str[1] = str[4] = str[7] = 33;

			   str[2] = (str[2]+3)%18;
			   str[5] = (str[5]+3)%18;
			   str[8] = (str[8]+3)%18;*/
		}else{
			bChange = 1;
			str[0] = 33;
		}
		//i++;
		//putstr(COM1,str,len);
		if(c == 'p'){
			//PutStr(COM1,str,len);
			Putc(COM1,str[0]);
			Putc(COM1,str[1]);
			Delay(clkid,20);
			Putc(COM1,Magic);
			//str[1] = str[4] = str[7] = 33;
		}else{
			Putc(COM1,c);
			Putc(COM2,c);
		}
	}
	Exit();
}

void FirstUserTask() {
	AP Args;
	int tid,clkid;
	//bwprintf(COM2, "FUT\n\r");

	InitNameServer();

	//bwprintf(COM2,"FUT: IRQ2:%d\n\r",(*(int *)(VIC2_BASE | INT_ENABLE)));
	//bwprintf(COM2,"FUT: IntIDIntClr %d\n\r",((*(int *)(UART1_BASE | 0x1c))<<28)>>28);
	int flag = ((*(int *)(UART1_BASE | 0x18)));
	//bwprintf(COM2,"FUT: Flags %d\n\r", flag & (1<<7 | 1<<6|1));
	*(int *)(UART1_BASE) = 96;
	// write to data


	// clock notifier
	Args.arg0 = (void *) 0;
	Args.arg1 = (void *)(&ClockNotifier);
	tid = Create(&Args);
	//bwprintf(COM2,"FUT: %d\n\r",tid);

	//clock server

	Args.arg0 = (void *) 1;
	Args.arg1 = (void *)(&ClockServer);
	clkid = Create(&Args);
	EnableInterrupt(51);
	// bwprintf(COM2,"FUT:%d\n\r",clkid);
	Pass();

	//bwprintf(COM2,"FUT: idletask %d\n\r",tid);




	// COM2 IOServer
	Args.arg0 = (void*) 12;
	Args.arg1 = (void*) (&IServer2);
	tid = Create(&Args);

	Args.arg0 = (void*) 12;
	Args.arg1 = (void*) (&OServer2);
	tid = Create(&Args);

	Pass();



	Args.arg0 = (void *)15;
	Args.arg1 = (void *)(&IdleTask);
	tid = Create(&Args);

	Args.arg0 = (void *) 2;
	Args.arg1 = (void *) (&TermInNotifier);
	tid = Create(&Args);


	Args.arg0 = (void *) 2;
	Args.arg1 = (void *) (&TermOutNotifier);
	tid = Create(&Args);
	// COM1 IO server
	Args.arg0 = (void*) 7;
	Args.arg1 = (void*) (&IServer1);
	tid = Create(&Args);
	//Delay(clkid,1);
	//bwprintf(COM2,"FUT: IS1 %d\n\r",tid);

	//Delay(clkid,1);
	//bwprintf(COM2,"FUT: IS2 %d\n\r",tid);

	Args.arg0 = (void*) 7;
	Args.arg1 = (void*) (&OServer1);
	tid = Create(&Args);
	//bwprintf(COM2,"FUT: OS1 %d\n\r",tid);

	//Delay(clkid,1);
	//bwprintf(COM2,"FUT: OS2 %d\n\r",tid);
	//  bwprintf(COM2,"FUT: IO server created\n\r");
	Pass();


	Args.arg0 = (void *) 1;
	Args.arg1 = (void *) (&TrainInNotifier);
	tid = Create(&Args);
	//Delay(clkid,1);
	// bwprintf(COM2,"FUT: TinNotifier %d\n\r",tid);

	Args.arg0 = (void *) 1;
	Args.arg1 = (void *) (&TrainOutNotifier);
	tid = Create(&Args);
	// bwprintf(COM2,"FUT: TOut %d\n\r",tid);

	EnableInterrupt(52);
	// bwprintf(COM2,"FUT: 52 enabled\n\r");
	//Delay(clkid,1);

	Pass();

	//EnableInterrupt(25);
	//EnableInterrupt(26);
	//Delay(clkid,1);
	//  bwprintf(COM2,"FUT: After Pass\n\r");
	//bwprintf(COM2,"FUT: TermOut %d\n\r",tid);
	// Delay(clkid,1);
	// bwprintf(COM2,"FUT: OutNotif %d\n\r",tid);
	//Pass();
	//Delay(clkid,10);
	//char str[] = "FUT:term out notif created\n\r";
	//int len = stringLen(str); 
	//PutStr(COM2,str,len);

	//notifier

	//bwprintf(COM2,"FUT: TrainOutNotifier %d\n\r",tid);
	//Pass();
	//bwprintf(COM2,"FUT: clock server created %d\n\r", ClockServerID);

	/* What's going on before this pass(ignoring idle task):
	 * all priority shown in ()
	 * NS(13) blocked, Notifier(0) ready, ClockServer(1) ready, FUT(1) active
	 * What happens after this pass
	 * FUT(14) ready, Notifier(0) active, ClockServer(1) ready, NS(13) blocked
	 * Notifier(0) registered, blocked, NS(13) Ready, 
	 * Clockserver(1) runs, registers, blocked
	 * NS(13) runs, register Notifier(0), ClockServer(1), reply to them
	 * NS(13) Active, Notifer(0) Ready, ClockServer(1) Ready, FUT(14) Ready
	 * NS(13) received from random one, blocked
	 * Notifier(0) runs, WhoIs(clockserver), blocked, NS(13) Ready
	 * ClockServer(1)runs, init clock, receiver from random one, blocked <1>
	 * NS(13) runs, reply to Notifier(0), Notifier->Ready, NS receive, NS Block
	 * Notifier(0) awaits, Notifier(0) blocked
	 * FUT(14) runs<2>
	 * everything between <1> and <2> finishes in 10ms
	 */

	
	Args.arg0 = (void *)9;
	Args.arg1 = (void *)(&TrackServer);
	tid = Create(&Args);
	Printf(COM2,"TrackServer:%d\n\r",tid);
	Delay(clkid,1);
	
	Args.arg0 = (void *)9;
	Args.arg1 = (void *)(&TCServer);
	tid = Create(&Args);
	Delay(clkid,1);

	
	Args.arg0 = (void *)5; //TODO what priority
	Args.arg1 = (void *)(&SensorServer);
	tid = Create(&Args);
	Pass();
	
	Args.arg0 = (void*)6;
	Args.arg1 = (void*)(&Display);
	tid = Create(&Args);
	Delay(clkid,1);
	//Printf(COM2,"FUT: Display %d created\n\r",tid);


	Args.arg0 = (void *)11;
	Args.arg1 = (void *)(&TimeDisplay);
	tid = Create(&Args);
	Delay(clkid,1);

	
	Args.arg0 = (void*)5;
	Args.arg1 = (void*)(&SensorPrintTask);
	tid = Create(&Args);
	Delay(clkid,1);  
	
	
	Args.arg0 = (void *)4;
	Args.arg1 = (void *)(&SensorNotifier);
	tid = Create(&Args);
	Delay(clkid,1);

	
	Args.arg0 = (void *)12;
	Args.arg1 = (void *)(&PathRouter);
	tid = Create(&Args);
	Delay(clkid,1);
	//Delay(clkid,10);
	//bwprintf(COM2,"FUT: idt1 %d \n\r",tid);

	/*
	//Pass();
	//bwprintf(COM2,"FUT ready to create print\n\r");
	Args.arg0 = (void *)14;
	Args.arg1 = (void *)(&PrintTask);
	tid = Create(&Args);
	//bwprintf(COM2,"FUT: PrintTask: %d\n\r",tid);
	Delay(clkid,1);
	 */
	//bwprintf(COM2,"FUT: user task %d\n\r",tid);
	//Delay(clkid,10);
	//bwprintf(COM2,"FUT: print task %d\n\r",tid);
	// bwprintf(COM2,"FUT: exiting()\n\r");
	Exit(); 
}
