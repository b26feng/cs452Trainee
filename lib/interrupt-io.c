/*
 * bwio.c - interrupt-driven I/O routines for diagnosis
 *
 * Specific to the TS-7200 ARM evaluation board
 *
 */
#include <ts7200.h>
#include <bwio.h>
#include <buffer.h>
#include <io-api.h>
#include <interrupt-io.h>
/*
 * The UARTs are initialized by RedBoot to the following state
 * 	115,200 bps
 * 	8 bits
 * 	no parity
 * 	fifos enabled
 */

void Putw( int channel, int n, char fc, char *bf , Buffer* Buf) {
	char ch;
	char *p = bf;

	while( *p++ && n > 0 ) n--;
	while( n-- > 0 ) FeedBufferByte(Buf, (void *)fc);;
	while( ( ch = *bf++ ) ) FeedBufferByte(Buf, (void *)ch);
}

int a2d( char ch ) {
	if( ch >= '0' && ch <= '9' ) return ch - '0';
	if( ch >= 'a' && ch <= 'f' ) return ch - 'a' + 10;
	if( ch >= 'A' && ch <= 'F' ) return ch - 'A' + 10;
	return -1;
}

char a2i( char ch, char **src, int base, int *nump ) {
	int num, digit;
	char *p;

	p = *src; num = 0;
	while( ( digit = a2d( ch ) ) >= 0 ) {
		if ( digit > base ) break;
		num = num*base + digit;
		ch = *p++;
	}
	*src = p; *nump = num;
	return ch;
}

void ui2a( unsigned int num, unsigned int base, char *bf ) {
	int n = 0;
	int dgt;
	unsigned int d = 1;

	while( (num / d) >= base ) d *= base;
	while( d != 0 ) {
		dgt = num / d;
		num %= d;
		d /= base;
		if( n || dgt > 0 || d == 0 ) {
			*bf++ = dgt + ( dgt < 10 ? '0' : 'a' - 10 );
			++n;
		}
	}
	*bf = 0;
}

void i2a( int num, char *bf ) {
	if( num < 0 ) {
		num = -num;
		*bf++ = '-';
	}
	ui2a( num, 10, bf );
}

void iFormat ( int channel, char *fmt, va_list va ) {
  char bf[12];
  char ch, lz;
  int w;
  Buffer Buf;
  char str[256];
  InitBuffer(&Buf,(void *)str,256);
  
  
  while ( ( ch = *(fmt++) ) ) {
    if ( ch != '%' )
      FeedBufferByte(&Buf, (void *)ch);
    else {
      lz = 0; w = 0;
      ch = *(fmt++);
      switch ( ch ) {
      case '0':
	lz = 1; ch = *(fmt++);
	break;
      case '1':
      case '2':
      case '3':
      case '4':
      case '5':
      case '6':
      case '7':
      case '8':
      case '9':
	ch = a2i( ch, &fmt, 10, &w );
	break;
      }
      switch( ch ) {
      case 0: return;
      case 'c':
	//Putc( channel, va_arg( va, char ) );
	FeedBufferByte(&Buf, (void *)(va_arg(va,char)));
	break;
      case 's':
	Putw( channel, w, 0, va_arg( va, char* ), &Buf );
	break;
      case 'u':
	ui2a( va_arg( va, unsigned int ), 10, bf );
	Putw( channel, w, lz, bf, &Buf );
	break;
      case 'd':
	i2a( va_arg( va, int ), bf );
	Putw( channel, w, lz, bf, &Buf );
	break;
      case 'x':
	ui2a( va_arg( va, unsigned int ), 16, bf );
	Putw( channel, w, lz, bf, &Buf );
	break;
      case '%':
	FeedBufferByte(&Buf, (void *)ch);
	break;
      }
    }
  }
  PutStr(channel,str,Buf.Length);
  
}

void Printf( int channel, char *fmt, ... ) {
	va_list va;

	va_start(va,fmt);
	iFormat( channel, fmt, va );
	va_end(va);
}
