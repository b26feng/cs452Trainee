#include <canvas.h>
#include <clockserver-api.h>
#include <interrupt-io.h>
#include <nameserver.h>
#include <syscall.h>
#include <tools.h>
#include <traintask.h>
#include <types.h>


int UpdateTrain(Train* Trains, char* Sensors,int TimeStamp,int* Diner){
  int i,dist;
  int Counter = 0;
  char Mask,NextMask;
  char Index,NextIndex;
  char Num;
  char TimeOutCond,NormalCond;
  float newvel;
  const int* Universal = GetShared();
  PRINTAT(35,80,"UpdateTrain:");
  for(i=0;i<5;i++){
    if(Trains[i].bRegistered){
      Num = (Trains[i].Next)->num;
      Index = ((Num>>3) + 1)%10;
      Mask = 128 >> (Num - ((Num>>3)<<3));
      
      NormalCond = Mask & (Sensors[Index]);
      
      //Printf(COM2,"%s%c[%d;%dH Num:%d Index:%d Mask:%d NormCond:%d%s",SAVE_CURSOR,ESC,47,80,Num,Index,Mask,NormalCond,RESTORE_CURSOR);
      Num = (Trains[i].NNext)->num;
      NextIndex = ((Num>>3)+1)%10;
      NextMask = 128 >> (Num - ((Num>>3)<<3));
      // next is not triggered but next next is triggered
      //TimeOutCond = (!(Mask&Sensors[Index])) && (NextMask & (Sensors[NextIndex]));
      TimeOutCond = (NextMask & (Sensors[NextIndex]));
      
      if(NormalCond || TimeOutCond){
	//Printf(COM2,"%s%c[%d;%dH Norm:%d TimeOut:%d %s",SAVE_CURSOR,ESC,37,80,NormalCond,TimeOutCond,RESTORE_CURSOR);
	// if next is not triggered but next next is triggered,(malfunc sensors)
	if(TimeOutCond){
	  dist = Trains[i].Dist + GetNextSensorD(Trains[i].Next);
	  Trains[i].LocBase = Trains[i].NNext;
	}else{
	  dist = Trains[i].Dist;
	  Trains[i].LocBase = Trains[i].Next;
	}
	// Set Next and Next Next
	Trains[i].Next = SetNextSensorD(Trains[i].LocBase,&Trains[i]);
	Trains[i].NNext = GetNextSensor(Trains[i].Next,1);
	// set velocity
	newvel = dist/(TimeStamp - Trains[i].TimeStamp);
	Trains[i].DymVel = (1-Trains[i].alpha)*Trains[i].DymVel + Trains[i].alpha*newvel;
	// update location offset
	Trains[i].LocOff = (int)(Trains[i].DymVel * (*Universal - TimeStamp + 10));
	Trains[i].TimeStamp = TimeStamp;
	Trains[i].bTimedOut = 0;
	Trains[i].bChanged = 1;
	// update watchdog return time
	Diner[i] = (TimeStamp + (int)(Trains[i].Dist/Trains[i].DymVel));
	// free the path behind it
	if(!RegTrack(&Trains[i])){
	  Printf(COM2,"%s%c[%d;%dHFailed!%s",SAVE_CURSOR,ESC,28+i,58,RESTORE_CURSOR);
	  Trains[i].Speed = 0;
	  Trains[i].bChanged = 1;
	  Printf(COM1,"%c%c",0,Trains[i].TrainID);
	  UpdateTrainSpeed(&(Trains[i]),*Universal,&(Diner[i]));
	}
	FreeTrack(&Trains[i]);
	Counter ++;
      }
    }
  }
  return Counter;
}

// check train's current location
// if it passed the switch then don't update
// otherwise update next sensor estimation based on Next
void UpdateNextOrNot(Train* TList,TrackNode* Switch,int *Dinner,const int* Universal){
  TrackNode* CurrentNext,*CurrentNNext,*SNextCur,*SNextLast;
  TrackNode* LastSensor = GetLastSensor(Switch);
  int Dist,i,DogID,res;
  for(i=0;i<5;i++){
    CurrentNext = TList[i].Next;
    CurrentNNext = TList[i].NNext;
    if(TList[i].bRegistered && ((TList[i].LocBase)->num == LastSensor->num || CurrentNext->num == LastSensor->num)){
      //Printf(COM2,"%s%c[%d;%dHUNON:%s%s",SAVE_CURSOR,ESC,30,80,LastSensor->name,RESTORE_CURSOR);
      // check offset
      Dist = GetDist(TList[i].LocBase,Switch);
      //Printf(COM2,"%s%c[%d;%dH DistToSw:%d %s",SAVE_CURSOR,ESC,31,80,Dist,RESTORE_CURSOR);
      if(Dist < TList[i].LocOff && Dist>=0) continue;
      SNextCur = GetNextSensor(Switch,1);
      SNextLast = GetNextSensor(Switch,0);
      //Printf(COM2,"%s%c[%d;%dHCur:%s,Last:%s%s",SAVE_CURSOR,ESC,32,80,SNextCur->name,SNextLast->name,RESTORE_CURSOR);
      // if switch is further check if switch's next overlaps with train's
      if(CurrentNext == SNextCur){
	if(CurrentNNext == SNextLast){
	  TList[i].NNext = SNextCur;
	  if(!RegTrack(&TList[i])){
	    Printf(COM2,"%s%c[%d;%dHFailed!%s",SAVE_CURSOR,ESC,28+i,58,RESTORE_CURSOR);
	  }
	  TList[i].bChanged = 1;
	}else continue;
	
      }else if(CurrentNext == SNextLast){
	TList[i].Next = SetNextSensorD(TList[i].LocBase, &TList[i]);
	//update dog dinner time
	Dist = GetDist(TList[i].LocBase,TList[i].Next);
	Dinner[i] = (*Universal + ((Dist - TList[i].LocOff)/TList[i].DymVel) + 10);
	TList[i].NNext = GetNextSensor(TList[i].Next,1);
	TList[i].Dist = Dist;
	if(!RegTrack(&TList[i])){
	  Printf(COM2,"%s%c[%d;%dHFailed!%s",SAVE_CURSOR,ESC,28+i,58,RESTORE_CURSOR);
	}
	TList[i].bChanged = 1;
      }else continue;
    }
  }
  return 0;
}

void ReverseUpdate(Train* T,int TimeStamp,int *Dinner){
  TrackNode* Temp = T->LocBase->reverse;
  T->LocBase = T->Next->reverse;
  T->Next = Temp;
  T->LocOff = T->Dist - T->LocOff;
  T->NNext = GetNextSensor(T->Next,1);
  
  float StpD = T->StopDist;
  float BackUpV = T->DymVel;
  setStopDist(T);
  T->DymVel = T->Vel;
  T->bChanged = 1;
  int Dist = T->Dist;
  Dist = Dist - T->LocOff;
  if(RegTrack(T)){
    Printf(COM2,"%s%c[%d;%dHFailed!%s",SAVE_CURSOR,ESC,28+T->Index,58,RESTORE_CURSOR);
    FreeTrack(T);
    //}else{
  }
  if(T->Speed){
    *Dinner = (TimeStamp + (int)(Dist/T->DymVel));
  }else{
    if((T->Dist - T->LocOff)<(int)StpD){
      *Dinner = (TimeStamp + (int)(Dist/BackUpV));
    }else{
      *Dinner = TimeStamp;
      T->LocOff += (int) StpD;
    }
  }
  //Printf(COM2,"%s%c[%d;%dH D:%d T:%d V:%d dV:%d Dist:%d  interval:%d %s",SAVE_CURSOR,ESC,45,80,*Dinner,TimeStamp,(int)T->Vel,(int)T->DymVel,Dist,(int)(Dist/T->DymVel),RESTORE_CURSOR);

}

void UpdateTrainSpeed(Train* T,int TimeStamp,int *Dinner){
  if(T->Speed == 15){
    T->Speed = 0;
    ReverseUpdate(T,TimeStamp,*Dinner);
  }else{
    float StpD = T->StopDist;
    float BackUpV = T->DymVel;
    setStopDist(T);
    T->DymVel = T->Vel;
    T->bChanged = 1;
    int Dist = GetNextSensorD(T->LocBase);
    T->Dist = Dist;
    Dist = Dist - T->LocOff;
    if(T->Speed){
      *Dinner = (TimeStamp + (int)(Dist/T->DymVel));
      if(!RegTrack(T)){
	Printf(COM2,"%s%c[%d;%dHFailed!%s",SAVE_CURSOR,ESC,28+T->Index,58,RESTORE_CURSOR);
      }
    }else{
      if((T->Dist - T->LocOff)<(int)StpD){
	*Dinner = (TimeStamp + (int)(Dist/BackUpV));
      }else{
	*Dinner = TimeStamp;
	T->LocOff += (int) StpD;
      }
    }
    //Printf(COM2,"%s%c[%d;%dH D:%d T:%d V:%d dV:%d Dist:%d  interval:%d %s",SAVE_CURSOR,ESC,45,80,*Dinner,TimeStamp,(int)T->Vel,(int)T->DymVel,Dist,(int)(Dist/T->DymVel),RESTORE_CURSOR);
  }
}
/*
void TrainTask(){
  int ClkServer = WhoIs("ClockServer");
  int TSID = WhoIs("TrackServer");
  int TCID = WhoIs("TCServer");
  const int* Universal = GetShared();
  TCMsg TCSMsg,TCRMsg;
  //TSMsg TSSMSg,TSRMsg;

  int Distance,Last;
  
  TCSMsg.Type = TT_GETTRAIN;
  Send(TCID,(void *)&TCSMsg,sizeof(TCMsg),(void *)&TCRMsg,sizeof(TCMsg));
  
  Train* MyTrain = (Train*) TCRMsg.Addr;
  
  MyTrain->bRegistered = 1;
  setDJerk(MyTrain);
  setDTDiff(MyTrain);
  setStopDist(MyTrain);
  Printf(COM2,"%s%c[%d;%dH%d%s",SAVE_CURSOR,ESC,28+MyTrain->Index,3,MyTrain->TrainID,RESTORE_CURSOR);
  FOREVER{
    // location
    Printf(COM2,"%s%c[%d;%dH%s + %d    %s",SAVE_CURSOR,ESC,28+MyTrain->Index,12,MyTrain->LocBase->name,MyTrain->LocOff,RESTORE_CURSOR);
    // next sensor
    if(MyTrain->Next == NULL){
      Printf(COM2,"%s%c[%d;%dHNULL%s",SAVE_CURSOR,ESC,28+MyTrain->Index,30,MyTrain->Next->name,RESTORE_CURSOR);
    }else{
      Printf(COM2,"%s%c[%d;%dH%s  %s",SAVE_CURSOR,ESC,28+MyTrain->Index,30,MyTrain->Next->name,RESTORE_CURSOR);
    }
    // Speed
    Printf(COM2,"%s%c[%d;%dH%d %s",SAVE_CURSOR,ESC,28+MyTrain->Index,39,MyTrain->Speed,RESTORE_CURSOR);
    // other info
    //Printf(COM2,"%s%c[%d;%dHNodeStatus %d%s",SAVE_CURSOR,ESC,36,80,MyTrain->LocBase->State,RESTORE_CURSOR);
    Printf(COM2,"%s%c[%d;%dHLast:%s, Dist to next:%d   ,StopDist:%d    %s",SAVE_CURSOR,ESC,28+MyTrain->Index,80,GetLastSensor(MyTrain->LocBase)->name,GetNextSensorD(MyTrain->LocBase)-(MyTrain->LocOff),(int)MyTrain->Vel,RESTORE_CURSOR);
    //Printf(COM2,"%s%c[%d;%dHDist to next %d%s",SAVE_CURSOR,ESC,38,80,,RESTORE_CURSOR);
    Delay(ClkServer,40);
  }
  /*
  FOREVER{
    Delay(10);
    }
  Exit();
}
*/

