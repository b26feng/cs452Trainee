#include <buffer.h>
#include <bwio.h>
#include <canvas.h>
#include <IRQ.h>
#include <nameserver.h>
#include <notifier.h>
#include <syscall.h>
#include <types.h>
#include <ts7200.h>

void ClockNotifier() {
  int Res, ClockServerID, VolatileData;
  ClkMsg SendMsg, ReplyMsg;
  int IRQ = (int )(VIC2_BASE);
  Res = RegisterAs("ClockNotifier");
  ClockServerID = WhoIs("ClockServer");
  //bwprintf(COM2,"Notifier: ClockServer is %d\n\r",ClockServerID);
  FOREVER {
    //EnableIRQ(1<<19,IRQ);
    Res = AwaitEvent(TIMER3_INTERRUPT);
    if (Res > -1) {
      //bwprintf(COM2,"Notifier: Result %d\n\r",Res);
      SendMsg.Type = NOTIFIER;
      Send(ClockServerID, (void*)&SendMsg, sizeof(SendMsg), (void*)&ReplyMsg, sizeof(ReplyMsg));
    }
    else break;
  }
  if (Res == INVALID_EVENT) bwprintf(COM2, "Notifier: Invalid Event\n\r");
  else if (Res == CORRUPTED_VOLATILE_DATA) bwprintf(COM2, "Notifier: Corrupte Volatile Data\n\r");
} 

void TermInNotifier(){
  int Res,IServerID, ReplyMsg,IRQSource,IRQ;
  IOReq SendReq;
  Res = RegisterAs("TermInNotifier");
  IServerID = WhoIs("IServer");

  IRQSource = (int)(UART2IN_INT);
  IRQ = (int ) VIC1_BASE;
  EnableInterrupt((int)(UART2IN_INT));
  FOREVER{
    //bwprintf(COM2,"TIN: BA\n\r");
    Res = AwaitEvent(UART2IN_INT);
    //bwprintf(COM1,"InNot: AwaitResult %d\n\r",Res);
    if (Res > -1){
      SendReq.Type = NIN;
      SendReq.Len = 0;
      SendReq.Byte = *(char *)(UART2_BASE);
      SendReq.Chan = COM2;
      //bwprintf(COM2,"TIN: type %d\n\r",SendReq.Type);
      //bwprintf(COM1,"InNot: Send %c to IServer %d\n\r",SendReq.Byte,IServerID);
      Send(IServerID,(void*)&SendReq, sizeof(IOReq), (void *)&ReplyMsg, sizeof(int));
      //bwprintf(COM2,"TIN: AF \n\r");
      // enable interrupt
      EnableIRQ(1<<IRQSource,IRQ);
      //bwprintf(COM1,"InNot: Enabled IRQ %d",*(int *)(VIC1_BASE | 0x10));
    }
    else break;
  }
  /*
  if(Res == INVALID_EVENT) bwprintf(COM1,"InNotifier: Invalid Event\n\r");
  if(Res == CORRUPTED_VOLATILE_DATA) bwprintf(COM1,"InNotifier: Corrupte Volatile Data\n\r");
  */
}


void TermOutNotifier(){
  int Res,OServerID,IRQSource,IRQ;
  char ReplyByte;
  int TxLen = 16<<2;
  IOReq SendReq;
  Res = RegisterAs("TermOutNotifer");
  OServerID = WhoIs("OServer");
  IRQSource = (int)(UART2OUT_INT);
  IRQ = (int)VIC1_BASE;

  Buffer FIFO;
  int BufStore[32<<1];
  Res = InitBuffer(&FIFO,(void *)BufStore,16<<4);
  //bwprintf(COM1,"OutNot: before enable uart2\n\r");
  //EnableInterrupt((int)(UART2OUT_INT));
  //bwprintf(COM1,"OutNot: after enable uart2\n\r");
  FOREVER{
    //bwprintf(COM1,"OutNot: before await\n\r");
    //bwprintf(COM2,"%c[%d;%dHTON: BA\n\r",ESC,1,120);
    Res = AwaitEvent(UART2OUT_INT);
    // uart status must be checked before waking up
    if(Res > -1){
      if(FIFO.Length == 0){
	BufferClear(&FIFO);
	SendReq.Type = NOUT;
	SendReq.Chan = COM2;
	SendReq.Addr = (void *)BufStore;
	SendReq.Len = 16<<2;
	//bwprintf(COM2,"%c[%d;%dHTON: tx %d\n\r",ESC,1,120, TxLen);
	Send(OServerID,(void *)&SendReq,sizeof(IOReq),(void *)&TxLen, sizeof(int));
	//bwprintf(COM2,"%c[%d;%dHTON: AF0 %d  \n\r",ESC,1,120,TxLen);
	FIFO.Length = FIFO.Tail = TxLen;
      }
      //bwprintf(COM2,"%c[%d;%dHTON: AF1 %d %d %d \n\r",ESC,1,120,ReplyByte,FIFO.Length,FIFO.Head);
      BufferFIFOByte(&FIFO,(void *)&ReplyByte);
      BufferPopHead(&FIFO);
      //bwprintf(COM2,"%c[%d;%dHTON: AF2 %d %d %d \n\r",ESC,1,120,ReplyByte,FIFO.Length,FIFO.Head);
      *(int *)(UART2_BASE) = ReplyByte;
    }
    else break;
  }
}

void TrainInNotifier(){
  int Res,IServerID, ReplyMsg,IRQSource,IRQ;
  IOReq SendReq;
  Res = RegisterAs("TrainInNotifier");
  IServerID = WhoIs("iServer");
  char Byte;
  
  char storage[11];
  Buffer FIFO;
  InitBuffer(&FIFO,(void*)storage,10);
  
  
  FOREVER{
    //bwprintf(COM2,"TrainIn: before await %d\n\r",(-(int)(UART1_INT)));
    Res = AwaitEvent(-(int )(UART1_INT));
    //bwprintf(COM2,"TrainIn: after await %d\n\r",Res);
    if (Res > -1){
      Byte = *(char *)(UART1_BASE);
      //Printf(COM2,"%s%c[%d;%dH%d%s",SAVE_CURSOR,ESC,25,80+(FIFO.Length<<1),Byte,RESTORE_CURSOR);
      FeedBufferByte(&FIFO,(void *)Byte);
      if(FIFO.Length == FIFO.Size){
	SendReq.Type = NIN;
	SendReq.Len = 10;
	SendReq.Chan = COM1;
	SendReq.Addr = (void *)storage;
	Send(IServerID,(void*)&SendReq, sizeof(IOReq), (void *)&ReplyMsg, sizeof(int));
	//Printf(COM2,"%s%c[%d;%dH%d,%d,%d,%d%s",SAVE_CURSOR,ESC,24,80,storage[0],storage[1],storage[2],storage[3],RESTORE_CURSOR);
	BufferClear(&FIFO);
      }
      // *(int *)(UART1_BASE | UART_CTLR_OFFSET) |= ( 1<<4 );
    }
    else break;
  }
}


void TrainOutNotifier(){
  int Res,OServerID;
  char ReplyByte;
  IOReq SendReq;
  Res = RegisterAs("TrainOutNotifer");
  OServerID = WhoIs("oServer");
  int CLKId = WhoIs("ClockServer");

  const int * Universal = (int *)GetShared();
  
  int TxLen = 3;
  Buffer FIFO;
  char BufStore[3];
  Res = InitBuffer(&FIFO,(void *)BufStore,3);
  
  FOREVER{
    //bwprintf(COM2,"TrainOut: before await\n\r");
    
    //bwprintf(COM2,"%c[%d;%dH BA\n\r",ESC,1,120);

    Res = AwaitEvent(UART1_INT);
    if(Res > -1){
      if(FIFO.Length == 0){
	BufferClear(&FIFO);
	SendReq.Type = NOUT;
	SendReq.Chan = COM1;
	SendReq.Addr = (void *)BufStore;
	SendReq.Len = 3;
	//bwprintf(COM2,"%c[%d;%dHTRON: tx %d\n\r",ESC,1,120, TxLen);
	Send(OServerID,(void *)&SendReq,sizeof(IOReq),(void *)&TxLen, sizeof(int));
	//bwprintf(COM2,"%c[%d;%dHTRON: AF0 %d\n\r",ESC,1,120,TxLen);
	FIFO.Length = FIFO.Tail = TxLen;
      }
      BufferFIFOByte(&FIFO,(void *)&ReplyByte);
      BufferPopHead(&FIFO);
      //bwprintf(COM2,"%c[%d;%dHTRON: AF2 %d %d %d \n\r",ESC,1,120,ReplyByte,FIFO.Length,FIFO.Head);
      *(int *)(UART1_BASE) = ReplyByte;
      Delay(CLKId,5);
      //*(int *)(UART1_BASE | UART_CTLR_OFFSET) |= (1 << 5 | 1 << 3);
      //bwprintf(COM2,"TrainOut: IRQ2:%d\n\r",(*(int *)(VIC2_BASE | INT_ENABLE)));
    }
    else break;
  }
}
