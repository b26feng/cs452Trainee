#include <buffer.h>
#include <bwio.h>
#include <nameserver.h>
#include <rps.h>
#include <syscall.h>
#include <td-shared.h>
#include <types.h>

extern Buffer*** TotalSendQ;

void GamePlay(RPSChoice Client1Move, RPSChoice Client2Move,RPSMsg* reply, int Client1, int Client2){
  if (Client1Move != None && Client2Move != None) {
    int Diff = (int) Client1Move - (int)Client2Move;
    if (Diff == 0) {
      reply->Msg = Draw;
      bwprintf(COM2, "We have a Draw.\n\r");
      Reply(Client1, (void *)reply, sizeof(RPSMsg));
      Reply(Client2, (void *)reply, sizeof(RPSMsg));
    } 
    else if (Diff == 1 || Diff == -2) {
      bwprintf(COM2, "Client(%d) Wins.\n\r",Client1);
      reply->Msg = Win;
      Reply(Client1, (void *)reply, sizeof(RPSMsg));
      reply->Msg = Lose;
      Reply(Client2, (void *)reply, sizeof(RPSMsg));
    }
    else {
      bwprintf(COM2, "Client(%d) Wins.\n\r",Client2);
      reply->Msg = Lose;
      Reply(Client1, (void *)reply, sizeof(RPSMsg));
      reply->Msg = Win;
      Reply(Client2, (void *)reply, sizeof(RPSMsg));
    }
  }
}

int RPSServer(){
  
  char Name[] = "RPSServer";
  // register to name server
  int  Result = RegisterAs(Name);
  if(Result == FAILURE) {
    bwprintf(COM2, "%s: Cannot register! %s\n\r",Name,Name);
    return FAILURE;
  }
  bwprintf(COM2,"%s: Successfully Regiseterd!\n\r",Name);

  // setup send q
  
  // setup send q
  int MyID = MyTid();
  int MyIndex = GetMemoryIndexINT(MyID);
  Buffer** AllSendQ = GetAllSendQ();

  //Pass();
  Buffer SendQ;
  int QStorage[MAX_NUM_TD];
  SendQ.Storage = (void *)QStorage;
  SendQ.Size = MAX_NUM_TD;
  SendQ.Length = 0;
  SendQ.Head = 0;
  SendQ.Tail = 0;
  //bwprintf(COM2,"%s: MyIndex %d\n\r",Name,MyIndex);
  //Pass();
  //bwprintf(COM2,"%s: SendQ Addr %d\n\r",Name,&SendQ);
  AllSendQ[MyIndex] = &SendQ;// problem
  //bwprintf(COM2,"%s: SendQ setup\n\r",Name);
  int TaskID;
  int Client1 = -1;
  int Client2 = -1;
  RPSMsg ReceiveMsg, ReplyMsg;
  RPSChoice Client1Move = None, Client2Move = None;
  
  
  int bOtherQuit = 0;
  //bwprintf(COM2,"%s: Before Loop\n\r",Name);
  FOREVER {
    TaskID = NULL;
    Result = BufferFIFO(&SendQ,(void *)&TaskID);
    if(TaskID != NULL){
      Result = Receive(TaskID, &ReceiveMsg, sizeof(ReceiveMsg));
      if(bOtherQuit != 0){
	bwprintf(COM2,"%s: other player quit\n\r",Name);
	ReplyMsg.Msg = OtherQuit;
	Reply(TaskID, &ReplyMsg, sizeof(ReplyMsg));
      }else{
	switch (ReceiveMsg.Msg) {
	  
	case Quit:
	  bwprintf(COM2,"%s: Player with tid(%d) quit\n\r",Name, TaskID);
	  ReplyMsg.Msg = Bye;
	  Reply(TaskID,&ReplyMsg,sizeof(ReplyMsg));
	  // xor Client1 with TaskID, if same, result 0, then set to 0
	  Client1 = (Client1 ^ TaskID)? Client1: 0;
	  Client2 = (Client2 ^ TaskID)? Client2: 0;
 	 
	  bOtherQuit = ((Client2 == Client1) &&(Client1 == 0))? 0:1;
	  break;
	case SignUp:
	  bwprintf(COM2,"%s: Player with tid(%d) signed up\n\r",Name,TaskID);
	  if(Client1 == -1){
	    Client1 = TaskID;
	  }else{
	    Client2 = TaskID;
	  }
	  ReplyMsg.Msg = LetsPlay;
	  Reply(TaskID, &ReplyMsg, sizeof(ReplyMsg));
	  break;
	  
	case Play:
	// only one player
	  if (Client1 == 0 || Client2 == 0) {
	    Client2Move = (Client1 == 0)? ReceiveMsg.Choice: None;
	    Client1Move = (Client2 == 0)? ReceiveMsg.Choice: None;
	    ReplyMsg.Msg = OnePlayer;
	    Reply(TaskID, &ReplyMsg, sizeof(ReplyMsg));
	    break;
	  }
	  Client1Move = (Client1 == TaskID)? ReceiveMsg.Choice: Client1Move;
	  Client2Move = (Client2 == TaskID)? ReceiveMsg.Choice: Client2Move;
	  if((Client1Move != None) && (Client2Move != None)){
	    GamePlay(Client1Move, Client2Move, &ReplyMsg, Client1, Client2);
	    Client1Move = Client2Move = None;
	  }
	  break;
	  
	default:
	  break;
	}
      }
    }else{
      Pass();
    }
  }
  return SUCCESS;
}

int Player(int ID){
  char Name[] = "RPSClient0";
  Name[9] = ID;
  int Result = RegisterAs(Name);
  if(Result == FAILURE) {
    //bwprintf(COM2, "%s: Cannot register %s\n\r",Name);
    return FAILURE;
  }
  
  char Input = 'U'; // for Undefined
  int MyTID, ServerTID;
  RPSMsg SendMsg, ReplyMsg;
  
  MyTID = MyTid();
  ServerTID = WhoIs("RPSServer");
  if (ServerTID < 0) return FAILURE;

  SendMsg.Msg = SignUp;
  
  Result = Send(ServerTID, &SendMsg, sizeof(RPSMsg), &ReplyMsg, sizeof(RPSMsg));
  
  if (Result != sizeof(ReplyMsg)) {
    bwprintf(COM2, "Sign Up Request to Server did not go through.");
    return Result;
  }
  
  //if (ReplyMsg.MsgType == ServerFull) //TODOTODOTODOTODO
  FOREVER {
    bwprintf(COM2, "Please enter r/R for Rock, p/P for Paper, s/S for Scissors.\n\r");
    while (Input = bwgetc(COM2)) {
      bwputc(COM2,Input);
      if (Input == 'r' || Input == 'R') {
	SendMsg.Choice = Rock;
	break;
      }
      else if (Input == 'p' || Input == 'P') {
	SendMsg.Choice = Paper;
	break;
      }
      else if (Input == 's' || Input == 'S') {
	SendMsg.Choice = Scissors;
	break;
      }else if(Input == 'q' || Input == 'Q'){
	SendMsg.Msg = Quit;
      }
      else {
	Input = 'U';
	bwprintf(COM2, "Please enter r/R for Rock, p/P for Paper, s/S for Scissors.\n\r");
      }
    }
    
    SendMsg.Msg = Play;
    Result = Send(ServerTID, &SendMsg, sizeof(SendMsg), &ReplyMsg, sizeof(ReplyMsg));
    if (Result != sizeof(ReplyMsg)) {
      if(SendMsg.Msg == Quit){
	bwprintf(COM2, "Request for QUIT from %s did not go through gracefully.", Name);
      }else{
	bwprintf(COM2, "%s: Game obstructed!",Name);
      }
      return FAILURE;
    }else{
      if(SendMsg.Msg == Quit){
	bwprintf(COM2, "%s exitting...",Name);
	Result = ReplyMsg.Msg;
	break;
      }
    }
  }
  return SUCCESS;
}


int RPSClient1(){
  Player(1);
  Exit();
}


int RPSClient2(){
  Player(2);
  Exit();
}
