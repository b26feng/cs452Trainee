#include <bwio.h>
#include <buffer.h>
#include <ipc.h>
#include <kernel.h>
#include <scheduler.h>
#include <td-shared.h>
#include <tools.h>
#include <types.h>


int doSend(KernelStruct* Colonel, int TaskID, void* Msg, int MsgLen, void* ReplyMsg, int ReplyLen) {
  Colonel->Active->RetVal = SRR_FAILED;
  Buffer** AllSendQ = Colonel->AllSendQ;
  int Result;
  // check the TD availability
  int TIDIndex = GetMemoryIndexINT(TaskID);
  //bwprintf(COM1,"doSend: TID: %d, TID Index:%d\n\r",TaskID, TIDIndex);
  TD* SenderTask = Colonel->Active;
  if(TIDIndex >= MAX_NUM_TD){
    //bwprintf(COM2,"doSend: TID Index:%d out of MaxNumTD\n\r", TIDIndex);
    SenderTask->RetVal = TASK_DNE;
    return FAILURE;
  }
  TD* ReceiverTask = &(Colonel->Tasks[TIDIndex]);
  if(GetAvailability(ReceiverTask)){
    //bwprintf(COM2,"doSend: Task %d Availability %d\n\r",TaskID, GetAvailability(ReceiverTask));
    SenderTask->RetVal = TASK_DNE;
    return FAILURE;
  }
  
  //bwprintf(COM1,"doSend: Task %d SendMsgLen:%d\n\r",SenderTask->TaskID,MsgLen);
  //bwprintf(COM1,"doSend: Task %d got recorded on %d sendQ, result %d\n\r",SenderTask->TaskID, TaskID, Result);
  if (ReceiverTask->TaskState == SendBlocked) {
    
    AP* Args = ReceiverTask->Args;
    Message *ReceiverSlot = (Message *)Args->arg1;
    int ReceiverLen = (int)(Args->arg2);
    //if(TaskID == 4 && SenderTask->TaskID!=2) bwprintf(COM2,"doSend: ClockServer sendblocked\n\r");
    //bwprintf(COM1,"doSend: Receiver %d SendBlocked\n\r", TaskID);
    ReceiverSlot->SenderID = SenderTask->TaskID;
    Result = byteCopy(ReceiverSlot->Addr, Msg, ReceiverLen, MsgLen);
    if(SenderTask->TaskID == 12){
      //IOReq* TONArgs = (AP*)(Msg);
      IOReq* IOR = (IOReq*)(Msg);
      //bwprintf(COM2,"doSend: TON %d\n\r",IOR->Type);
      //bwprintf(COM2,"doSend: TON\n\r");
      //IOR = Args->arg1
    }
    if (Result == FAILURE){
      //bwprintf(COM1,"doSend: byteCopy Failed\n\r");
      ReceiverTask->RetVal = MSG_TRUNCATED;
      return Result;
    }
    SenderTask->TaskState = ReplyBlocked;
    ReceiverTask->TaskState = Ready;
    ReceiverTask->RetVal = MsgLen;
    pushToScheduler(Colonel, ReceiverTask);
    //BufferPopHead(AllSendQ[TIDIndex]);
  }else {
    Result = FeedBuffer(AllSendQ[TIDIndex],(void *)(SenderTask->TaskID));
    //if(SenderTask->TaskID == 8) bwprintf(COM2,"dS: %d L %d\n\r",TIDIndex,AllSendQ[TIDIndex]->Length);
    //if(TaskID == 4 && SenderTask->TaskID!=2) bwprintf(COM2,"doSend: ClockServer not received, %d RBblocked\n\r",SenderTask->TaskID);
    //bwprintf(COM2,"doSend: Receiver %d not Received, Task %d RBLocked\n\r",TaskID,SenderTask->TaskID);
    SenderTask->TaskState = ReceiveBlocked;
    //bwprintf(COM2,"doSend: add Task(%d) to SendQ[%d]\n\r",SenderTask->TaskID,TIDIndex);
    //bwprintf(COM2,"doSend: Guess 4, after feed buffer\n\r");
  }
  Colonel->Active = NULL;
  //int id = SenderTask->TaskID;
  /*if(id == 8 || id == 3){
    Colonel->Active = getNextTaskScheduler(Colonel);
    bwprintf(COM2,"dS:->%d\n\r",Colonel->Active->TaskID);
    }*/
  return SUCCESS;
}

int doReceive(KernelStruct* Colonel, int TaskID, void* Msg, int MsgLen) {
  //bwprintf(COM2,"dR: %d <- %d\n\r",Colonel->Active->TaskID,TaskID);
  /*if(Colonel->Active->TaskID == 5){
    bwprintf(COM2,"dR: ->%d\n\r",TaskID);
    }*/
    TD* ReceiverTask = Colonel->Active; // me
    int RxIndex = GetMemoryIndexINT(ReceiverTask->TaskID);
    Buffer** AllSendQ = Colonel->AllSendQ;
    int TID = TaskID;
    /*if(Colonel->Active->TaskID == 5){
      bwprintf(COM2,"dR1: ->%d %d\n\r",TaskID,RxIndex);
    }*/
    if(TID < 0){
      if(AllSendQ[RxIndex]->Length > 0){
	/*
	if(Colonel->Active->TaskID == 5){
	  bwprintf(COM2,"dR2: L %d\n\r",AllSendQ[RxIndex]->Length);
	  }*/
	BufferFIFO(AllSendQ[RxIndex],&TID);
	BufferPopHead(AllSendQ[RxIndex]);
      }else{/*
	if(Colonel->Active->TaskID == 13){
	  bwprintf(COM2,"dR3: ->%d\n\r",TaskID);
	  }*/
	ReceiverTask->TaskState = SendBlocked;
	Colonel->Active = NULL;
	return SUCCESS;
      }
    }
    int TIDIndex = GetMemoryIndexINT(TID);
    TD* SenderTask = &(Colonel->Tasks[TIDIndex]);
    int Result;
    AP* Args = SenderTask->Args;
    Message* M = (Message *)Msg;

    void* SendMsg = Args->arg1;
    int SendMsgLen = (int)Args->arg2;
    //bwprintf(COM2,"Message: %d\n\r",M->Addr);

    if (SenderTask->TaskState!= ReceiveBlocked) {
      //bwprintf(COM2,"dR: not yet\n\r");
      ReceiverTask->TaskState = SendBlocked;
      Colonel->Active = NULL;
    }else{
      //bwprintf(COM2,"doReceive,MsgLen:%d,SendMsgLen:%d\n\r",MsgLen,SendMsgLen);
      M->SenderID = TID;
      Result = byteCopy(M->Addr, SendMsg, MsgLen, SendMsgLen);
      //bwprintf(COM2,"doReceive: Receive Message from %d, result: %d\n\r",SenderTask->TaskID, Result);
      if (Result == FAILURE){
	ReceiverTask->RetVal = MSG_TRUNCATED;
	return Result;
      }
      SenderTask->TaskState = ReplyBlocked;
      //bwprintf(COM2,"doReceive: Sender%d Reply Blocked\n\r",TaskID);
      //Result = BufferPopHead(AllSendQ[GetMemoryIndexINT(ReceiverTask->TaskID)]);
      //bwprintf(COM2,"doReceive: SendQ[%d] popped head\n\r",(ReceiverTask->TaskID));
      Colonel->Active->RetVal = MsgLen;
    }
    //bwprintf(COM2,"doReceive: Returning with success\n\r");
    return SUCCESS;
}

int doReply(KernelStruct* Colonel, int TaskID, void* ReplyMsg, int ReplyLen) {
  //bwprintf(COM1,"doReply: ReplyMsg: %d\n\r",ReplyMsg);
  TD* ReplyTask = Colonel->Active; // me
  int TIDIndex = GetMemoryIndexINT(TaskID);
  int ReplyIndex = GetMemoryIndexINT(ReplyTask->TaskID);
  TD* SenderTask = &(Colonel->Tasks[TIDIndex]);
  Buffer** AllSendQ = Colonel->AllSendQ;
  if(TIDIndex >= MAX_NUM_TD){
    //bwprintf(COM2,"doReply: TID:%d invalid \n\r", TIDIndex);
    SenderTask->RetVal = TASK_DNE;
    return FAILURE;
  }
  if(GetAvailability(SenderTask)){
    SenderTask->RetVal = TASK_DNE;
    return FAILURE;
  }
  
  AP* Args = SenderTask->Args;
  int Result;
  
  void* SenderReply = Args->arg3;
  int SenderLen = (int)Args->arg4;
  //Result = BufferPopHead(AllSendQ[ReplyIndex]);
  //bwprintf(COM2,"doReply: poped from ALlSendQ[%d]\n\r",ReplyIndex);
  if(SenderTask->TaskState != ReplyBlocked){
    //bwprintf(COM2,"doReply: Sender is not in ReplyBlocked state\n\r");
    Colonel->Active->RetVal = SRR_FAILED;
    return FAILURE;
  }
  
  Result = byteCopy(SenderReply, ReplyMsg, SenderLen, ReplyLen);
  if(Result != SUCCESS){
    //bwprintf(COM2,"doReply: MSG_TRUNCATED\n\r");
    SenderTask->RetVal = MSG_TRUNCATED;
    return Result;
  }
  
  SenderTask->TaskState = Ready;
  SenderTask->RetVal = ReplyLen;
  Colonel->Active->RetVal = 0;
  //bwprintf(COM2,"doReply: push %d back to scheduler\n\r",TaskID);
  pushToScheduler(Colonel, SenderTask);
  return SUCCESS;
}
