#include <bwio.h>
#include <timer.h>
#include <ts7200.h>

void ClockInit(Clock *MyClock)
{
  //bwprintf(COM2,"Entered Clock init\n\r");
  MyClock->ClockCount = 0;
  MyClock->LastTimerValue = CLOCK_LOAD_VALUE;
  unsigned int* Clock3LoadRegister = (unsigned int *)(TIMER3_BASE + LDR_OFFSET);
  unsigned int* Clock3ControlRegister = (unsigned int *)(TIMER3_BASE + CRTL_OFFSET);
  unsigned int ClockConfig = MODE_MASK | CLKSEL_MASK;
  // set interval
  *Clock3LoadRegister = CLOCK_LOAD_VALUE;
  // enable clock
  *Clock3ControlRegister = ENABLE_MASK | ClockConfig;
  //bwprintf(COM2,"clock init!!!!\n\r");
}

void ClockUpdate(Clock *MyClock)
{
  unsigned int *ValRegister = (unsigned int *)(TIMER3_BASE+VAL_OFFSET);
  unsigned int CurrentTimerValue = *ValRegister;
  if(CurrentTimerValue > MyClock->LastTimerValue){
    MyClock->ClockCount = MyClock->ClockCount + 1;
    MyClock->bChanged = 1;
  } else {
    MyClock->bChanged = 0;
  }
  MyClock->LastTimerValue = CurrentTimerValue;
}

void ClearAllTimer()
{
  //bwprintf(COM2,"CLR: All Timer\n\r");
  asm volatile(".T1CLR:\n\t"
	       ".word 0x8081000c\n\t"
	       "ldr r0, .T1CLR\n\t"
	       "mov r1, #1\n\t"
	       "str r1,[r0]\n\t"
	       "add r0, r0, #0x20\n\t"
	       "str r1,[r0]\n\t"
	       "add r0, r0, #0x60\n\t"
	       "str r1,[r0]\n\t"
	       );
}
