#include <bwio.h>
#include <canvas.h>
#include <interrupt-io.h>
#include <shortlife.h>
#include <syscall.h>
#include <td-shared.h>
#include <train-controller.h>
#include <types.h>

void Watcher(){
  SSMsg SensorS,SensorR;
  TCMsg TCS,TCR;
  //Printf(COM2,"WATCHER\n\r");
  int TID = MyTid();
  int Generation = GetGenerationINT(TID);
  int SensorID = WhoIs("SensorServer");
  int TCID = WhoIs("TCServer");
  int TrainID, Sensor;
  char* Sensors;
  
  const int* Universal = (int *)GetShared();
  TCS.Type = WATCHER;
  Send(TCID,(void*)&TCS, sizeof(TCMsg),(void *)&TCR,sizeof(TCMsg));
  TrainID = TCR.Num;
  Sensor = TCR.TrainSpeed;
  Printf(COM2,"%s%c[%d;%dH%d Stop After %d:%d, TC:%d at %d%s",SAVE_CURSOR,ESC,42,80,TID,TCR.Num,TCR.TrainSpeed,TCID,*Universal,RESTORE_CURSOR);
  int index = ((Sensor>>3)+1)%10;
  //Printf(COM2,"Watcher %d %d\n\r",Sensor>>3,(Sensor-((Sensor>>3)<<3)));
  //Printf(COM2,"Watcher %d %d \n\r",TrainID,Sensor);
  FOREVER{
    SensorS.Type = SENSOR_QUERY;
    //Printf(COM2,"%s%c[%d;%dH%dSND%s",SAVE_CURSOR,ESC,5,80,Generation,RESTORE_CURSOR);
    Send(SensorID,(void *)&SensorS,sizeof(SSMsg),(void*)&SensorR,sizeof(SensorR));
    Sensors = (char *)SensorR.SensorInput;
    //Printf(COM2,"%s%c[%d;%dHSensor: %d,Index: %d,Bit: %d%s",SAVE_CURSOR,ESC,1,80,Sensor,Sensor>>3,(Sensor-((Sensor>>3)<<3)),RESTORE_CURSOR);
    
    
    if(Sensors[index] & (128 >> (Sensor-((Sensor>>3)<<3)))){
      Printf(COM2,"%s%c[%d;%dHTimeStamp1:%d%s",SAVE_CURSOR,ESC,46,80,*(Universal),RESTORE_CURSOR);
      TrainSetSpeed(TCID,TrainID,0);
      SelfDestroy();
    }
  }
}

void Stamper(){
  SSMsg SensorS,SensorR;
  TCMsg TCS,TCR;
  int TID = MyTid();
  int Generation = GetGenerationINT(TID);
  int SensorID = WhoIs("SensorServer");
  int TCID = WhoIs("TCServer");
  int Sensor1, Sensor2;
  char* Sensors;
  Sensor1 = Sensor2 = -1;
  
  const int* Universal = (int *)GetShared();
  TCS.Type = WATCHER;
  Send(TCID,(void*)&TCS, sizeof(TCMsg),(void *)&TCR,sizeof(TCR));
  Sensor1 = TCR.Num;
  Sensor2 = TCR.TrainSpeed;

  int index1 = ((Sensor1>>3)+1)%10;
  int index2 = ((Sensor2>>3)+1)%10;
  Printf(COM2,"%s%c[%d;%dHSTAMPER %d %d%s\n\r",SAVE_CURSOR,ESC,41,80,Sensor1,Sensor2,RESTORE_CURSOR);
  // Printf(COM2,"Watcher %d %d\n\r",Sensor>>3,(Sensor-((Sensor>>3)+1<<3)));
  //Printf(COM2,"Watcher %d %d \n\r",TrainID,Sensor);
  FOREVER{
    SensorS.Type = SENSOR_QUERY;
    //Printf(COM2,"%s%c[%d;%dH%dSND%s",SAVE_CURSOR,ESC,5,80,Generation,RESTORE_CURSOR);
    Send(SensorID,(void *)&SensorS,sizeof(SSMsg),(void*)&SensorR,sizeof(SensorR));
    Sensors = (char *)SensorR.SensorInput;
    if(Sensor1>=0 && (Sensors[index1]&(128 >> (Sensor1-((Sensor1>>3)<<3))))){
      Printf(COM2,"%s%c[%d;%dHTimeStamp1:%d%s",SAVE_CURSOR,ESC,47,80,*(Universal),RESTORE_CURSOR);
      Sensor1 = -1;
    }else if(Sensor2>=0 && (Sensors[index2] & (128 >> (Sensor2-((Sensor2>>3)<<3))))){
      Printf(COM2,"%s%c[%d;%dHTimeStamp2:%d%s",SAVE_CURSOR,ESC,48,80,*(Universal),RESTORE_CURSOR);
      Sensor2 = -1;
    }
    if(Sensor1 >> 31 && Sensor2 >>31) SelfDestroy();
  }
}


void ShortDistance(){
  TCMsg TCS,TCR;
  int TID = MyTid();
  int Generation = GetGenerationINT(TID);
  int ClkID = WhoIs("ClockServer");
  int TCID = WhoIs("TCServer");
  
  const int* Universal = (int *)GetShared();
  TCS.Type = WATCHER;
  Send(TCID,(void*)&TCS, sizeof(TCMsg),(void *)&TCR,sizeof(TCR));
  int TrainID = TCR.Num;
  int Tick = TCR.TrainSpeed;
  Train* A = (Train *)TCR.SwitchDir;
  
  
  //Printf(COM2,"%s%c[%d;%dHEstimateDistance:%d Train:%d%s",SAVE_CURSOR,ESC,12,80,SDGetDistance(A,Tick),A,RESTORE_CURSOR);
  
  Printf(COM2,"%s%c[%d;%dHTimeStamp1:%d%s",SAVE_CURSOR,ESC,47,80,*(Universal),RESTORE_CURSOR);
  TrainSetSpeed(TCID,TrainID,14);
  Printf(COM2,"%s%c[%d;%dHTimeStamp2:%d%s",SAVE_CURSOR,ESC,48,80,*(Universal),RESTORE_CURSOR);
  Delay(ClkID,Tick);
  Printf(COM2,"%s%c[%d;%dHTimeStamp3:%d%s",SAVE_CURSOR,ESC,49,80,*(Universal),RESTORE_CURSOR);
  TrainSetSpeed(TCID,TrainID,0);
  Printf(COM2,"%s%c[%d;%dHTimeStamp4:%d%s",SAVE_CURSOR,ESC,50,80,*(Universal),RESTORE_CURSOR);
  SelfDestroy();
}

void ShortDistanceTick(){
  TCMsg TCS,TCR;
  int TID = MyTid();
  int Generation = GetGenerationINT(TID);
  int ClkID = WhoIs("ClockServer");
  int TCID = WhoIs("TCServer");
  
  const int* Universal = (int *)GetShared();
  TCS.Type = WATCHER;
  Send(TCID,(void*)&TCS, sizeof(TCMsg),(void *)&TCR,sizeof(TCR));
  int TrainID = TCR.Num;
  int Distance = TCR.TrainSpeed;
  Train* A = (Train *)TCR.SwitchDir;
  int Tick = SDGetTicks(A,Distance);
  
  Printf(COM2,"%s%c[%d;%dHDistance:%d Estimate Tick:%d%s",SAVE_CURSOR,ESC,52,80,Distance,Tick,RESTORE_CURSOR);
  
  Printf(COM2,"%s%c[%d;%dHTimeStamp1:%d%s",SAVE_CURSOR,ESC,47,80,*(Universal),RESTORE_CURSOR);
  TrainSetSpeed(TCID,TrainID,14);
  Printf(COM2,"%s%c[%d;%dHTimeStamp2:%d%s",SAVE_CURSOR,ESC,48,80,*(Universal),RESTORE_CURSOR);
  Delay(ClkID,Tick);
  Printf(COM2,"%s%c[%d;%dHTimeStamp3:%d%s",SAVE_CURSOR,ESC,49,80,*(Universal),RESTORE_CURSOR);
  TrainSetSpeed(TCID,TrainID,0);
  Printf(COM2,"%s%c[%d;%dHTimeStamp4:%d%s",SAVE_CURSOR,ESC,50,80,*(Universal),RESTORE_CURSOR);
  SelfDestroy();
}
