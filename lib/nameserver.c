#include <buffer.h>
#include <bwio.h>
#include <hash-table.h>
#include <nameserver.h>
#include <syscall.h>
#include <td-shared.h>
#include <tools.h>
#include <types.h>

void NameServer(){
  int TIDSeq[HASH_SIZE];
  int i = 0, ClockServerID;
  for(;i<HASH_SIZE;i++){
    TIDSeq[i] = -1;
  }
  char Names[HASH_SIZE*(NAME_LENGTH_MAX+1)];

  // set up send q
  int MyID = MyTid();
  int MyIndex = GetMemoryIndexINT(MyID);
  Buffer** AllSendQ = GetAllSendQ();

  //bwprintf(COM2,"NS Index %d\n\r",MyIndex);
  // bwprintf(COM2,"NS AllSendQ add%d\n\r",AllSendQ);
  Buffer SendQ;
  int QStorage[MAX_NUM_TD];
  SendQ.Storage = (void *)QStorage;
  SendQ.Size = MAX_NUM_TD;
  SendQ.Length = 0;
  SendQ.Head = 0;
  SendQ.Tail = 0;
  AllSendQ[MyIndex] = &SendQ;

  
  //bwprintf(COM2,"NS: SendQ setup \n\r");
  //bwprintf(COM2,"SendQ Setup\n\r");
  // initialize Names
  for(i=0;i<HASH_SIZE;i++){
    Names[i*(NAME_LENGTH_MAX+1)-1] = 0;
  }
  
  HT HashTable;
  HashTable.TidSequence = TIDSeq;
  HashTable.NameSequence = Names;
  HashTable.size = HASH_SIZE;
  
  //bwprintf(COM2,"HashTable Setup\n\r");
  
  // some sort of while loop to receive and reply
  int TaskToReceive;
  NSReq ReceiveSlot;
  Message ReceiveMsg;
  ReceiveMsg.Addr = (void *)(& ReceiveSlot);
  //bwprintf(COM2,"NS: ReceiveSlot %d\n\r",&ReceiveSlot);
  int Result;
  int StrLen;

  int bReceived = 0;
  FOREVER{
    //TaskToReceive = NULL;
    if(!bReceived){
      Result = BufferFIFO(&SendQ,(void *)&TaskToReceive);
      BufferPopHead(&SendQ);
    }
    if(Result == 0){
      //if(bReceived) bwprintf(COM2,"NS: Got %d <- Null\n\r",TaskToReceive);
      if(bReceived == 0){
	//bwprintf(COM2,"NS: Got %d <- SQ\n\r",TaskToReceive);
	Result = Receive(TaskToReceive,(void*) &ReceiveMsg, sizeof(NSReq));
	TaskToReceive = ReceiveMsg.SenderID;
	//bwprintf(COM2,"NS: after receive %d\n\r",TaskToReceive);
      }
      bReceived = 0;
      //bwprintf(COM2,"NS_LOOP:Task %d Receive Result: %d\n\r",TaskToReceive,Result);
      //bwprintf(COM2,"NS_LOOP: Msg:%s\n\r",ReceiveSlot.Msg);
      StrLen = stringLen(ReceiveSlot.Msg) + 1;
      //bwprintf(COM2,"NS_LOOP:after string length function\n\r");
      if(ReceiveSlot.MsgType == WhoIS){
	//bwprintf(COM2,"NS_LOOP: msg type == %d\n\r",ReceiveSlot.MsgType);
      //bwprintf(COM2,"NS_LOOP: before hash look up, Sender: %d\n\r",TaskToReceive);
	//if(TaskToReceive == 16) Printf(COM2,"WhoIs: %s\n\r",ReceiveSlot.Msg);
	Result = Lookup(&HashTable, ReceiveSlot.Msg,StrLen);
	//if(TaskToReceive == 16) Printf(COM2,"Result: %d\n\r",Result);
	//bwprintf(COM2,"NS_LOOP: hash look up result %d\n\r",Result);
	Reply(TaskToReceive,(void *)(&Result), sizeof(int));
      }else if(ReceiveSlot.MsgType == RegAS){
	Result = InsertTo(&HashTable,ReceiveSlot.TaskID, ReceiveSlot.Msg,StrLen);
	//bwprintf(COM2,"Hash:%d\n\r",Result);
	if(TaskToReceive == 15) Printf(COM2,"%s:%d Result%d\n\r",ReceiveSlot.Msg,ReceiveSlot.TaskID,Result);
	//if(TaskToReceive == 13) Printf(COM2,"TS Result%d\n\r",Result);
	if (ReceiveSlot.Msg == "ClockServer") ClockServerID = ReceiveSlot.TaskID;
      //bwprintf(COM2,"NS_USR: Inserted to %d, %d, %s\n\r",Result, &(Names[Result*17]),(Names + (Result * 17)));
	Reply(TaskToReceive,(void *)(&Result), sizeof(int));
	//bwprintf(COM2,"NS: %d Registered\n\r",TaskToReceive);
      }
    }else{
      //bwprintf(COM2,"NS: Receive as SendQ is empty\n\r");
      bReceived = 1;
      Result = 0;
      //bwprintf(COM2,"NS: receive to NULL\n\r");
      Receive(-1,(void*) &ReceiveMsg, sizeof(NSReq));
      TaskToReceive = ReceiveMsg.SenderID;
      //bwprintf(COM2,"NS: back Null, task:%d\n\r",TaskToReceive);
    }
  }
}

int ActualWhoIS(AP* Args){
	asm volatile ("swi 8\n\t");
	register int r0 asm ("r0");
	int R = r0;
	return R;
}

int WhoIs(char* Name) {
  NSReq SendReq;
  int ReplyTID;
  int Status;
  AP Args;
  
  stringCopy(SendReq.Msg, Name, NS_MESSAGE_MAX_LENGTH);
  //bwprintf(COM2,"WhoIs: Name: %s, SendReq.Msg: %s\n\r",Name, SendReq.Msg);
  SendReq.MsgType = WhoIS;
  //bwprintf(COM2,"WhoIs: MsgType: %d\n\r",SendReq.MsgType);
  
  Args.arg1 = (void *)(&SendReq);
  Args.arg2 = (void *)sizeof(NSReq);
  Args.arg3 = (void *)(&ReplyTID);
  Args.arg4 = (void *)sizeof(int);

  //bwprintf(COM2,"NS_USR: WhoIs %s\n\r",Name);
  Status = ActualWhoIS(&Args);
  
  if (Status == sizeof(int)) return ReplyTID; 
  return NAMESERVER_TID_INVALID;
}
int ActualRegAS(AP* Args){
	asm volatile ("swi 9\n\t");
	register int r0 asm ("r0");
	int R = r0;
	return R;
}

int RegisterAs (char* Name) {
  NSReq SendReq;
  int Result;
  int Status;
  AP Args;
  
  SendReq.TaskID = MyTid();
  stringCopy(SendReq.Msg, Name, NS_MESSAGE_MAX_LENGTH);
  SendReq.MsgType = RegAS;
  //bwprintf(COM2,"RegAS: MsgType %d\n\r",RegAS);
  
  Args.arg1 = (void *)(&SendReq);
  Args.arg2 = (void *)sizeof(NSReq);
  Args.arg3 = (void *)(&Result);
  Args.arg4 = (void *)sizeof(int);

  
  //bwprintf(COM2,"NS_USR: registering %s\n\r",Name);
  Status = ActualRegAS(&Args);
  
  if (Status == sizeof(int)) return Result;
  return NAMESERVER_TID_INVALID;
}
