#include <bwio.h>
#include <event-itr.h>
#include <IRQ.h>
#include <scheduler.h>
#include <ts7200.h>
#include <types.h>

void InitWaitEventTable(EventAwaitTask* WaitEventTable) {
  int i;
  for (i = 0; i < NUM_EVENTS; i++) {
    WaitEventTable[i].AwaitTask = NULL;
    WaitEventTable[i].Data = NULL;
  }
}

int IsValidEvent(int EventID) {
  return (EventID < NUM_EVENTS) && (EventID >= 0);
}

int GetEventID(const int IRQ1, const int IRQ2){
  int mask = 1;
//   bwprintf(COM2,"Get event %d %d \n\r", IRQ1, IRQ2);
  if((mask<<25) & IRQ1) return 25;
  if((mask<<26) & IRQ1) return 26;
  if((mask<<20) & IRQ2) return 52;
  if((mask<<19) & IRQ2) return 51;
}

int doAwaitEvent(KernelStruct* Colonel, int EventID) {
  EventAwaitTask* WET = Colonel->WaitTable;
  char bNegative = 0;
  // check EventID's validity
  if (!IsValidEvent(EventID)) {
    //bwprintf(COM2, "doAwaitEvent: Invalid Event\n\r");
    if(!IsValidEvent(-EventID)) return INVALID_EVENT;
    bNegative = 1;
  }
  
  if (WET[EventID].AwaitTask == NULL || WET[-EventID].Data == NULL) {
    if(!bNegative){
      WET[EventID].AwaitTask = Colonel->Active;
    }else{
      WET[-EventID].Data = (void *)(Colonel->Active);
    }
    Colonel->Active->TaskState = EventBlocked;
    //if(!EventID == 51) bwprintf(COM1,"%d -> %d\n\r",WET[EventID].AwaitTask->TaskID,EventID);
    if(EventID == UART2OUT_INT){
      EnableIRQ(1<<UART2OUT_INT,VIC1_BASE);
      //bwprintf(COM2,"DWE:%d\n\r",*(int *)(VIC1_BASE | INT_ENABLE));
    }else if(EventID == UART1_INT || -EventID == UART1_INT){
      
      int* Uart1Status =(int *)(UART1_BASE | UART_CTLR_OFFSET);
      if(bNegative){
	*Uart1Status |= (1<<4);
	//bwprintf(COM2,"DWE: RX %d\n\r",*Uart1Status);
      }else{
	*Uart1Status |= (1<<5 | 1<<3);
	//bwprintf(COM2,"DWE: TX %d\n\r",((*Uart1Status)<<24)>>24);
      }
      
    }else{
      int Offset = (EventID >=32)? EventID-32:EventID;
      int IRQ = (EventID >= 32)? VIC2_BASE:VIC1_BASE;
      EnableIRQ(1<<Offset,IRQ);
    }
    return SUCCESS;
  }else {
    //bwprintf(COM2, "doAwaitEvent: Another Task is already waiting on this Event\n\r");
    return ANOTHER_WAITING;
  }
}

void UART1Event(int* Uart1Status, char* bTX, char* bRX ){
  int* Uart1IDClr = (int *)(UART1_BASE | UART_INTR_OFFSET);
  int* Uart1Flag = (int *)(UART1_BASE | UART_FLAG_OFFSET);
  int flag = *Uart1Flag;
  int intID = *Uart1IDClr;
  *bTX = (*Uart1Status & (1<<5 | 1<<3));
  *bRX = (*Uart1Status & (1<< 4));
  // turn off whoever is on
  //bwprintf(COM2,"UART1E: %d %d %d %d %d\n\r",((*Uart1Status)<<24)>>24,((*Uart1IDClr)<<28)>>28,flag&TXFE_MASK,flag&CTS_MASK,flag&RXFE_MASK);
  
  if(flag & TXFE_MASK) *Uart1Status &= ~(1<<5);
  if(flag & CTS_MASK) *Uart1Status &= ~(1<<3);
  if(flag & RXFF_MASK) *Uart1Status &= ~(1<<4);
  /*
  if(intID & TIS) *Uart1Status &= ~(1<<5);
  if(intID & MIS) {
    *Uart1Status &= ~(1<<3);
    *Uart1IDClr |= MIS;
    }*/
  if(intID & RIS) *Uart1Status &= ~(1<<4);
  //bwprintf(COM2,"UART1E: modified %d %d\n\r",((*Uart1Status)<<24)>>24,((*Uart1IDClr)<<28)>>28);
}

void ProcessEvent(KernelStruct* Colonel, int EventID) { //TODOTODO when use this
  EventAwaitTask* WET = Colonel->WaitTable;
  TD* NotifTask = (WET[EventID].AwaitTask);
  char bRealTX = 0;
  char bRealRX = 0;
  //if(EventID!=51) bwprintf(COM2,"PE: %d\n\r",EventID);
  //bwprintf(COM1,"PE: task:%d, EventID:%d \n\r",WET[EventID].AwaitTask->TaskID,EventID);
  // if its uart1 interrupt
  if(EventID == UART1_INT){
    int* Uart1Status = (int *)(UART1_BASE | UART_CTLR_OFFSET);
    UART1Event(Uart1Status,&bRealTX,&bRealRX);
    int status = *Uart1Status;
    // if uart1's cts and tie are disabled, wake uart1 out notifier 
    if((!(status & (1<<5 | 1<<3))) && bRealTX){
      //if(!(status &(1<<5))){
      //bwprintf(COM2,"PE: RealTX:%d\n\r",bRealTX);
      WET[EventID].AwaitTask = NULL;
      NotifTask->TaskState = Ready;
      pushToScheduler(Colonel,NotifTask);
      //bwprintf(COM2,"PE: push %d to scheduler\n\r",NotifTask->TaskID);
    }//else bwprintf(COM2,"PE: %d %d ReatTX %d\n\r",status &(1<<5), status&(1<<3),bRealTX);
    // if uart1's receive is disabled, wake uart1 in notifier
    if((!(status & (1<<4))) && bRealRX){
      //bwprintf(COM2,"PE: RealRX:%d\n\r",bRealRX);
      NotifTask = (TD*)(WET[EventID].Data);
      WET[EventID].Data = NULL;
      NotifTask->TaskState = Ready;
      pushToScheduler(Colonel,NotifTask);
      //bwprintf(COM2,"PE: push %d to scheduler\n\r",NotifTask->TaskID);
    }//else bwprintf(COM2,"PE: %d  RealRX %d\n\r",status &(1<<4),bRealRX);
    return ;
  }
  //bwprintf(COM2,"PE: IRQ2 %d\n\r",(*(int *)(VIC2_BASE | INT_ENABLE)));
  NotifTask->TaskState = Ready;
  pushToScheduler(Colonel, NotifTask);
  WET[EventID].AwaitTask = NULL;
}

void HandleEvents(KernelStruct* Colonel, int IRQ2,int IRQ1){
  int code1 = IRQ1;
  int code2 = IRQ2;
  int Mask = 1<<31;
  int base = 31;
  // process all event in IRQ2
  while(base > 0){
    if(Mask & code2){/*
      if(base+32 == UART2_INT){
	int Flags = *(int *)(UART2_BASE | UART_FLAG_OFFSET);
	int TrxMask = (int )(TXFE_MASK | CTS_MASK);
	if(!(Flags & TrxMask)) {
	  base --;
	  code2 <<= 1;
	  continue;
	}
	}*/
      ProcessEvent(Colonel,base+32);
    }
    base --;
    code2 <<= 1;
  }
  base = 31;
  while(base > 0){
    if(Mask & code1){
      ProcessEvent(Colonel, base);
    }
    base --;
    code1 <<=1;
  }
}
/*
int CheckItrEvent(int EventID) {
  int Base;
  
  if (!IsValidEvent(EventID)) {
    bwprintf(COM2, "CheckItrEvent: Invalid Event\n\r");
    return INVALID_EVENT;
  }
  
  Base = (EventID < 32) ? VIC1_BASE : VIC2_BASE; 
  
  return (*(int*) Base) & (1 << (EventID - 32));
}
*/
/*
 * For example,  for Off Hwi
 *              INT_ENABLE for On Hwi
 *
int SetOnItrEvent(int EventID, int Offset) {
    int Shift, Base;

    if (!IsValidEvent(EventID)) {
      bwprintf(COM2, "EnableItrEvent: Invalid Event\n\r");
      return INVALID_EVENT;
    }

    Base = (EventID < 32) ? VIC1_BASE : VIC2_BASE;
    Base += OnOrOff;  

    *(int*)Base |= (1 << (EventID - 32));

    return SUCCESS;
}

*/
