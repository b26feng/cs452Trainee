#include <bwio.h>
#include <buffer.h>
#include <canvas.h>
#include <clockserver-api.h>
#include <courier.h>
#include <error.h>
#include <io-api.h>
#include <interrupt-io.h>
#include <nameserver.h>
#include <shortlife.h>
#include <syscall.h>
#include <td-shared.h>
#include <tools.h>
#include <types.h>
#include <ts7200.h>
#include <train-controller.h>
#include <traintask.h>

  //Printf(COM2,"Watcher %d %d \n\r",Train,Sensor);


void TrainTask(){
  int ClkServer = WhoIs("ClockServer");
  int TSID = WhoIs("TrackServer");
  int TCID = WhoIs("TCServer");
  const int* Universal = GetShared();
  TCMsg TCSMsg,TCRMsg;
  //TSMsg TSSMSg,TSRMsg;
  int LastTime;
  TCSMsg.Type = TT_GETTRAIN;
  Send(TCID,(void *)&TCSMsg,sizeof(TCMsg),(void *)&TCRMsg,sizeof(TCMsg));

  Train* MyTrain = (Train*) TCRMsg.Addr;
  //
  MyTrain->bRegistered = 1;
  setDJerk(MyTrain);
  setDTDiff(MyTrain);
  setStopDist(MyTrain);
  MyTrain->Last = GetLastSensor(MyTrain->LocBase);
  MyTrain->Dist = GetNextSensorD(MyTrain->LocBase);
  MyTrain->NNext = GetNextSensor(MyTrain->Next,1);
  
  Printf(COM2,"%s%c[%d;%dH%d%s",SAVE_CURSOR,ESC,28+MyTrain->Index,3,MyTrain->TrainID,RESTORE_CURSOR);
  if(RegTrack(MyTrain)){
    Printf(COM2,"%s%c[%d;%dH%d Failed!%s",SAVE_CURSOR,ESC,28+MyTrain->Index,60,MyTrain->TrainID,RESTORE_CURSOR);
  }
  FOREVER{
    // location
    if(MyTrain->bChanged){
      Printf(COM2,"%s%c[%d;%dH%s + %d   %s",SAVE_CURSOR,ESC,28+MyTrain->Index,10,MyTrain->LocBase->name,MyTrain->LocOff,RESTORE_CURSOR);
      // next sensor
      if(MyTrain->Next == NULL){
	Printf(COM2,"%s%c[%d;%dHNULL%s",SAVE_CURSOR,ESC,28+MyTrain->Index,28,MyTrain->Next->name,RESTORE_CURSOR);
      }else{
	Printf(COM2,"%s%c[%d;%dH%s  %s",SAVE_CURSOR,ESC,28+MyTrain->Index,28,MyTrain->Next->name,RESTORE_CURSOR);
      }
      // Speed
      Printf(COM2,"%s%c[%d;%dH%d %s",SAVE_CURSOR,ESC,28+MyTrain->Index,35,MyTrain->Speed,RESTORE_CURSOR);
      // other dym vel
      Printf(COM2,"%s%c[%d;%dH%d %s",SAVE_CURSOR,ESC,28+MyTrain->Index,40,(int)MyTrain->DymVel,RESTORE_CURSOR);
      Printf(COM2,"%s%c[%d;%dH%d  %s",SAVE_CURSOR,ESC,28+MyTrain->Index,44,MyTrain->Dist-MyTrain->LocOff,RESTORE_CURSOR);
      /*  
	  Printf(COM2,"%s%c[%d;%dHLast:%s, Dist to next:%d   ,StopDist:%d    %s",SAVE_CURSOR,ESC,28+MyTrain->Index,80,GetLastSensor(MyTrain->LocBase)->name,GetNextSensorD(MyTrain->LocBase)-(MyTrain->LocOff),(int)MyTrain->Vel,RESTORE_CURSOR);
      */
      
      
      if(MyTrain->Last){
	Printf(COM2,"%s%c[%d;%dHLast:%s,StopDist:%d    %s",SAVE_CURSOR,ESC,28+MyTrain->Index,80,MyTrain->Last->name,(int)MyTrain->StopDist,RESTORE_CURSOR);
      }else{
	Printf(COM2,"%s%c[%d;%dHLast:%d,StopDist:%d    %s",SAVE_CURSOR,ESC,28+MyTrain->Index,80,0,(int)MyTrain->StopDist,RESTORE_CURSOR);
	
      }

      //clear out
      PRINTAT(28+MyTrain->Index,105,"                                    ");
      TrackNode* Temp = MyTrain->LocBase;
      int counter = 0;
      char Dir;
      while(Temp->TrainOwn == MyTrain->TrainID){
	PRINTAT(28+MyTrain->Index,105+(counter<<2),Temp->name);
	counter ++;
	if(Temp->type == NODE_EXIT) break;
	switch(Temp->type){
	case NODE_BRANCH:
	  Dir = Temp->State;
	  break;
	default:
	  Dir = DIR_AHEAD;
	  break;
	}
	Temp = (Temp->edge)[Dir].dest;
      }
      
      
      //Printf(COM2,"%s%c[%d;%dHDist to next %d%s",SAVE_CURSOR,ESC,38,80,,RESTORE_CURSOR);
      MyTrain->bChanged = 0;
      
    }
    LastTime = *Universal;
    Delay(ClkServer,10);
    
    if(MyTrain->Speed){
      MyTrain->LocOff += (int)((*Universal - LastTime) * MyTrain->DymVel);
      //MyTrain->bChanged = 1;
    }
  }
  /*
    FOREVER{
    Delay(10);
    }*/
  Exit();
}


int GetTrainIndex(char Num){
  switch(Num){
  case 24:
    return 0;
    break;
  case 58:
    return 1;
    break;
  case 74:
    return 2;
    break;
  case 78:
    return 3;
    break;
  case 79:
    return 4;
    break;
  default:
    return -1;
    break;
  }
}

void InitTrains(Train* Trains){
  
  Trains[4].TrainID = 79;
  Trains[4].Tick14 = 110.0;
  Trains[4].Tick10 = 200.0;
  Trains[4].Dist14 = 1260.0;
  Trains[4].Dist10 = 450.0;
  Trains[4].bRegistered = 0;
  Trains[4].bStartup = 1;
  //Trains[4].Last = NULL;
  Trains[4].LocBase = NULL;
  Trains[4].Next = NULL;
  Trains[4].Index = 4;
  Trains[4].Speed = 0;
  Trains[4].TimeStamp = 0;
  Trains[4].alpha = 0.25;
  Trains[4].bChanged = 0;
  
  Trains[3].TrainID = 78;
  Trains[3].Tick14 = 160.0;
  Trains[3].Tick10 = 280.0;
  Trains[3].Dist14 = 930.0;
  Trains[3].Dist10 = 300.0;
  Trains[3].SDMinTick = 80;
  Trains[3].SDMaxTick = 300;
  Trains[3].bRegistered = 0;
  Trains[3].bStartup = 1;
  //Trains[3].Last = NULL;
  Trains[3].LocBase = NULL;
  Trains[3].Next = NULL;
  Trains[3].Index = 3;
  Trains[3].Speed = 0;
  Trains[3].TimeStamp = 0;
  Trains[3].alpha = 0.25;
  Trains[3].bChanged = 0;
  
  Trains[0].TrainID = 24;
  Trains[0].Tick14 = 120.0;
  Trains[0].Tick10 = 210.0;
  Trains[0].Dist14 = 1260.0;
  Trains[0].Dist10 = 400.0;
  Trains[0].SDMinTick = 50;
  Trains[0].SDMaxTick = 300;
  Trains[0].bRegistered = 0;
  Trains[0].bStartup = 1;
  //Trains[0].Last = NULL;
  Trains[0].LocBase = NULL;
  Trains[0].Next = NULL;
  Trains[0].Index = 0;
  Trains[0].Speed = 0;
  Trains[0].TimeStamp = 0;
  Trains[0].alpha = 0.25;
  Trains[0].bChanged = 0;
  
  Trains[1].TrainID = 58;
  Trains[1].Dist14 = 1240.0;
  Trains[1].Dist10 = 460.0;
  Trains[1].bRegistered = 0;
  Trains[1].bStartup = 1;
  //Trains[1].Last = NULL;
  Trains[1].LocBase = NULL;
  Trains[1].Next = NULL;
  Trains[1].Index = 1;
  Trains[1].Speed = 0;
  Trains[1].TimeStamp = 0;
  Trains[1].alpha = 0.25;
  Trains[1].bChanged = 0;
  
  Trains[2].TrainID = 74;
  Trains[2].bRegistered = 0;
  Trains[2].bStartup = 1;
  //Trains[2].Last = NULL;
  Trains[2].LocBase = NULL;
  Trains[2].Next = NULL;
  Trains[2].Index = 2;
  Trains[2].Speed = 0;
  Trains[2].TimeStamp = 0;
  Trains[2].alpha = 0.25;
  Trains[2].bChanged = 0;

}


void TCServer() {
  int SenderID, Res,i,Courier,PRTC;
  char bReceived = 0;
  TCMsg RcvMsg, ReplyMsg;
  Message RcvSlot;
  RcvSlot.Addr = (void *)&RcvMsg;
  char Package[2];
  int TrainSpeeds[NUM_TRAINS];
  
  // SendQ
  Buffer** AllSendQ = GetAllSendQ();
  int QStorage[MAX_NUM_TD];
  Buffer SendQ;
  InitBuffer(&SendQ,(void *)QStorage,MAX_NUM_TD);
  int TID = MyTid();
  int Index = GetMemoryIndexINT(TID);
  AllSendQ[Index] = &SendQ;


  
  //Watchdog List
  Buffer WL;
  int WLStorage[MAX_NUM_TD];
  Res = InitBuffer(&WL,(void *)WLStorage,(MAX_NUM_TD));
  int PRList[5];
  int DinerTime[5];
  for(i=0;i<5;i++){
    PRList[i] = -1;
    DinerTime[i] = 0;
  }
  char TrainBack[] = {1,1,1,1,1};
  
  
  // Things for update sensors together
  char Encounter[] = {0,0,0,0,0};
  
  
  int TCTS[5];
  for(i=0;i<5;i++) TCTS[i] = -1;
  int TCTSReply[10];
  TCMsgType TCTSType;
  
  char TCServerName[] = "TCServer";
  RegisterAs(TCServerName);
  
  char ClkServerName[] = "ClockServer";
  int ClkServer = WhoIs(ClkServerName);

  // read systick from it
  const int *Universal = (int *)(GetShared());
  
  AP Args;
  
  int TargetTrain,SensorIDSpeed,TrainIndex,Offset,DogID,TimeHolder;
  int SaveTrain,SaveSensor;
  int WatcherID;
  TargetTrain = SensorIDSpeed = -1;
  //Printf(COM2,"TC:before loop\n\r");
  Train Trains[5];
  Train* Target;
  InitTrains(Trains);

  Args.arg0 = (void *)6;
  Args.arg1 = (void *)&TCTSCourier;
  Create(&Args);

  /*
  Args.arg0 = (void *)6;
  Args.arg1 = (void *)&TCPRCourier;
  Create(&Args);
  */
  
  /* // Watch Dog
  for(i = 0;i<5;i++){
  int DogList[5];
 
    Args.arg0 = (void *)7;
    Args.arg1 = (void *)&WatchDog;
    Create(&Args);
    }*/
	//Train EUEX = {58,3.4,0.45,0};// calibrated 
  FOREVER {
    //SenderID = 0;
    if(!bReceived){
      Res = BufferFIFO(&SendQ, (void*)(&SenderID));
      BufferPopHead(&SendQ);
      //Printf(COM2,"TC:SQ->%d R:%d\n\r",SenderID,Res);
    }
    if (Res == 0) {
      if (bReceived == 0) {
	Res = Receive(SenderID, (void*)&RcvSlot, sizeof(RcvMsg));
	      SenderID = RcvSlot.SenderID;
	      //Printf(COM2,"TC: Rcv %d\n\r",SenderID);
      }
      //Printf(COM2,"TC: Got %d\n\r",SenderID);
      bReceived = 0;
      TargetTrain = RcvMsg.Num;
      SensorIDSpeed = RcvMsg.TrainSpeed;
      Offset = RcvMsg.Offset;
      TrainIndex = GetTrainIndex(TargetTrain);
      //Printf(COM2,"TrainIndex: %d, TrainNum:%d\n\r",TrainIndex,TargetTrain);
      if(TrainIndex == -1){
	Target = NULL;
      }else{
	Target = &(Trains[TrainIndex]);
      }
      switch (RcvMsg.Type){
      case SET_SPEED:
	//Printf(COM2,"TC: SetSpeed\n\r");
	//ReplyMsg.Type = RESPONSE;
	if(Target && Target->bRegistered){
	  Target->bStartup = (SensorIDSpeed == 0);
	  Target->Speed = SensorIDSpeed;
	  Target->bChanged = 1;
	  Package[0] = (char)SensorIDSpeed;
	  Package[1] = (char)TargetTrain;
	  
	  PutStr(COM1, Package, 2);
	  // update velocity, update diner time
	  TimeHolder = *Universal;
	  UpdateTrainSpeed(Target,TimeHolder,&(DinerTime[TrainIndex]));
	  // send out dog
	  //Printf(COM2,"%s%c[%d;%dHDinerTime[%d]:%d,address %d,base %d %s",SAVE_CURSOR,ESC,43,80,TrainIndex,DinerTime[TrainIndex],&(DinerTime[TrainIndex]),DinerTime,RESTORE_CURSOR);
	  //Printf(COM2,"%s%c[%d;%dHUpdateTSp%d,%d,%d,%d,%d%s",SAVE_CURSOR,ESC,43,80,DinerTime[0],DinerTime[1],DinerTime[2],DinerTime[3],DinerTime[4],RESTORE_CURSOR);
	  
	  if(DinerTime[TrainIndex] > 0){
	    Res = BufferFIFO(&WL,(void *)&DogID);
	    BufferPopHead(&WL);
	    if(Res == 0){
	      ReplyMsg.Num = TrainIndex;
	      ReplyMsg.Offset = DinerTime[TrainIndex];
	      Trains[TrainIndex].MyDog = DogID;
	      //Printf(COM2,"%s%c[%d;%dHSetSpeed:ReplyMsg.Offset %d %s",SAVE_CURSOR,ESC,43,80,ReplyMsg.Offset,RESTORE_CURSOR);
	      //Printf(COM2,"%s%c[%d;%dHSetSpRply%d,%d,%d,%d,%d%s",SAVE_CURSOR,ESC,43,80,DinerTime[0],DinerTime[1],DinerTime[2],DinerTime[3],DinerTime[4],RESTORE_CURSOR);
	      Reply(DogID,(void *)&ReplyMsg,sizeof(TCMsg));
	      DinerTime[TrainIndex] = 0;
	    }else{
	      Args.arg0 = (void *) 5;
	      Args.arg1 = (void *) &TCSensorDog;
	      DogID = Create(&Args);
	    }
	  }
	  Res = 0;
	  Reply(SenderID, (void*)&Res, sizeof(int));
	}else{
	  Res = TRAIN_UNREG;
	  Reply(SenderID,(void*)&Res,sizeof(int));
	}
	
	//Printf(COM2,"TC: Set\n\r");
	break;
      case REVERSE:
	if(Target && Target->bRegistered){
	  Res = 0;
	  ReplyMsg.Type = RESPONSE;
	  Reply(SenderID,(void*)&Res,sizeof(int));
	  // int TxServer = WhoIs("TxServerU1");
	  Package[0] = (char)0;
	  Package[1] = (char)RcvMsg.Num;
	  PutStr(COM1, Package, 2);
	  Delay(ClkServer,400); // TODO is this enough ?
	  Package[0] = (char)15;
	  Package[1] = (char)RcvMsg.Num;
	  PutStr(COM1, Package, 2);
	  //Delay(ClkServer,400); // TODO is this enough ?
	  Package[0] = (char)Target->Speed;
	  Package[1] = (char)RcvMsg.Num;
	  PutStr(COM1, Package, 2);
	  TimeHolder = *Universal;
	  ReverseUpdate(Target,TimeHolder,&DinerTime[TargetTrain]);
	}else{
	  Res = TRAIN_UNREG;
	  Reply(SenderID,(void*)&Res,sizeof(int));
	}
	break;
      case REGTRAIN:
	if(Target){
	  //Res = 0;
	  //Target->bRegistered = 1;
	  TCTS[TrainIndex] = SensorIDSpeed;
	  Target->LocOff = Offset;
	  Res = (Courier == 0);
	  //Res = BufferFIFO(&WL,(void*)&Courier);
	  //BufferPopHead(&WL);
	  //Printf(COM2,"%s%c[%d;%dHWL Rs:%d%s",SAVE_CURSOR,ESC,39,80,Res,RESTORE_CURSOR);
	  // copy all the current sensor/nodes
	  if(!Res){
	    for(i = 0;i<5;i++) TCTSReply[i] = TCTS[i];
	    //Printf(COM2,"%s%c[%d;%dHReg:%d %d %d %d %d%s",SAVE_CURSOR,ESC,37,80,TCTS[0],TCTS[1],TCTS[2],TCTS[3],TCTS[4],RESTORE_CURSOR);
	    ReplyMsg.Addr = TCTSReply;
	    Reply(Courier,(void *)&ReplyMsg,sizeof(TCMsg));
	    Courier = 0;
	  }
	  Reply(SenderID,(void *)&Res,sizeof(int));
	}else{
	  Res = TRAIN_NEXIT;
	  Reply(SenderID,(void *)&Res,sizeof(int));
	}
	break;
      case STPAFT:
	if(Target && Target->bRegistered){
	  Res = 0;
	  Reply(SenderID, (void*)&Res, sizeof(int));
	  Args.arg0 = (void *) 10;
	  Args.arg1 = (void *)(&Watcher);
	  WatcherID = Create(&Args);
	  SaveTrain = TargetTrain;
	  SaveSensor = SensorIDSpeed;
	  Target->bChanged = 1;
	  Printf(COM2,"%s%c[%d;%dHWatcherID:%d on:%d, at:%d%s",SAVE_CURSOR,ESC,44,80,WatcherID,SensorIDSpeed,*Universal,RESTORE_CURSOR);
	}else{
	  Res = TRAIN_UNREG;
	  Reply(SenderID,(void *)&Res,sizeof(int));
	}
	break;
      case TCSTAMP:
	Res = 0;
	Reply(SenderID,(void*)&Res,sizeof(int));
	
	Args.arg0 = (void *) 10;
	Args.arg1 = (void *)(&Stamper);
	WatcherID = Create(&Args);
	Printf(COM2,"%s%c[%d;%dHStamperID:%d Index:%d%s",SAVE_CURSOR,ESC,43,80,WatcherID,GetMemoryIndexINT(WatcherID),RESTORE_CURSOR);
	SaveTrain = TargetTrain;
	SaveSensor = SensorIDSpeed;
	break;
	//used for all watcher types, stop after, time stamp and so on
      case WATCHER:
	if(TargetTrain >=0 && SensorIDSpeed >= 0){
	  ReplyMsg.Type = RESPONSE;
	  ReplyMsg.Num = SaveTrain;
	  ReplyMsg.TrainSpeed = SaveSensor;
	  ReplyMsg.SwitchDir = (TargetTrain == 24)? &Trains[0]:&Trains[3];
	  //Printf(COM2,"TC:%d\n\r",Trains[0].SDMinTick);
	  
	  Printf(COM2,"%s%c[%d;%dHTask:%d Train:%d Sensor:%d at %d %s",SAVE_CURSOR,ESC,43,80,SenderID,SaveTrain,SaveSensor,*Universal,RESTORE_CURSOR);
	  Reply(SenderID, (void*)&ReplyMsg, sizeof(TCMsg));
	  SaveTrain = -1;
	  SaveSensor = -1;
	}
	break;
      case TCSDTIME:
	if(Target && Target->bRegistered){
	  Res = 0;
	  Reply(SenderID,(void *)&Res,sizeof(int));
	  
	  Args.arg0 = (void *) 10;
	  Args.arg1 = (void *)(&ShortDistance);
	  WatcherID = Create(&Args);
	  //Printf(COM2,"%s%c[%d;%dHShortDistID:%d Index:%d%s",SAVE_CURSOR,ESC,43,80,WatcherID,GetMemoryIndexINT(WatcherID),RESTORE_CURSOR);
	}else{
	  Res = TRAIN_UNREG;
	  Reply(SenderID,(void *)&Res,sizeof(int));
	}	
	break;
      case TCTS_FIRST:
	Courier = SenderID;
	//FeedBuffer(&WL,(void *)SenderID);
	break;
      case TCTS_ORDER:
	//int* TSInfo = (int *)RcvMsg.Addr;
	// Copy the Node it self and Next Nodes 
	//for(i = 0;i<10;i++)TCTSReply[i] = TSInfo[i];
	//FeedBuffer(&WL,(void *) SenderID);
	Courier = SenderID;
	for(i = 0;i<5;i++){
	  Trains[i].bChanged = 1;
	  Trains[i].LocBase = (TrackNode*)(TCTSReply[i]);
	  Trains[i].Next = (TrackNode*)(TCTSReply[i+5]);
	  //Trains[i].NNext = GetNextSensor(Trains[i].Next,1);
	  if(Trains[i].LocBase != NULL){
	    if(Trains[i].bRegistered == 0){
	      Args.arg0 = (void *)7;
	      Args.arg1 = (void *)&TrainTask;
	      Create(&Args);
	      Printf(COM2,"%s%c[%d;%dHTCTS %d%s",SAVE_CURSOR,ESC,41,80,i,RESTORE_CURSOR);
	    }
	  }
	}
	break;
      case TCS_UPDATE:
	 Printf(COM2,"%s%c[%d;%dHTCS_UPDATE%s",SAVE_CURSOR,ESC,50,80,RESTORE_CURSOR);
	Res = UpdateTrain(Trains,(char *)RcvMsg.Addr,Offset,DinerTime);
	// if sensors updated, update next sensor  
	//UpdateTrain(&Trains[i],Offset);
	// update the time the watchdog get fed
	// inerTime[i] = (Offset+(int)(Trains[i].Dist/Trains[i].DymVel));
	for(i = 0;i<5;i++){
	  // Router
	  if(Trains[i].MyPR){
	    if(Trains[i].LocBase->num == PRList[i]){
	      Res = 0;
	      Reply(Trains[i].MyPR,(void *)Res,sizeof(int));
	      PRList[i] = -1;
	    }
	  }
	  // Dog
	  if(DinerTime[i]>0){
	    Res = BufferFIFO(&WL,(void *)&DogID);
	    BufferPopHead(&WL);
	    if(Res == 0){
	      ReplyMsg.Num = i;
	      ReplyMsg.Offset = DinerTime[i];
	      Reply(DogID,(void *)&DinerTime[i],sizeof(int));
	      DinerTime[i]  = 0;
	      Trains[i].MyDog = DogID;
	      Trains[i].bChanged = 1;
	    }else{
	      Args.arg0 = (void *) 5;
	      Args.arg1 = (void *) &TCSensorDog;
	      DogID = Create(&Args);
	    }
	  }
	}
	Reply(SenderID,(void *)&Res,sizeof(int));
	break;
      case TSTC_UPDATE:
	//PRINTAT(37,80,"BEFORE Update");
	UpdateNextOrNot(Trains,(TrackNode*)RcvMsg.Addr,DinerTime,Universal);
	for(i = 0;i<5;i++){
	  if(DinerTime[i] && Trains[i].bRegistered){
	    Res = BufferFIFO(&WL,(void *)DogID);
	    BufferPopHead(&WL);
	    if(Res){
	      Args.arg0 = 5;
	      Args.arg1 = (void *)&TCSensorDog;
	      Create(&Args);
	    }else{
	      ReplyMsg.Offset = DinerTime[i];
	      ReplyMsg.Num = i;
	      Trains[i].MyDog = DogID;
	      Reply(DogID,(void *)&ReplyMsg,sizeof(TCMsg));
	    }
	  }
	}
	//PRINTAT(38,80,"AFTER update");
	Res = 0;
	Reply(SenderID,(void *)&Res,sizeof(int));
	break;
      case DOGINIT:
	for(i = 0;i<5;i++){
	  if(DinerTime[i]>0){
	    ReplyMsg.Num = i;
	    ReplyMsg.Offset = DinerTime[i];
	    Reply(SenderID,(void *)&ReplyMsg,sizeof(TCMsg));
	    Trains[i].MyDog = SenderID;
	    Trains[i].bChanged = 1;
	    DinerTime[i] = 0;
	    break;
	  }
	}
	break;
      case DOGBACK:
	if(Trains[TargetTrain].MyDog == SenderID){
	  Trains[TargetTrain].bTimedOut = 1;
	  Trains[TargetTrain].bChanged = 1;
	  Printf(COM2,"%s%c[%d;%dHTrain:%d TimedOut! Time:%d Sensor: %s%s",SAVE_CURSOR,ESC,33,80,Trains[TargetTrain].TrainID,*Universal,(Trains[TargetTrain].Next)->name,RESTORE_CURSOR);
	}
	FeedBuffer(&WL,(void *)SenderID);
	break;
      case TT_GETTRAIN:
	for(i = 0;i<5;i++){
	  if(Trains[i].LocBase != NULL && !(Trains[i].bRegistered)){
	    ReplyMsg.Addr = (void *)&Trains[i];
	    Reply(SenderID,(void *)&ReplyMsg,sizeof(TCMsg));
	    break;
	  }
	}
	break;
      case GET_PF_ORDER:
	PRTC = SenderID;
	break;
      case GOTO:
	//Printf(COM2,"%s%c[%d;%dHGOTO:%d %d %s",SAVE_CURSOR,ESC,52,80,TargetTrain,TrainIndex,RESTORE_CURSOR);
	if(Target && Target->bRegistered){
	  ReplyMsg.Addr = (void *)Target;
	  ReplyMsg.Num = SensorIDSpeed;
	  Reply(PRTC,(void *)&ReplyMsg,sizeof(TCMsg));
	  Res = 0;
	  Reply(SenderID,(void *)&Res,sizeof(int));
	}else{
	  Res = TRAIN_UNREG;
	  Reply(SenderID,(void *)&Res,sizeof(int));
	}
	break;
      case PR_SR:
	PRList[TrainIndex] = Offset;
	break;
      case TEMP: //TODO
	break;
      default:
	break;
      }
    }
    else {
      bReceived = 1;
      //Printf(COM2, "TC->Null\n\r");
      Receive(-1, (void*)&RcvSlot, sizeof(RcvMsg));
      SenderID = RcvSlot.SenderID;
      Res = 0;
      //Printf(COM2,"TC: Null->%d\n\r",SenderID); 
    }
  }
  //Printf(COM2,"TC: Out of loop\n\r");
}


int TrainSetSpeed(int TCServer, int TrainNum, int Speed) {
  TCMsg SendMsg;
  int Result;
  SendMsg.Type = SET_SPEED;
  SendMsg.TrainSpeed = Speed;
  SendMsg.Num = TrainNum;
  Send(TCServer, (void*)&SendMsg, sizeof(SendMsg), (void*)&Result, sizeof(int));/*
  if(!Result){
    Printf(COM2,"%s%c[%d;%dH%d%d%s",SAVE_CURSOR,ESC,28,50,Speed/10,Speed%10,RESTORE_CURSOR);
    }*/
  return Result;
}

int TrainRegister(int TCServer, int TrainNum,int Node,int Offset){
  TCMsg SendMsg;
  int Result = -1;
  SendMsg.Type = REGTRAIN;
  SendMsg.TrainSpeed = Node;
  SendMsg.Offset = Offset;
  SendMsg.Num = TrainNum;
  //Printf(COM2,"TrainNum: %d\n\r",SendMsg.Num);
  Send(TCServer,(void *)&SendMsg,sizeof(SendMsg),(void*)&Result,sizeof(int));
  return Result;
}

int TrainReverse(int TCServer, int TrainNum) {
  TCMsg SendMsg;
  int Result;
  
  SendMsg.Type = REVERSE;
  SendMsg.Num = TrainNum;
  Send(TCServer, (void*)&SendMsg, sizeof(SendMsg), (void*)&Result, sizeof(int));
  return Result;
}

int GoToSensor(int TCServer,int TrainID,int SensorID){
  //Printf(COM2,"%s%c[%d;%dHGT:%d %d %s",SAVE_CURSOR,ESC,51,80,TrainID,SensorID,RESTORE_CURSOR);
  TCMsg SendMsg;
  int Result;
  SendMsg.Type = GOTO;
  SendMsg.Num =(char) TrainID;
  SendMsg.TrainSpeed = SensorID;

  Send(TCServer,(void*)&SendMsg,sizeof(TCMsg),(void*)&Result,sizeof(int));
  return Result;
}
