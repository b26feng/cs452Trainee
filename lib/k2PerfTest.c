#include <bwio.h>
#include <types.h>
#include <k2PerfTest.h>
#include <timer.h>
#include <syscall.h>
int Sender() {
  int MyID = MyTid();
  bwprintf(COM2,"Sender: %d runs\n\r",MyID);
  int ReceiverTID, i, StartTime, EndTime, Res; 
  int Sum = 0, Average;
  char* SendMsg = "water water water loo loo loo, water water water loo loo loo, water water water loo loo loo cs452 cs452";
  ReceiverTID = 1;
  char ReplyMsg[32]; // 4 bytes
  //char ReplyMsg[256]; // 64 bytes
  //Clock* c;
  //ClockInit(c);
  //ReceiverTID = WhoIs("Rcvr");
  for (i = 0; i < 100; ++i) {
    //ClockUpdate(c);
    //StartTime = c->ClockCount;
    Res = Send(ReceiverTID, (char*)SendMsg, 32, ReplyMsg, 32);
    if (Res != 32) {
      bwprintf(COM2, "Send has Errors\n\r");
      return Res;
    }
    //ClockUpdate(c);
    //EndTime = c->ClockCount;
    //Sum += (EndTime - StartTime);
  }
  //Average = Sum/100; 
  bwprintf(COM2, "Send Time: \n\r", Average);
    Exit();
}
int Receiver() {
  int MyID = MyTid();
  bwprintf(COM2,"Receiver:%d runs\n\r", MyID);
  int SenderTID, i, StartTime, EndTime, Res;
  int Sum = 0, Average;
  char ReceiveMsg[32];      
  char ReplyMsg[32]; // 4 bytes
  Clock* c;
  SenderTID = 2;
  bwprintf(COM2,"Receiver: Guess1\n\r");
  ClockInit(c);
  // Res = RegisterAS("Rcvr");
  //Reply(SenderTID, ReplyMsg, sizeof(ReplyMsg));
  StartTime = 0;
  bwprintf(COM2,"Receiver: Before Loop\n\r");
    for (i = 0; i < 100; i++) {
      ClockUpdate(c);
      Res = Receive(SenderTID, (char*)ReceiveMsg, 32);
      if (Res < 0) {
	bwprintf(COM2, "Receive has Errors.\n\r");
	return Res;
      }
      ClockUpdate(c);
      EndTime = c->ClockCount;
      Reply(SenderTID, (char*)ReplyMsg, Res);
      Sum += (EndTime-StartTime);
    }
    Average = Sum / 100;
    bwprintf(COM2, "Receive Time: %d\n\r", Average);
    Exit();
}

int PerfTest() {
  int SenderTID, ReceiverTID, NSTID, SendFirst = 1;
  AP Args1, Args5;
  char* SendMsg, ReplyMsg;
  Args5.arg0 = (void *) 5;
  Args5.arg1 = (void *) (&Receiver);
  //Args1.arg0 = (void *) 1;
  //Args1.arg1 = (void *) (&Receiver);
  Args3.arg0 = (void *) 3;
  Args3.arg1 = (void*) (&Sender);
  //NSTID = Create(&Args5);
  ReceiverTID = Create(&Args5);
  //Args5.arg1 = (void*)(&Sender);
  SenderTID = Create(&Args1);
  bwprintf(COM2,"PerfTest exiting...\n\r");
  //Send(ReceiverTID, (void*)SendMsg, sizeof(SendMsg), (void*)Reply, sizeof(ReplyMsg));
  Exit();
}
