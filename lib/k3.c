#include <buffer.h>
#include <bwio.h>
#include <clock.h>
#include <idle-task.h>
#include <k3.h>
#include <nameserver.h>
#include <notifier.h>
#include <syscall.h>
#include <types.h>

void InitNameServer() {
	AP Args;
	Args.arg0 = (void *) 13;
	Args.arg1 = (void *)(&NameServer);
	Args.arg2 = 0;
	Args.arg3 = 0;
	
	int NameServerID = Create(&Args);
	bwprintf(COM2,"NS created %d\n\r", NameServerID);
	Args.arg0 = (void *)(NameServerID);
	//RecordNameServer(&Args);
	bwprintf(COM2,"FUT: name server created, before pass\n\r");
	Pass();
}


void FirstUserTask() {
  AP Args;
  int ClockServerID, ClockClient1ID, ClockClient2ID, ClockClient3ID, ClockClient4ID, IdleTaskID;
  int i, Res;
  ClkMsg ReceiveMsg, ReplyMsg;
  
  
  bwprintf(COM2, "Starting the First User Task\n\r");
  
  InitNameServer();
  
  // Idle Task
  Args.arg0 = (void*) 15;
  Args.arg1 = (void*) (&IdleTask);
  IdleTaskID = Create(&Args);
  //bwprintf(COM2,"FUT: idle task created %d\n\r", IdleTaskID);

  //notifier
  Args.arg0 = (void *) 0;
  Args.arg1 = (void *) (&ClockNotifier);
  int NotifierID = Create(&Args);
  //bwprintf(COM2,"FUT: Notifier created %d\n\r", NotifierID);
  
  // ClockServer
  Args.arg0 = (void*) 1;
  Args.arg1 = (void*) (&ClockServer);
  ClockServerID = Create(&Args);
  //bwprintf(COM2,"FUT: clock server created %d\n\r", ClockServerID);

  /* What's going on before this pass(ignoring idle task):
   * all priority shown in ()
   * NS(13) blocked, Notifier(0) ready, ClockServer(1) ready, FUT(1) active
   * What happens after this pass
   * FUT(14) ready, Notifier(0) active, ClockServer(1) ready, NS(13) blocked
   * Notifier(0) registered, blocked, NS(13) Ready, 
   * Clockserver(1) runs, registers, blocked
   * NS(13) runs, register Notifier(0), ClockServer(1), reply to them
   * NS(13) Active, Notifer(0) Ready, ClockServer(1) Ready, FUT(14) Ready
   * NS(13) received from random one, blocked
   * Notifier(0) runs, WhoIs(clockserver), blocked, NS(13) Ready
   * ClockServer(1)runs, init clock, receiver from random one, blocked <1>
   * NS(13) runs, reply to Notifier(0), Notifier->Ready, NS receive, NS Block
   * Notifier(0) awaits, Notifier(0) blocked
   * FUT(14) runs<2>
   * everything between <1> and <2> finishes in 10ms
   */
   
  Pass();
  
  // ClockClient1
  Args.arg0 = (void*) 3;
  Args.arg1 = (void*) (&ClockClient1);
  ClockClient1ID = Create(&Args);
  //bwprintf(COM2,"FUT: client 1 created %d\n\r", ClockClient1ID);
  
  // ClockClient2
  Args.arg0 = (void*) 4;
  Args.arg1 = (void*) (&ClockClient2);
  ClockClient2ID = Create(&Args);
  //bwprintf(COM2,"FUT: client 2 created %d\n\r", ClockClient2ID);
  
  // ClockClient3
  Args.arg0 = (void*) 5;
  Args.arg1 = (void*) (&ClockClient3);
  ClockClient3ID = Create(&Args);
  //bwprintf(COM2,"FUT: client 3 created %d\n\r", ClockClient3ID);
  
  // ClockClient4
  Args.arg0 = (void*) 6;
  Args.arg1 = (void*) (&ClockClient4);
  ClockClient4ID = Create(&Args);
  //bwprintf(COM2,"FUT: client 4 created %d\n\r", ClockClient4ID);
  
  // Set up SendQ
  Buffer SendQ;
  int QStorage[4];
  SendQ.Storage = (void *)QStorage;
  SendQ.Size = 4;
  SendQ.Length = 0;
  SendQ.Head = 0;
  SendQ.Tail = 0;
  Buffer** AllSendQ = GetAllSendQ();
  AllSendQ[0] = &SendQ;
  
  
  int ToReceive;
  ReplyMsg.Type = DELAY_INFO;
  int ReplyCount = 4;
  FOREVER{
    ToReceive = NULL;
    BufferFIFO(&SendQ,(void *)&ToReceive);
    BufferPopHead(&SendQ);
    //bwprintf(COM2,"FUT: picked %d\n\r",ToReceive);
    if(ToReceive != NULL){
      Res = Receive(ToReceive, (void*)(&ReceiveMsg), sizeof(ReceiveMsg));
      bwprintf(COM2,"FUT: after received from %d\n\r",ToReceive);
      if (Res != sizeof(ReceiveMsg)) {
	bwprintf(COM2, "FirstUserTask: Receive from %d failed.\n\r",ToReceive);
      }
      if(ToReceive == ClockClient1ID){
	ReplyMsg.Tick = 10;
	ReplyMsg.NumDelays = 20;
      }else if(ToReceive == ClockClient2ID){	      
	ReplyMsg.Tick = 23;
	ReplyMsg.NumDelays = 9;
      }else if(ToReceive == ClockClient3ID){
	ReplyMsg.Tick = 33;
	ReplyMsg.NumDelays = 6;
      }else if(ToReceive == ClockClient4ID){
	ReplyMsg.Tick = 71;
	ReplyMsg.NumDelays = 3;
      }
      Res = Reply(ToReceive, (void*)(&ReplyMsg), sizeof(ReplyMsg));
      if (Res != SUCCESS) {
	bwprintf(COM2, "FirstUserTask: Reply to %d failed.\n\r",ToReceive);
      }
      ReplyCount --;
      if(ReplyCount == 0) break;
    }else{
	bwprintf(COM2,"FUT: Pass\n\r");
      Pass();
    }
  }
  bwprintf(COM2,"FUT: exiting()\n\r");
  Exit();
}
