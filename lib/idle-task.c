#include <bwio.h>
#include <canvas.h>
#include <idle-task.h>
#include <io-api.h>
#include <nameserver.h>
#include <ts7200.h>
#include <types.h>
void IdleTask() {
  int ClockServerID = WhoIs("ClockServer");
  const int *Universal = (int *)(GetShared());
  //unsigned int initTick = *Universal;
  unsigned int LastTick = *Universal;
  unsigned int CurrentTick = LastTick;
  unsigned int Accum = 0;
  unsigned mid;
  unsigned int Ten;
  unsigned int Cen;
  //float portion = 0.0;
  //bwprintf(COM2,"IdleTask: before into loop\n\r");
  while(1){
    //CurrentTick = Time(ClockServerID);
    CurrentTick = *Universal;
    if(LastTick != CurrentTick){
      Accum++;
      LastTick = CurrentTick;
      // Accum * 100 = Accum*128 - Accum*32 + Accum*4
      Ten = ((Accum << 7) - (Accum << 5) + (Accum << 2)) / CurrentTick;
      mid = ((Accum << 7) - (Accum << 5) + (Accum << 2)) % CurrentTick;
      Cen = ((mid << 7) - (mid<<5) + (mid << 2) ) / CurrentTick;
      //Printf(COM2,"%s",SAVE_CURSOR);
    //CURSOR(9,55);
    //Printf(COM2,"%d.%d",Ten,Cen);
    //Printf(COM2,"%s",RESTORE_CURSOR);
    //Pass();
      Printf(COM2,"%s%c[%d;%dH%d.%d%s",SAVE_CURSOR,ESC,16,55,Ten,Cen,RESTORE_CURSOR);
    }
    //Printf(COM2,"%c7%c[%d;%dH%d.%d%c8",ESC,ESC,9,55,Ten,Cen,ESC);
    //bwprintf(COM2,"IdleTask: idleTask:Total %d:%d\n\r", Accum, CurrentTick);
    //bwprintf(COM2,"IdleTask: IRQ: %d\n\r",*(int *)(VIC2_BASE));
  }
}

void IdleTask1(){
  int *int_contr = (int *)(UART2_BASE | UART_CTLR_OFFSET);
  int i;
  char bPrinted = 0;
  while(1){
    //i = int_contr;
    //if(bPrinted){
      //bwprintf(COM1,"IDT1: fires interrupt\n\r");
      //*(int *)(VIC1_BASE | SFT_INT) = (1<<26);
      //bPrinted = 0;
    //}else
    if(!bPrinted){
      //bwprintf(COM2,"IDT1: IntIDIntCLr %d\n\r",(*(int *)(UART1_BASE | UART_INTR_OFFSET)<<28) >> 28);
      //bwprintf(COM2,"IDT1: IRQ2 :%d\n\r",(*(int *)(VIC2_BASE | INT_ENABLE)));
      bPrinted = 1;
    }else{
      bPrinted = 0;
      //bwprintf(COM2,"IDT1: sft interrupt \n\r");
      //*(int *)(VIC2_BASE | SFT_INT) = 1<<20;
    }
    //if(!bPrinted){
      //bwprintf(COM1,"IDT1: runs\n\r");
      //bPrinted = 1;
    //}
    //if(i & (1<<4)) break;
    //bwprintf(COM2,"IdleTask1: runs\n\r");
  }
}
