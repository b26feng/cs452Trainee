#include <types.h>
#include <canvas.h>
#include <display.h>
#include <syscall.h>
#include <ts7200.h>
#include <io-api.h>
#include <train-controller.h>
#include <switches.h>
// #include <idle-task.h>
#include <clockserver-api.h>
#include <interrupt-io.h>
#include <nameserver.h>
#include <buffer.h>

int IsAlphabet(char Ch) {
	return (( 65 <= Ch && Ch <= 90) || (97<= Ch && Ch <= 122));
}
int IsSpace(char Ch) {
	return (Ch == ' ' || Ch == '\t');
}
int IsNumber(char Ch) {
	return (Ch >= '0' && Ch <= '9');
}
int ParseDir(Buffer* InputBuf, int* Res) {
	char Ch = 'U';
	char Ch1;
	*Res = SUCCESS;
	if (InputBuf->Length == 0) {
		*Res = FAILURE;
		return 0;
	}
	BufferFIFOByte(InputBuf, (void*)&Ch);
	while (IsSpace(Ch)) {
		BufferPopHead(InputBuf);
		if (InputBuf->Length > 0) BufferFIFOByte(InputBuf, &Ch);
		else {
			*Res = FAILURE;
			return FAILURE;
		}
	}
	// Putc(COM2, '1');
	if (Ch== 'S' || Ch =='s') {
		// Putc(COM2, '2');
		BufferPopHead(InputBuf);
		if (InputBuf->Length == 0) return STRAIGHT;
		BufferFIFOByte(InputBuf, &Ch1);
		if (!IsSpace(Ch1)) *Res = FAILURE;
	} 
	else if (Ch== 'C'||Ch=='c') {
		// Putc(COM2, '3');
		BufferPopHead(InputBuf);
		if (InputBuf->Length == 0) return CURVED;
		BufferFIFOByte(InputBuf, &Ch1);
		if (!IsSpace(Ch1)) *Res = FAILURE;
	} 
	*Res = FAILURE;
	return FAILURE;
}

int ParseArgs(Buffer* InputBuf, int* Res) {
	int Num = 0;
	char Ch;
	if (InputBuf->Length == 0) {
		*Res = FAILURE;
		return 0;
	}
	BufferFIFOByte(InputBuf, &Ch);
	while (IsSpace(Ch)) {
		BufferPopHead(InputBuf);
		if (InputBuf->Length > 0) BufferFIFOByte(InputBuf, &Ch);
	} 
	if (InputBuf->Length == 0 || (!IsNumber(Ch))) { 
		*Res = FAILURE;
		return FAILURE;
	}
	while(IsNumber(Ch)) {
		Num *= 10;
		Num += (int)(Ch - '0');
		BufferPopHead(InputBuf);
		if (InputBuf->Length == 0) break;
		BufferFIFOByte(InputBuf, &Ch);
	}
	if (InputBuf->Length != 0 && (!IsSpace(Ch))) *Res = FAILURE;
	return Num;
}

int ParseSensor(Buffer* InputBuf, int* Res) {
	char Ch = 'U';
	char Ch1,bEven,bOdd;
	int Num = 0, Num1 = 0;
	*Res = SUCCESS;
	bEven = bOdd = 0;
	if (InputBuf->Length == 0) {
		*Res = FAILURE;
		return 0;
	}
	BufferFIFOByte(InputBuf, (void*)&Ch);
	while (IsSpace(Ch)) {
		BufferPopHead(InputBuf);
		if (InputBuf->Length > 0) BufferFIFOByte(InputBuf, &Ch);
		else {
			*Res = FAILURE;
			return FAILURE;
		}
	}
	if (Ch <= 'E' && Ch >= 'A')  {
		BufferPopHead(InputBuf);
		Num += ((Ch -'A')<<4);// *16
	}
	else if (Ch >= 'a' && Ch <= 'e' ) {
		BufferPopHead(InputBuf);
		Num += ((Ch -'a')<<4);//*16
	}else if(Ch == 'n' || Ch=='N'){
		bOdd = 1;
		Num += 123;
	}else if(Ch == 'x' || Ch == 'X'){
		bEven = 1;
		Num += 123;
	}else return WrongInput;

	if (InputBuf->Length == 0) {
		*Res = FAILURE;
		return FAILURE; 
	}
	BufferFIFOByte(InputBuf, &Ch1);
	while (IsNumber(Ch1)) {
		BufferPopHead(InputBuf);
		Num1 *= 10;
		Num1 += (Ch1 - '0');
		if (Num1 > 16) {
			*Res = FAILURE;
			return FAILURE;
		}
		if (InputBuf->Length == 0) return (Num+Num1-1); 
		BufferFIFOByte(InputBuf, &Ch1);
	}
	if (IsSpace(Ch1)){
		if(bEven){
			Num+(Num1<<2);
		}else if(bOdd){
			Num+(Num1<<2)-1;
		}else{
			return (Num+Num1-1);
		}
	}
	*Res = FAILURE;
	return FAILURE;
}

int ParseTrainCmd(Buffer* InputBuf, int* Num, int* SpeedDirSensor, int* Extra) {
  char Ch0, Ch1, Ch2, track;
  int Res;
  TrainCmdType Result;
  if (InputBuf->Length <= 2) {
    // Printf(COM2,"%d\n\r",InputBuf->Length);
    BufferFIFOByte(InputBuf, &Ch0);
    if (Ch0 == 'q' || Ch0 == 'a' || Ch0 == '`') {
      BufferPopHead(InputBuf);
      if (InputBuf->Length == 0) return QuitTrain;
      BufferFIFOByte(InputBuf, &Ch1);
      BufferPopHead(InputBuf);
      if (IsSpace(Ch1)) {
	if (Ch0 == '`') return OnTrain;
	return QuitTrain;
      } 
    }
    return WrongInput;
  }
  // Putc(COM2,'z');
  BufferFIFOByte(InputBuf, &Ch0);
  // Putc(COM2, Ch0);
  BufferPopHead(InputBuf);
  BufferFIFOByte(InputBuf, &Ch1);
  BufferPopHead(InputBuf);
  BufferFIFOByte(InputBuf, &Ch2);
  BufferPopHead(InputBuf);
  // Putc(COM2, Ch0);
  switch(Ch0){
  case 't':
    switch(Ch1){
    case 'r':  //&& Ch2 == ' ') {
      // Printf(COM2,"in here\n\r");
      *Num = ParseArgs(InputBuf, &Res);
      if (Res == FAILURE) return WrongInput;
      *SpeedDirSensor = ParseArgs(InputBuf, &Res);
      if (Res == FAILURE) return WrongInput;
      Result = SetSpeed;
      break;
    case 'k':
      BufferFIFOByte(InputBuf, &track);
      Result = (track == 'a' || track =='A')? SetA:SetB;
      break;
    }
    break;
  case 'r':
    *Num = ParseArgs(InputBuf, &Res);
    if (Res == FAILURE) return WrongInput;
    switch(Ch1){
    case 'v':
      Result = Reverse;
      break;
    case 'g':
      *SpeedDirSensor = ParseSensor(InputBuf, &Res);
      if (Res == FAILURE) return WrongInput; 
      *Extra = ParseArgs(InputBuf, &Res);
      if (Res == FAILURE) return WrongInput;
      Result = RegTrain;
      break;
    }
    break;
  case 's':
    *Num = ParseArgs(InputBuf, &Res);
    if (Res == FAILURE) return WrongInput;
    switch(Ch1){
    case 'w':
      *SpeedDirSensor = ParseDir(InputBuf, &Res);
      if (Res == FAILURE) return WrongInput; 
      Result = SetSwitch;
      break;
    case 'a':
      *SpeedDirSensor = ParseSensor(InputBuf, &Res);
      if (Res == FAILURE) return WrongInput;
      Result = StopAfter;
      break;
    case 'd':
      *SpeedDirSensor = ParseArgs(InputBuf, &Res);
      if (Res == FAILURE) return WrongInput; 
      Result = ShortDist;
      break;
    case 'b':
      *SpeedDirSensor = ParseSensor(InputBuf, &Res);
      if (Res == FAILURE) return WrongInput; 
      *Extra = ParseArgs(InputBuf, &Res);
      if (Res == FAILURE) return WrongInput; 
      Result = StopBefore;
      break;
    }
    break;
  case 'v':
    //Ch1 == 'l' && Ch2 == ' ') {
    *Num = ParseArgs(InputBuf, &Res);
    if (Res == FAILURE) return WrongInput;
    *SpeedDirSensor = ParseSensor(InputBuf, &Res);
    if (Res == FAILURE) return WrongInput; 
    *Extra = ParseSensor(InputBuf, &Res);
    if (Res == FAILURE) return WrongInput; 
    Result = Velocity;
    break;
  case 'd':
    //Ch1 == 'c' && Ch2 == ' ') {
    *Num = ParseArgs(InputBuf, &Res);
    if (Res == FAILURE) return WrongInput;
    Result = DynamicCal;
    break;
  case 'g':
    *Num = ParseArgs(InputBuf, &Res);
    if (Res == FAILURE) return WrongInput;
    *SpeedDirSensor = ParseSensor(InputBuf, &Res);
    if (Res == FAILURE) return WrongInput; 
    Result = Goto;
    break;
  default:
    Result = WrongInput;
    break;
  }
  return Result;
}

int TrainStopAfter(int TCServer, int Train,int Sensor){
  TCMsg SendMsg;
  int ReplyMsg;
  SendMsg.Type = STPAFT;
  SendMsg.TrainSpeed = Sensor;
  SendMsg.Num = Train;
  
  Printf(COM2, "%s%c[%d;%dH%d %d %s",SAVE_CURSOR,ESC,55,80,Sensor,SendMsg.TrainSpeed,RESTORE_CURSOR);
  Send(TCServer,(void *)&SendMsg,sizeof(SendMsg),(void *)&ReplyMsg,sizeof(ReplyMsg));
  return SUCCESS;
}


int SensorStamp(TCServer,Num,SpeedDirSensor){
  TCMsg SendMsg,ReplyMsg;
  SendMsg.Type = TCSTAMP;
  SendMsg.Num = Num;
  SendMsg.TrainSpeed = SpeedDirSensor;
  Send(TCServer,(void *)&SendMsg,sizeof(TCMsg),(void *)&ReplyMsg,sizeof(TCMsg));
  return SUCCESS;
}


void ShortByTick(TCServer,Num,SpeedDirSensor){
  TCMsg SendMsg,ReplyMsg;
  SendMsg.Type = TCSDTIME;
  SendMsg.Num = Num;
  SendMsg.TrainSpeed = SpeedDirSensor;
  Send(TCServer,(void *)&SendMsg,sizeof(TCMsg),(void *)&ReplyMsg,sizeof(TCMsg));
}

void Display() {
  char Ch;
  char InputBuffer[CMD_SIZE];
  int Num, SpeedDirSensor, TrCmdType,Result, Extra, Index = 0;
  int TCID, ClkServer, TSServer;
  Buffer InputBuf;
  TSMsg TSSendMsg;
  TCMsg TCSendMsg;
  InitBuffer(&InputBuf,(void*)InputBuffer, CMD_SIZE);
  // InitSwitches();
  //Printf(COM2, "%s%s", CLEAR_SCREEN, UP_LEFT);
  //CLEARALL;
  //CURSOR(1,1);
  char ClkServerName[] = "ClockServer";
  ClkServer = WhoIs(ClkServerName);
  
  
  TSServer = WhoIs("TrackServer");
  //Printf(COM2,"DP: Trackserver %d\n\r",TSServer);
  
  char TCName[] = "TCServer";
  TCID = WhoIs(TCName);
  InitLayout();
  Delay(ClkServer,5);
  
  char bSetTrack = 0;
  char bSwitchSet = 0;
  FOREVER {
    if(bSetTrack && !bSwitchSet){
      InitSwitches(ClkServer,TSServer);
      //InitSwitches(ClkServer);
      bSwitchSet = 1;
    }	
    Ch = (char)Getc(COM2);
    switch(Ch) {
    case '\n':
    case '\r':
      // Putc(COM2,'x');
      // InputBuffer[Index] = '\0';
      // FeedBufferByte(&InputBuf, (void*)'\0');
      // Printf(COM2,"Length %d ",InputBuf.Length);
      TrCmdType = ParseTrainCmd(&InputBuf, &Num, &SpeedDirSensor, &Extra);
      // Printf(COM2,"Typee%d",TrCmdType);
      switch(TrCmdType) {
      case SetSpeed:
	if(TrainSetSpeed(TCID, Num, SpeedDirSensor)){
	  PRINTAT(40,80,"Train Not Registered");
	}
	break;
      case Reverse:
	if(TrainReverse(TCID, Num)){
	  PRINTAT(40,80,"Train Not Registered");
	}
	break;
      case RegTrain:
	
	Printf(COM2, "%s%c[%d;%dH%d %d %s",SAVE_CURSOR,ESC,41,80, Num, SpeedDirSensor,RESTORE_CURSOR);
	//Printf(COM2,"RegTrain:TC%d\n\r",TCID);
	if(bSetTrack){
	  //Printf(COM2,"%s%c[%d;%dHT:%d L:%d O:%d%s",SAVE_CURSOR,ESC,34,80,Num,SpeedDirSensor,Extra,RESTORE_CURSOR);
	  if(TrainRegister(TCID,Num,SpeedDirSensor,Extra)){
	    PRINTAT(21,80,"Train Out of Index!");
	  }/*else{
	     PRINTAT(29,80,"Train Registered!!");
	     }*/
	}else{
	  PRINTAT(40,80,"Please specify a track!");
	}
	break;
      case SetSwitch:
	if(bSetTrack){
	  TrainSetSwitch(ClkServer,TSServer, Num, SpeedDirSensor);
	  //TrainSetSwitch(ClkServer, Num, SpeedDirSensor);
	}else{
	  PRINTAT(40,80,"Please specify a track!");
	}
	break;
      case QuitTrain:
	// Printf(COM2,"System exiting.\n\r"); 
	//Putc(COM1,'a');
	//QuitProgram();
	*(int *)(UART1_BASE) = 97;
	QuitProgram();
	// Exit();
	break;
      case OnTrain:
	*(int *)(UART1_BASE) = 96;
	Printf(COM2,"\n\r Track On.\n\r"); 
	break;
      case WrongInput:
	PRINTAT(40,80,"Wrong input.      ");
	break;
      case StopAfter:
	Printf(COM2, "%s%c[%d;%dH%d %d %s",SAVE_CURSOR,ESC,40,80, Num, SpeedDirSensor,RESTORE_CURSOR);
	TrainStopAfter(TCID,Num,SpeedDirSensor);
	//		Printf(COM2, "\n\rStopAfter Num Sensor\n\r");
	
	break;
      case Velocity:
	SensorStamp(TCID,SpeedDirSensor,Extra);
	//		Printf(COM2, "\n\rVelocity Num S1 S2\n\r");
	//Printf(COM2, "%d %d %d\n\r", Num, SpeedDirSensor, Extra);
	break;
      case DynamicCal:
	// Num
	//		Printf(COM2, "\n\rDynamicCal Num.\n\r");
	//		Printf(COM2, "%d \n\r", Num);
	break;
      case ShortDist:
	ShortByTick(TCID,Num,SpeedDirSensor);
	//		Printf(COM2, "\n\rShortDist Num Dist\n\r");
	//		Printf(COM2, "%d %d\n\r", Num, SpeedDirSensor);
	break;
      case StopBefore:
	//		Printf(COM2, "\n\rStopBefore Num Sensor Dist\n\r");
	//		Printf(COM2, "%d %d %d\n\r", Num, SpeedDirSensor, Extra);
	break;
      case SetA:
	//Printf(COM2,"\n\rInitializing Track A . . .\n\r");
	TSSendMsg.TrackAB = 'A';
	TSSendMsg.Type = InitTrack;
	Send(TSServer,(void*)&TSSendMsg,sizeof(TSMsg),(void*)&Result,sizeof(int));
	//Printf(COM2,"Track Init Result%d\n\r",Result);
	//Printf(COM2,"Track A Ready!\n\r");
	bSetTrack = 1;
	DrawTrack('a');
	break;
      case SetB:
	//Printf(COM2,"\n\rInitializing Track B . . .\n\r");
	TSSendMsg.TrackAB = 'B';
	TSSendMsg.Type = InitTrack;
	Send(TSServer,(void*)&TSSendMsg,sizeof(TSMsg),(void*)&Result,sizeof(int));
	//Printf(COM2,"Track B Ready!\n\r");
	bSetTrack = 1;
	DrawTrack('B');
	break;
      case Goto:
	if(GoToSensor(TCID, Num, SpeedDirSensor)){
	  PRINTAT(40,80,"Train Not Registered!");
	}
	break;
      }			// Index = 0;
      ClearBuffer(&InputBuf);
      CURSOR(60,1);
      Printf(COM2,"> Prompt: %c[J",ESC);
      break;
    case '\b':
      if (InputBuf.Length > 0) {
	// Index--;
	// InputBuffer[Index] = '\0';
	BufferPopLast(&InputBuf);
	// Printf(COM2,"Buf len %d", InputBuf.Length);
	Printf(COM2,"\033[1D \033[1D"); // Move cursor back 1, overwrite with space then move cursor back 1 again
      }
      break;
    default:
      if (IsNumber(Ch) || IsAlphabet(Ch) || Ch == '`' || (IsSpace(Ch) && InputBuf.Length > 0)) {
	Putc(COM2,Ch);
	FeedBufferByte(&InputBuf, (void*)Ch);
	// InputBuffer[Index++] = Ch;
      }
      break;
    }
  }
  Exit();
}
