#include <bwio.h>
#include <io-api.h>
#include <ts7200.h>
#include <types.h>

void EnableUART(int uart){
  int *Control;
  int Mask;
  switch (uart){
  case COM1:
    Control = (int *)(UART1_BASE | UART_CTLR_OFFSET);
    Mask = (1<<5 | 1<< 4| 1<<3 | 1);
    //Mask = (1<<5 | 1<< 4| 1);
    break;
  case COM2:
    Control = (int *)(UART2_BASE | UART_CTLR_OFFSET);
    Mask = (1<<5 | 1<< 4 | 1);
    //EnableInterrupt(25);
    //EnableInterrupt(26);
    break;
  }
  *Control = Mask;
}

int ActualPutC(AP* Args){
  asm volatile ("swi 13\n\t");
  register int r0 asm("r0");
  int R = r0;
  return R;
}

int ActualGetC(AP* Args){
  asm volatile ("swi 12\n\t");
  register int r0 asm("r0");
  int R = r0;
  return R;
}

int Putc(int channel, char c){
  //bwprintf(COM2,"putc: putc get called\n\r");
  IOReq SendReq;
  SendReq.Byte = c;
  SendReq.Type = OUT;
  SendReq.Len = 0;
  SendReq.Chan = channel;
  int Result, Reply;

  AP Args;
  Args.arg1 = (void *)(&SendReq);
  Args.arg2 = (void *)sizeof(IOReq);
  Args.arg3 = (void *)(&Reply);
  Args.arg4 = (void *)sizeof(int);

  Result = ActualPutC(&Args);
  if(Result == sizeof(int)) return 1;
}

int PutStr(int channel, const char *Str,int Len){
  IOReq SendReq;
  SendReq.Len = Len;
  SendReq.Type = OUT;
  SendReq.Addr = (void *)(Str);
  SendReq.Chan = channel;
  int Result, Reply;

  AP Args;
  Args.arg1 = (void *)(&SendReq);
  Args.arg2 = (void *)sizeof(IOReq);
  Args.arg3 = (void *)(&Reply);
  Args.arg4 = (void *)sizeof(int);
  //bwprintf(COM2,"PutStr: %d\n\r",Len);
  Result = ActualPutC(&Args);
  //bwprintf(COM2,"PutStr: result\n\r");
  if(Result == sizeof(int)) return Len;
}

char Getc(int channel){
  IOReq SendReq;
  SendReq.Type = IN;
  SendReq.Chan = channel;
  SendReq.Len = 0;
  int Result;
  char Byte;

  AP Args;
  Args.arg1 = (void *)(&SendReq);
  Args.arg2 = (void *)sizeof(IOReq);
  Args.arg3 = (void *)(&Byte);
  Args.arg4 = (void *)sizeof(char);

  
  //bwprintf(COM2,"Getc:\n\r");
  Result = ActualGetC(&Args);
  //bwprintf(COM2,"Getc: result %d\n\r",Result);
  if(Result == sizeof(char)) return Byte;
}

int GetStr(int channel,char* Str, int Len){
  IOReq SendReq;
  SendReq.Type = IN;
  SendReq.Chan = channel;
  SendReq.Len = Len;
  SendReq.Addr = (void *)Str;
  int Result,ReplyMsg;

  AP Args;
  Args.arg1 = (void *)(&SendReq);
  Args.arg2 = (void *)sizeof(IOReq);
  Args.arg3 = (void *)(&ReplyMsg);
  Args.arg4 = (void *)sizeof(int);

  
  //bwprintf(COM2,"Getc:\n\r");
  Result = ActualGetC(&Args);
  //bwprintf(COM2,"Getc: result %d\n\r",Result);
  if(Result == sizeof(int)) return ReplyMsg;
}
