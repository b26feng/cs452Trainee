#include <buffer.h>
#include <bwio.h>
#include <nameserver.h>
#include <syscall.h>
#include <td-shared.h>
#include <types.h>
#include <ts7200.h>

void IOServer(char IO, IOType Producer, IOType Consumer){
  int Result;
  char ServerName[] = "IServer";
  ServerName[0] = IO;
  //bwprintf(COM2,"%s:before register\n\r",ServerName);
  Result = RegisterAs(ServerName);
  //bwprintf(COM2,"%s:registered\n\r",ServerName);
  // set up send q
  int MyID = MyTid();
  int MyIndex = GetMemoryIndexINT(MyID);
  Buffer** AllSendQ = GetAllSendQ();

  //bwprintf(COM1,"NS Index %d\n\r",MyIndex);
  // bwprintf(COM1,"NS AllSendQ add%d\n\r",AllSendQ);
  Buffer SendQ;
  int QStorage[MAX_NUM_TD];
  Result = InitBuffer(&SendQ,(void *)QStorage,(int)(MAX_NUM_TD));
  AllSendQ[MyIndex] = &SendQ;

  Buffer WL;
  int WL1Storage[MAX_NUM_TD];
  Result = InitBuffer(&WL,(void *)WL1Storage,(int)(MAX_NUM_TD));

  Buffer CAdr;
  int CAdr1Storage[MAX_NUM_TD];
  Result = InitBuffer(&CAdr,(void *)CAdr1Storage,(int)(MAX_NUM_TD));

  Buffer CLen;
  int CLenStorage[MAX_NUM_TD];
  Result = InitBuffer(&CLen,(void *)CLenStorage,(int)(MAX_NUM_TD));

  
  Buffer PL;
  int PL1Storage[MAX_NUM_TD];
  Result = InitBuffer(&PL,(void *)PL1Storage,(int)(MAX_NUM_TD));

  Buffer PAdr;
  int PAdr1Storage[MAX_NUM_TD];
  Result = InitBuffer(&PAdr,(void *)PAdr1Storage,(int)(MAX_NUM_TD));
  
  Buffer PLen;
  int PLenStorage[MAX_NUM_TD];
  Result = InitBuffer(&PLen,(void *)PLenStorage,(int)(MAX_NUM_TD));
  
  Buffer Buff;
  int B1Storage[BUFF_SIZE>>1];
  // need to check errors
  Result = InitBuffer(&Buff,(void *)B1Storage,(int)(BUFF_SIZE<<1));


  // some sort of while loop to receive and reply
  int TaskToReceive;
  IOReq ReceiveSlot;
  Message ReceiveMsg;
  ReceiveMsg.Addr = (void *)&ReceiveSlot;
  
  int WLResult = 1;
  int PLResult = 1;
  int TxLen = 0;
  
  char bIsWLTask;
  char bIsPLTask;
  char bIsConsumer;
  char bIsProducer;
  char* str;
  char bEmpty;
  char bReceived = 0;
  char Byte;
  //bwprintf(COM1,"%s: before loop\n\r",ServerName);
  FOREVER{
    //TaskToReceive = NULL;
    // if didn't wake up from block, first try to get one from wait list
    if(bReceived == 0){
      if(!bEmpty){
	WLResult = BufferFIFO(&WL,(void *)&TaskToReceive);
	BufferFIFO(&CAdr,(void *)&str);
	BufferFIFO(&CLen,(void *)&TxLen);
	bIsConsumer = bReceived = bIsWLTask = (WLResult == 0);
	Result = WLResult;
	//if(MyID == 5) bwprintf(COM2,"WL: %d needs %d res %d  \n\r",TaskToReceive,TxLen,Result);
      }else if(PLen.Length!=0 && PLenStorage[PLen.Head] <= BufferSlots(&Buff)){
	PLResult = BufferFIFO(&PL,(void *)&TaskToReceive);
	BufferFIFO(&PAdr,(void *)&str);
	BufferFIFO(&PLen,(void *)&TxLen);
	bIsProducer = bReceived = bIsPLTask = (PLResult == 0);
	Result = PLResult;
      }else{	
	Result = BufferFIFO(&SendQ,(void *)(&TaskToReceive));
	//if(MyID == 5)bwprintf(COM2,"SQ %d %d %d\n\r",Result,TaskToReceive,TxLen);
	BufferPopHead(&SendQ);
      }
      //bwprintf(COM2,"%s: T %d R %d Chn %d\n\r",ServerName,TaskToReceive,WLResult,channel);
    }
    if(Result == 0){
      //if(MyID == 5) bwprintf(COM2,"%s: Res %d, %d\n\r",ServerName,Result,TaskToReceive);
      if(bReceived == 0){
	//bwprintf(COM2,"%s: Receive %d\n\r",ServerName, TaskToReceive);
	Result = Receive(TaskToReceive,(void*) &ReceiveMsg, sizeof(IOReq));
	TaskToReceive = ReceiveMsg.SenderID;
	bIsConsumer = (ReceiveSlot.Type == Consumer);
	bIsProducer = (ReceiveSlot.Type == Producer);	
	TxLen = ReceiveSlot.Len;
	str = (char *)(ReceiveSlot.Addr);
      }
      //bwprintf(COM2,"%s: Got %d \n\r",ServerName,TaskToReceive);
      bReceived = 0;
      // if sender is getc or we got one from wait list, we do receive procedure
      if(bIsConsumer){
	//if(MyID == 5) bwprintf(COM2,"%d C\n\r",TaskToReceive);
	// if Buffer is empty, put this on wait list
	if(Buff.Length == 0){
	  bEmpty = 1;
	  if(!bIsWLTask){
	    //if(MyID == 5) bwprintf(COM2,"%d -> WL\n\r",TaskToReceive);
	    FeedBuffer(&WL,(void*) TaskToReceive);
	    FeedBuffer(&CLen,(void *)TxLen);
	    FeedBuffer(&CAdr,(void *)str);
	  }
	  bIsConsumer = bIsWLTask = 0;
	  Result = 1;
	  continue;
	}
	if(TxLen > 0){
	  //if(MyID==5) bwprintf(COM2,"%d got %d %d\n\r",TaskToReceive,TxLen,Buff.Length);
	  TxLen = (Buff.Length > TxLen)? TxLen:Buff.Length;
	  //if(MyID==5) bwprintf(COM2,"%d got %d",TaskToReceive,TxLen);
	  BufferFIFOStr(&Buff,(void *)str,TxLen);
	  //if(MyID == 5) bwprintf(COM2,"%d got reply %d\n\r",TaskToReceive,TxLen);
	  Reply(TaskToReceive,(void *)(&TxLen),sizeof(int));
	}else{
	  Result = BufferFIFOByte(&Buff, &Byte);
	  BufferPopHead(&Buff);
	  Reply(TaskToReceive,(void *)(&Byte), sizeof(char));
	}
	//bwprintf(COM2,"%s: %d ced!!\n\r",ServerName,TaskToReceive);
	if(bIsWLTask){
	  bIsConsumer = bIsWLTask = 0;
	  BufferPopHead(&WL);
	  BufferPopHead(&CAdr);
	  BufferPopHead(&CLen);
	}
	//if(MyID = 5) bwprintf(COM2,"%d Ced\n\r",TaskToReceive);
      }else if(bIsProducer){
	//bwprintf(COM2,"%s: Task %d Tx %d\n\r",ServerName,TaskToReceive,TxLen);
	//bwprintf(COM2,"%s: BFree %d\n\r",ServerName,BufferSlots(&Buff));
	bEmpty = 0;
	if(ReceiveSlot.Len == 0){
	  Result = FeedBufferByte(&Buff, (void *)(ReceiveSlot.Byte));
	}else if(TxLen > 0){
	  if(TxLen > BufferSlots(&Buff)){
	    if(!bIsPLTask){
	      FeedBuffer(&PL,(void *) TaskToReceive);
	      FeedBuffer(&PLen,(void *)(TxLen));
	      FeedBuffer(&PAdr,(void *)(str));
	    }
	    bIsProducer = bIsPLTask = 0;
	    Result = 1;
	    continue;
	  }
	  Result = FeedBufferStr(&Buff,(void *)(str),TxLen);
	}
	Reply(TaskToReceive,(void *)(&Result), sizeof(int));
	if(bIsPLTask){
	  BufferPopHead(&PL);
	  BufferPopHead(&PAdr);
	  BufferPopHead(&PLen);
	}
      }
    }else{
      bReceived = 1;
      PLResult = WLResult = 1;
      Result = bIsPLTask = bIsWLTask = 0;
      //if(MyID == 5) bwprintf(COM2,"->NULL\n\r");
      Receive(-1,(void*) &ReceiveMsg, sizeof(IOReq));
      TaskToReceive = ReceiveMsg.SenderID;
      TxLen = ReceiveSlot.Len;
      str = (char *)(ReceiveSlot.Addr);
      bIsProducer = (ReceiveSlot.Type == Producer);
      bIsConsumer = (ReceiveSlot.Type == Consumer);
      //if(MyID == 5)bwprintf(COM2,"Back %d Type: %d\n\r",TaskToReceive,ReceiveSlot.Type);
      //bwprintf(COM2,"%s: Null is %d\n\r",ServerName,ReceiveSlot.Type = Producer);
    }
  }
}

void IServer1(){
  IOServer('i',NIN,IN);
}

void OServer1(){
  IOServer('o',OUT,NOUT);
}

void IServer2(){
  IOServer('I',NIN,IN);
}

void OServer2(){
  IOServer('O',OUT,NOUT);
}
