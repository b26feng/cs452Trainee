#ifndef KERNEL_H
#define KERNEL_H

#include <types.h>
void kernelInit (KernelStruct* Colonel);

void InitSwi();

//int activate(KernelStruct* Colonel); 

//void handle(KernelStruct* Colonel, int syscallRequest);

void EnableInterrupt(int ID);

void EnableCaches();

void DisableCaches();

TD* GetFreeTD(KernelStruct* Colonel);

#endif
