#ifndef SYSCALL_HANDLER_H
#define SYSCALL_HANDLER_H

#include <types.h>

int Activate(KernelStruct* Colonel,TD* Task);

int Handle(KernelStruct* Colonel, int n);

#endif

