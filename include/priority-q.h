#ifndef __TDPQ_H__
#define __TDPQ_H__

#define DEFAULT_SIZE 4

#include <types.h>

int TDPQReady(TDPQ* PQ);

int PushToTDPQ(TDPQ* PQ, TD* Task);
// Fill Char with the char in Start

TD* TDPQGetStart(TDPQ* PQ);

// Pop from Start
int TDPQPopStart(TDPQ* PQ);

// Clear Buffer
int ClearPQ(TDPQ* PQ);

#endif
