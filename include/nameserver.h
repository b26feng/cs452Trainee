#ifndef NAMESERVER_H
#define NAMESERVER_H

void NameServer();

int WhoIs (char* Name);

int RegisterAs (char* Name);

#endif
