#ifndef IO_H
#define IO_H

#include <types.h>

#define COM1 0
#define COM2 1

void Putw( int channel, int n, char fc, char *bf,Buffer* Buf );

int a2d( char ch );

char a2i( char ch, char **src, int base, int *nump );

void ui2a( unsigned int num, unsigned int base, char *bf );

void i2a( int num, char *bf );

// void iFormat ( int channel, char *fmt, va_list va );

void Printf( int channel, char *fmt, ... );

#endif
