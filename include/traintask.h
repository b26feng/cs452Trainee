#ifndef __TRAINTASK_H__
#define __TRAINTASK_H__

#include <types.h>

int UpdateTrain(Train* Trains,char* Sensors,int TimeStamp,int* Diner);
void UpdateNextOrNot(Train* TList,TrackNode* Switch,int* Dinner,const int* Universal);
void ReverseUpdate(Train* T,int TimeStamp,int* Dinner);
void UpdateTrainSpeed(Train* T, int TimeStamp, int *Dinner);
#endif
