#ifndef NOTIFIER_H
#define NOTIFIER_H


void ClockNotifier();
void TermInNotifier();
void TermOutNotifier();
void TrainInNotifier();
void TrainOutNotifier();
#endif
