#ifndef GENERIC_HEAP_H
#define GENERIC_HEAP_H

int printHeap(MinHeap* Heap);

TrackNode* GetMinHeap(MinHeap* Heap);

void RemoveMinHeap(MinHeap* Heap);

void InsertHeap(MinHeap* Heap, TrackNode* NewNode);

void BubbleDownHeap(MinHeap* Heap, int StartIndex);

void BubbleUpHeap(MinHeap* Heap, int Last);

void DecreaseKey(MinHeap* Heap, TrackNode* NewDistNode);

#endif

