#ifndef MIN_HEAP_H
#define MIN_HEAP_H

#include <types.h>

int printMinHeap(MinHeap* Heap);

int GetMinMinHeap(MinHeap* Heap, int* TaskID, int* DelayedTick);

void RemoveMinMinHeap(MinHeap* Heap);

void InsertMinHeap(MinHeap* Heap, int TaskID, int DelayedTick);

void BubbleDownMinHeap(MinHeap* Heap, int StartIndex);

void BubbleUpMinHeap(MinHeap* Heap, int Last);

#endif

