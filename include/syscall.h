#ifndef SYSCALL_H
#define SYSCALL_H

/*
    According to the kernel description doc
*/

#include <types.h>

// Kernel 1
int Create (AP* Args);

int MyTid();

int MyParentTid();

void Pass();

void Exit();

int Send (int TaskID, void* Msg, int MsgLen, void* Reply, int ReplyLen);

int Receive (int TaskID, void* Msg, int MsgLen);

int Reply (int TaskID, void* Reply, int ReplyLen);

int RecordNameServer(AP* Args);

Buffer** GetAllSendQ();

int* GetShared();

int AwaitEvent(int EventID);

void QuitProgram();
#endif
