#ifndef __CANVAS_H__
#define __CANVAS_H__

/* The cursor addressing macro is provided by
 * courseweb.stthomas.edu/tpsturm/private/notes/qm300/ANSI.html
 */
#include <types.h>

#define ESC 27
#define UP 65
#define DOWN 66
#define RIGHT 67
#define LEFT 68

#define CLEARALL Printf(COM2,"%c[2J",ESC);
#define CLEARLINE Printf(COM2,"%c[1k",ESC);

#define MOVE_UP(Num) Printf(COM2,"%c[%dA",ESC,Num);
#define MOVE_DOWN(Num) Printf(COM2,"%c[%dB",ESC,Num);
#define MOVE_LEFT(Num) Printf(COM2,"%c[%dD",ESC,Num);
#define MOVE_RIGHT(Num) Printf(COM2,"%c[%dC",ESC,Num);
#define CURSOR(Row,Col) Printf(COM2,"%c[%d;%dH",ESC,Row,Col);

#define PRINTAT(Row,Col,String) Printf(COM2,"%s%c[%d;%dH%s%s",SAVE_CURSOR,ESC,Row,Col,String,RESTORE_CURSOR);

typedef struct
{
  int x;
  int y;
  char bChanged;
}CursorPos;

void DrawLine_V(const char Elem, const int Length);

void InitLayout();

void DrawTrack(char A);
#endif
