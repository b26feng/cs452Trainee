#ifndef __TRIE_H__
#define __TRIE_H__

#include <types.h>

int InsertTo(HT* Table,int Tid, char* Str, int Len);

int Lookup(HT* Table,char* Str, int Len);

#endif
