#ifndef BUFFER_H
#define BUFFER_H
#include <types.h>
// General Purpose buffer


// returns 1 if buffer is ready(not null, has memory)
int BufferReady(Buffer* Buf);

void BufferClear(Buffer* Buf);

int BufferSlots(Buffer* Buf);

// Initialize buffer
int InitBuffer(Buffer* Buf, void* Store, int size);

// insert at the last
int FeedBuffer(Buffer* Buf,void* Word);

// insert a byte at the last
int FeedBufferByte(Buffer* Buf, void* Byte);

// insert multiple byte at once
int FeedBufferStr(Buffer* Buf, void* Str, int Len);

// Get from the start, does not change size
int BufferFIFO(Buffer* Buf, void* Dest);

// Get a single byte from the start, does not change size
int BufferFIFOByte(Buffer* Buf,void* Dest);

// Get multiple byte from start, change size
int BufferFIFOStr(Buffer* Buf,void* Dest,int len);

int BufferPopHead(Buffer* Buf);

// Get from the Last, does not change size
int BufferFILO(Buffer* Buf, void* Dest);

// Clear the Buffer, does not modify memory
int ClearBuffer(Buffer* Buf);

// pop out first Num of the buffer
int BufferShrinkFIFO(Buffer* Buf, int Num);

#endif
