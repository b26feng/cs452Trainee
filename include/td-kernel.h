#ifndef __TD_K_H
#define __TD_K_H

#include <types.h>

TD* CreateTask(KernelStruct* Colonel, int Priority, void* code);

void RemoveTask(KernelStruct* Colonel, TD* Task);

int isTaskAlive(int TaskID);


#endif

