#ifndef __IO_API_H__
#define __IO_API_H__

void EnableUART(int uart);

int Putc(int channel,char c);
int PutStr(int channel,const char *Str, int Len);
char Getc(int channel);
int GetStr(int channel, char* Str,int Len);
#endif
