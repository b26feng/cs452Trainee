#ifndef TOOLS_H
#define TOOLS_H

#include <types.h>

int stringLen(const char* Str);

// DestinationLength can be greater than that of the Src;
int stringCopy(char* Dest, const char* Src, int DestLength);  

int stringCompare(const char* Str1, const char* Str2);

int byteCopy(void* Dest, void* Src, int DestLen, int SrcLen);

void memcpy(void* Dest,void* Src, int Size);

void setDJerk(Train* A);

void setDTDiff(Train* A);

void setStopTick(Train* A);

void setVel(Train* A);

void setStopDist(Train* A);

int SDGetDistance(Train* A, int Tick);

int ABS(int Num);

int SDGetTicks(Train* A,int Distance);

TrackNode* GetNextSensor(TrackNode* Src,char Normal);

int GetNextSensorD(TrackNode* Src);

// return the next sensor, and set the distance to next sensor
TrackNode* SetNextSensorD(TrackNode* Src,Train* T);

TrackNode* GetLastSensor(TrackNode* Src);

int GetDist(TrackNode* Src,TrackNode* Dest);

char RegTrack(Train* T);

void FreeTrack(Train* T);

void FreeFromSwitch(TrackNode* S);

#endif
