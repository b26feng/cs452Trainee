#ifndef IPC_H
#define IPC_H

#include<types.h>

int doSend(KernelStruct* Colonel, int TaskID, void* Msg, int MsgLen, void* Reply, int ReplyLen);

int doReceive(KernelStruct* Colonel, int TaskID, void* Msg, int MsgLen);

int doReply(KernelStruct* Colonel, int TaskID, void* Reply, int ReplyLen);

#endif
