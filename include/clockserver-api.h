#include <types.h>
#include <clock.h>
#include <syscall.h>
#include <bwio.h>


int Delay (int TaskID, int Ticks);

int Time (int TaskID);

int DelayUntil (int TaskID, int Ticks);
