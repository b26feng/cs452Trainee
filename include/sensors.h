#ifndef SENSORS_H
#define SENSORS_H


/*
Track Detection Modules (COMPIED FROM the Marklin's Commands)
The s88 Decoders store data sent from the track detectors or the reed switches. Each s88 has room to store two bytes of data (16 separate contacts). In order for these decoders to be read by the computer, they must first be told to "dump" their memories into the computer. Two options are available for requesting this "dump", depending on how you want the s88 units to feed their data into the computer. =

Read one s88 unit - If only one s88 is to be read, you send the code "192" PLUS the number of the unit to be read in. For example: you only have one s88 on the layout - you read it with the command "193" (192 +1). On the other hand, if you had 4 units on the layout and you wanted to read only number 3, then send the code 195 (192+3). The command would be:

    10 PRINT #1,CHR$(195);..........

The command is not finished because more instructions need to be given to the computer so it can accept the incoming data, but that will be covered later.

Read many s88 units - The other option for "dumping" s88 memory is for getting all the s88 units to dump memory up to a specific unit. For example: four s88 units are on the layout and you want information from the first 3 only. The command for this type of memory dump is the number 128 PLUS the number of the last unit to be read. In this case the command would be "131" (128+3). The command would look like:

    10 PRINT #1,CHR$(131);..........

In this case, the first s88 on line would dump its memory, then the second, and finally the third unit connected in the series.

When any of these commands are given, the s88 units will send data back to the computer. The program needs to be alerted so it can receive this data and report it back to the computer operator in some readable fashion. Most BASIC programs require that two communications lines be opened in order to both send and receive data. The opening lines on a program would be:

    10 OPEN "COM1:2400,N,8,2"FOR OUTPUT AS #1
    20 OPEN "COM1:2400,N,8,2"FOR INPUT AS #2

The parameters mentioned earlier are seen in these statements. Notice the 2400 Baud, No parity, 8 data bits, 2 stop bits. One line is set for output and numbered #1. That is why all the print statements seen earlier said PRINT #1. They were only sending data on the output line. The other line is for input and is numbered as #2. =

If data is expected, then it should be assigned a variable and the computer should be alerted immediately after sending the code to dump memory. The program would look like this:

    10 OPEN "COM1:2400,N,8,2"FOR OUTPUT AS #1
    20 OPEN "COM1:2400,N,8,2"FOR INPUT AS #2
    30 PRINT #1,CHR$(193);:a$=3DINPUT$(2,#2)

Line 30 sends the command CHR$(193) out on #1 telling s88 number 1 to dump memory. That memory will come into the variable "a$". It will be assigned as a result of the command a$=3DINPUT$(2,#2). The first number 2 means that there will be 2 bytes coming in and the #2 means it is on the INPUT format as defined in statement 20. Those two bytes can now be separated and printed on the screen to let you know the status of the s88. This command will do that:

    30 PRINT #1,CHR$(193);:a$=3DINPUT$(2,#2)
    40 contact=3DASC(LEFT$(a$,1)):PRINT contact
    50 contact2=3DASC(RIGHT$(a$,1)):PRINT contact2

A new variable called "contact" will be assigned the leftmost single byte of the two bytes (this is the first one from sockets 1-8 on the s88 unit) and then will be printed to the screen. Line 50 will take the second byte (sockets 9-16 on the s88) and assign it to variable "contact2" and print it.

The data printed to the screen can be confusing unless you understand how binary works. Each of the sockets stand for a binary number in the sequence 1, 2, 4, 8, 16, 32, 64 and 128. They are assigned as follows:

    sockets binary number
    1 =3D 128
    2 =3D 64
    3 =3D 32
    4 =3D 16
    5 =3D 8
    6 =3D 4
    7 =3D 2
    8 =3D 1

If only the contact connected to socket #8 was triggered, then the number printed on the screen would be "1". If only #3 was triggered then it would be "32". If multiple numbers are triggered, then their assigned binary numbers are added together. For instance, contacts #7 and #8 would be reported as a "3" (2+1), while #1 and #8 would be 129 (128+1). =

The second side of the s88 is read the same way, only it will be in the second byte of data received. Contact #9 would be the same as #1 on the opposite side of the s88 and socket #16 is the same as #8 (they are both "1"). Programs can be written that will analyze these number totals and tell you the contacts that were active. It is easy to write such a program. If the number is greater than 128 then you know that contact #1 has been triggered. If this was true, then subtract 128 from the number and check it again. If it is now greater than 64, that means contact #2 was triggered. If that's right, subtract 64 and check the next number, etc.

The s88 units can be told to reset its memory after a dump to the computer, or it can remain as it was and continue adding data to the byte. A reset would make each position in memory a "0" again. If you want it to reset, send the single byte 192 without adding any numbers to it. If the reset mode is to be "off", then send the code 128. 
*/

void SensorPrintTask();

//void ProcessData (int* Sensors, char* SensorChars, int* SensorNums);
void SensorServer();

void SensorNotifier();


#endif
