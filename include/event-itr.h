#ifndef EVENT_ITR_H
#define EVENT_ITR_H

#include <types.h>

void InitWaitEventTable();

int IsValidEvent(int EventID);

int GetEventID(const int IRQ1, const int IRQ2);

int doAwaitEvent(KernelStruct* Colonel, int EventID);

void ProcessEvent(KernelStruct* Colonel, int EventID);

// returns 1 for Event Interrupt is on; 0 off
//int CheckItrEvent(int EventID)

//void SetOnItrEvent(int EventID, int OffSet);

#endif
