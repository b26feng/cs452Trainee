#ifndef __IRQ_H__
#define __IRQ_H__

void ClearAllIRQ(int IRQ1, int IRQ2);
void ClearIRQ(int Source, int IRQ);
void DisableIRQ(int Source, int IRQ);
void EnableIRQ(int Source, int IRQ);
#endif
