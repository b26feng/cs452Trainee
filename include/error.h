#ifndef ERROR_H
#define ERROR_H

#define FAILURE -1
#define SUCCESS 0

#define HT_NOSLOT -11

#define ILLEGAL_NAME -21

#define BUFF_NOTREADY -30
#define BUFF_FULL -31
#define BUFF_EMPTY -32
#define BUFF_INSUFFICIENT -33

#define TRAIN_UNREG -40
#define TRAIN_NEXIT -41
#endif
