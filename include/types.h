#ifndef TYPES_H
#define TYPES_H


#define K2PTEST 0

#define SUCCESS 0
#define FAILURE -1

#define MAX_NUM_PRIORITY 16
#define MAX_NUM_TD 60

#define KERNEL_STACK (0xFFFFFFFF - 0x200000)
#define STACK_SIZE 0x80000 // 512 KB    "mrc p15, 0, r0, c1, c0, 0\n\t" // read SCTLR (system control register) configuration data
#define USER_STACK_TOP (0x1E00000)

#define NULL 0

#define NAMESERVER_TID 6
#define NS_MESSAGE_MAX_LENGTH 18

#define MSG_TRUNCATED -1
#define TASK_DNE -2
#define SRR_FAILED -3
#define TASK_NOT_REPLY_BLOCKED -3
#define NAMESERVER_TID_INVALID -1
#define MAX_NUM_BUFFER 20

#define NAME_LENGTH_MAX 18

#define HASH_SIZE 251

#define FOREVER while(1)

// Clock Stuff
#define NUM_CLOCK_CLIENTS 4

#define INVALID_EVENT -1
#define CORRUPTED_VOLATILE_DATA -2

// Delay
#define CLOCK_SERVER_ID_INVALID -1
#define NON_POSITIVE_DELAY -2

// Events stuff
#define NUM_EVENTS 63
#define ANOTHER_WAITING -3

// from ep93xxs guide pg 166
#define UART2IN_INT 25
#define UART2OUT_INT 26
#define TIMER3_INTERRUPT 51
#define UART1_INT 52
#define IRQ_MAGIC 100

#define NUM_TRAINS 81


#define SENSOR_RESET 192
#define SENSOR_REQUEST 133
#define NUM_SENSORS_DISPLAYED 5
#define NUM_SENSORS_READ 10

#define CMD_SIZE 256

#define CLEAR_SCREEN "\033[2J"
#define UP_LEFT "\033[H"
#define SAVE_CURSOR "\0337"
#define RESTORE_CURSOR "\0338"
#define RESET_COLOR "\033[0m"
#define BLACK "\033[30m"
#define RED "\033[31m"
#define GREEN "\033[32m"
#define YELLOW "\033[33m"
#define BLUE "\033[34m"
#define MAGENTA "\033[35m"
#define CYAN "\033[36m"
#define WHITE "\033[37m"

#define STRAIGHT 33
#define CURVED 34
#define SOLENOID_OFF 32

#define INT_MAX 2147483647;

#define DIR_AHEAD 0
#define DIR_STRAIGHT 0
#define DIR_CURVED 1

typedef enum {
	RESERVE_TRACK,
	FREE_TRACK,
	IS_RESERVED,
	FAIL2RESERVE,
	YES,
	NOT_RESERVED
} TrkRsrvrType;

typedef struct TrackReserverMessage {
	TrkRsrvrType Type;
	int TrainID;
	int Src;
	int Distance;
} TrkRsrvrMsg; 

typedef struct track_edge TrackEdge;
typedef struct track_node TrackNode;

/* BELOW Taken from the track data on the website BELOW */
typedef enum {
  NODE_NONE,
  NODE_SENSOR,
  NODE_BRANCH,
  NODE_MERGE,
  NODE_ENTER,
  NODE_EXIT,
} node_type;

struct track_edge {
  TrackEdge *reverse;
  TrackNode *src, *dest;
  int dist;             /* in millimetres */
};

struct track_node {
  const char *name;
  node_type type;
  int num;              /* sensor or switch number */
  TrackNode *reverse;  /* same location, but opposite direction */
  TrackEdge edge[2];
  char State;
  /* for Dijkstras */
  char bVisited;
  TrackNode * Parent;
  int PathDistance;
  int TrainOwn;
};

/* ABOVE Taken from the track data on the website ABOVE */


typedef enum TrackServerType {
	GetSensorNode,
	GetNextSensorNode,
	GetPathSrcDest,
	FlipSwitch,
	InitTrack,
	TCTS_GETNODE,
	TSTC_WAITSWITCH,
} TSType;

typedef struct trackServerMessage {
  TSType Type;
  TrackNode* Src;
  TrackNode* Dest;
  char TrackAB;
  void* Addr;
} TSMsg;

typedef enum SensorServerMessageType {
	SENSOR_RESPONSE,
	SENSOR_NOTIFIER,
	SENSOR_QUERY
} SSMsgType;

typedef struct SensorServerMessage{
	SSMsgType Type;
	char* SensorInput;
  int TimeStamp;
  //char* SensorChars;
} SSMsg;


typedef enum TrainCommandType {
	SetSpeed,
	Reverse,
	RegTrain,
	QuitTrain,
	WrongInput,
	OnTrain,
	StopAfter,
	Velocity,
	DynamicCal,
	SetSwitch,
	ShortDist,
	StopBefore,
	SetA,
	SetB,
	Goto
} TrainCmdType;

typedef enum TrainControllerMessageType {
	SET_SPEED,
	REVERSE,
	REGTRAIN,
	RESPONSE,
	TEMP,
	WATCHER,
	STPAFT,
	TCSTAMP,
	TCSDTIME,
	TCTS_FIRST,
	TCTS_ORDER,
	TT_GETTRAIN,
	TCS_UPDATE,
	TSTC_UPDATE,
	DOGINIT,
	DOGBACK,
	GET_PF_ORDER,
	GOTO,
	PR_SR,// pr wait on sensors
} TCMsgType;

typedef struct TrainControllerMessage {
  TCMsgType Type;
  char Num;
  char TrainSpeed;
  char SwitchDir;
  int Offset;
  void* Addr;
} TCMsg;

// I/O buffer things
#define BUFF_SIZE 256
typedef struct heapnode {
	int DelayedTick;
	int TaskID;
} HN;

typedef struct minheap {
	int Num;
	int Last;
	void* Storage;
} MinHeap;

typedef enum ClockMessageType {
	DELAY,
	TIME,
	DELAY_UNTIL,
	DELAY_REQUEST,
	DELAY_INFO,
	READY,
	NOTIFIER,
	CONTINUE
} ClkMsgType;

typedef struct ClockMessage {
	int Tick;
	int NumDelays;
	ClkMsgType Type;
} ClkMsg;

typedef enum rockPaperScissorsGame { 
	Rock,
	Paper,
	Scissors,
	None
} RPSChoice;

typedef enum RPSMessageType {
	SignUp,
	Play,
	Quit,
	Draw,
	Win,
	Lose,
	Bye,
	LetsPlay,
	OnePlayer,
	OtherQuit
} RPSMsgType;

typedef struct RPSMessage {
	RPSChoice Choice;
	RPSMsgType Msg;
} RPSMsg;

typedef struct ArgumentPack{
	void * arg0;
	void * arg1;
	void * arg2;
	void * arg3;
	void * arg4;
}AP;


typedef struct buffer {
	void * Storage;
	int Size;
	int Length;
	int Head;
	int Tail;
} Buffer;


typedef struct
{
	unsigned int ClockCount;
	unsigned int LastTimerValue;
	char bChanged;
}Clock;


typedef struct hash_table{
	int *TidSequence;
	char *NameSequence;
	int size;
}HT;

typedef enum IOServerMessageType{
  IN,
  OUT,
  NIN, // notifier input
  NOUT, // notifier output
} IOType;

typedef struct IOServerRequest{
  IOType Type;
  char Byte;
  int Len;
  int Chan;
  void * Addr;
} IOReq;

typedef struct NameServerMessage {
	char Msg[NS_MESSAGE_MAX_LENGTH + 1];
} NSMsg;


typedef enum NameServerMessageType {
	WhoIS,
	RegAS
} NSMsgType;

typedef struct NameServerRequest{
	int TaskID;
	char Msg[NS_MESSAGE_MAX_LENGTH + 1];
	NSMsgType MsgType;
} NSReq;

typedef struct node{
	void* Storage;
	struct node* theNode;
} Node;

typedef struct node_pq{
	Node* Head;
	Node* Tail;
	int Len;
}NPQ;

typedef struct pair {
	int NodeIndex;
	char State;
} Pair;


typedef enum syscall {
	SYS_Create,
	SYS_MyTid,
	SYS_ParentTid,
	SYS_Pass,
	SYS_Exit,
	SYS_Send,
	SYS_Receive,
	SYS_Reply,
	SYS_WHOIS,
	SYS_REGAS,
	SYS_SENDQ,
	SYS_WAITEVENT,
	SYS_GETC,
	SYS_PUTC,
	SYS_SHARED,
	SYS_GETSHARED,
	SYS_QUIT,
	SYS_SDES
} Syscall;


typedef enum taskstate {
	Ready, 
	Init,
	Zombie,
	SendBlocked,
	ReceiveBlocked,
	ReplyBlocked,
	EventBlocked
} State;


struct taskDescriptor {

	int* sp; //sp_usr
	int* lr; // lr_svc
	int spsr; //user's cpsr
	int RetVal; // passing kernel's return value to user
	AP* Args; //argument that user gives kernel
	int TaskID;
	int ParentID;
	State TaskState;
	int TaskPriority;
  int NextPriority;
	struct taskDescriptor* NextFree;
	struct taskDescriptor* NextInPQ;
	char* Msg;
};

typedef struct taskDescriptor TD;

typedef struct
{
	TD* Head;
	TD* Tail;
	int Length;
} TDPQ;


typedef struct eventAwaitTask {
  TD* AwaitTask;
  void* Data;
} EventAwaitTask;


typedef struct kernel {
  TD* Active;
  MinHeap* Scheduler;
  //TDPQ ArrayPQ[MAX_NUM_PRIORITY];
  TD Tasks[MAX_NUM_TD];
  Buffer** AllSendQ;
  //index is TaskID; value of 1 = alive
  // a free list of pointers to free TDs. This might take the form of bits set in a couple of words. WHAT?
  // a structure for keeping the request currently being serviced, including its arguments.
  TD* FirstOfFreeList;
  TD* LastOfFreeList;
  EventAwaitTask* WaitTable;
  int NumTDsAvail;
  int NameServerID;
  int IServer1ID;
  int IServer2ID;
  int OServer1ID;
  int OServer2ID;
  void* Shared;
} KernelStruct;

typedef struct {
  int SenderID;
  void* Addr;
} Message;

typedef struct{
  int TrainID;
  int Index;
  float Tick14;
  float Tick10;
  float Dist14;
  float Dist10;
  float DJerk;
  float DTDiff;
  float Vel;
  float StopDist;
  float StopTick;
  float DymVel;
  float alpha;
  int SDMinTick;//tick to move train for 1 cm
  int SDMaxTick;//tick to move train for ...?
  int Speed;
  int LocOff;
  int TimeStamp;
  int Dist;
  int MyDog;
  int MyPR;
  TrackNode* Last;
  TrackNode* LocBase;
  TrackNode* Next;
  TrackNode* NNext;
  char bRegistered;
  char bStartup;
  char bTimedOut;
  char bChanged;
}Train;

typedef enum {
  PRTASK,
  // PRTASK_RESULT,
  PATH_RESULT,
  ORDER_FROM_TC
} PRType;

typedef struct{
  PRType Type;
  Train* Trn;
  int Dest;
  Buffer* Buf;
} PRMsg;

/*
typedef enum{
  
}
typedef struct{
  TNType Type;
  int IndexA;
  int IndexB;
  int IndexC;
  int NeighA;
  int NeighB;
  int NeighC;
  int DA;
  int DB;
  int DC;
}TN;
*/
// typedef enum TrainCommandType {
// 	SetSpeed,
// 	Reverse,
// 	SetSwitch,
// 	QuitTrain
// } TrainCmdType;
// typedef enum TrainControllerMessageType {
// 	SET_SPEED,
// 	REVERSE,
// 	RESPONSE
// } TCMsgType;
// typedef struct TrainControllerMessage {
// 	TCMsgType Type;
// 	int Num;
// 	int TrainSpeed;
// 	int SwitchDir;
// } TCMsg;


//static Buffer*** TotalSendQ;

#endif
