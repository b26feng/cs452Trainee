/* THIS FILE IS GENERATED CODE -- DO NOT EDIT */
#ifndef __TRDT_H__
#define __TRDT_H__
#include <types.h>

// The track initialization functions expect an array of this size.
#define TRACK_MAX 144
#define SWITCH_OFFSET 80
#define SWITCH_STEP 2

void init_tracka(TrackNode *track);
void init_trackb(TrackNode *track);
#endif
