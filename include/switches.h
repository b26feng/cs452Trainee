#ifndef SWITCHES_H
#define SWITCHES_H

void PrintSwitch(int SwitchNum, char SwitchDir);

void OffSolenoid();

void InitSwitches(int ClkServer, int TSServer);

//void InitSwitches(int ClkServer);

void TrainSetSwitch(int ClkServer, int TSServer,int SwitchNum, int SwitchDir);

//void TrainSetSwitch(int ClkServer,int SwitchNum, int SwitchDir);
#endif
