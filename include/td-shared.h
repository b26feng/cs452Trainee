#ifndef __TD_S_H
#define __TD_S_H

#include <types.h>

int isTaskAlive(int TaskID);

int GetGeneration(TD* Task);

int GetGenerationINT(int TaskID);

int GetAvailability(TD* Task);

int GetMemoryIndexINT(int TaskID);

int GetMemoryIndex(TD* Task);


#endif

