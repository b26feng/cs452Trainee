#ifndef TIMER_H
#define TIMER_H

#define CLOCK_LOAD_VALUE 5080

#define SLOW_LOAD_VALUE 200

#define TIMER_LOAD_VALUE 508000;

#include <types.h>

void ClockInit(Clock *MyClock);

void ClockUpdate(Clock *MyClock);

void ClearAllTimer();
#endif
